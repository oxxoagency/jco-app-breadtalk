package com.breadtalk.app.data.local

import com.breadtalk.app.data.remote.model.res.RelatedMenuRes

data class SpecialOffer(
    val menuCode: String?,
    val img: String,
    val productName: String,
    val period: String
) : BaseCell()

data class DetailSpecialOffer(
    val id: String?,
    val bannerImage: String?,
    val bannerDescription: String?,
    val promoPeriod: String?,
    val relatedMenu: List<RelatedMenuRes>?,
    val version:Int?
): BaseCell()

data class RelatedMenu(
    val id: String?,
    val menuCode: String?,
    val menuName: String?,
    val menuImage: String?,
    val isPromo: Boolean?,
    val price: String?,
    val priceText: String?
) : BaseCell()