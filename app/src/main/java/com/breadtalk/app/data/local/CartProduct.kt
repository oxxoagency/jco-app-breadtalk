package com.breadtalk.app.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cart_product")
data class CartProduct(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    val menuCode: String?,
    val name: String?,
    val imgURL: String?,
    var price: Double,
    val productType: String?,
    var qty: Int,
    var priceOriginal: Double,
    var variantname: String = "",
    val idType:Int?,
    var isBasePrice:Int?,
    var menuDetail:String?,
    var detailVariant: MutableList<CartDetailVariant>?
) : BaseCell()

@Entity(tableName = "cart_product_pick_up")
data class CartProductPickUp(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    val menuCode: String?,
    val name: String?,
    val imgURL: String?,
    var price: Double,
    val productType: String?,
    var qty: Int,
    var priceOriginal: Double,
    var variantname: String = "",
    val idType:Int?,
    var isBasePrice:Int?,
    var menuDetail:String?,
    var detailVariant: MutableList<CartDetailVariant>?
) : BaseCell()