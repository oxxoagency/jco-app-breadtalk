package com.breadtalk.app.data.repository

import android.app.Application
import com.breadtalk.app.data.remote.api.BreadTalkApi
import com.breadtalk.app.data.remote.helper.ErrorNetworkHandler
import com.breadtalk.app.data.remote.model.req.CityReq
import com.breadtalk.app.data.remote.model.req.CitySearchReq
import com.breadtalk.app.data.remote.model.req.OutletCityReq
import com.breadtalk.app.data.remote.model.req.OutletReq
import com.breadtalk.app.data.remote.model.res.CityRes
import com.breadtalk.app.data.remote.model.res.OutletRes
import io.reactivex.Single
import javax.inject.Inject

interface PickupRepository {
    fun getOutletLocation(body: OutletReq): Single<OutletRes>
    fun getCity(body: CityReq): Single<CityRes>
    fun getCitySearch(body: CitySearchReq): Single<CityRes>
    fun getOutletByCity(body: OutletCityReq): Single<OutletRes>
}

class PickupRepositoryImpl @Inject constructor(
    private val application: Application,
    private val service: BreadTalkApi,
) : PickupRepository, BaseRepository() {

    override fun getOutletLocation(body: OutletReq): Single<OutletRes> =
        composeSingle { service.showOutlet(body) }
            .compose(ErrorNetworkHandler())

    override fun getCity(body: CityReq): Single<CityRes> =
        composeSingle { service.getCity(body) }.compose(ErrorNetworkHandler())

    override fun getCitySearch(body: CitySearchReq): Single<CityRes> =
        composeSingle { service.getCitySearch(body) }.compose(ErrorNetworkHandler())

    override fun getOutletByCity(body: OutletCityReq): Single<OutletRes> =
        composeSingle { service.showOutletByCity(body) }.compose(ErrorNetworkHandler())
}
