package com.breadtalk.app.data.remote.api

import com.breadtalk.app.data.remote.model.req.OrderPaymentReq
import com.breadtalk.app.data.remote.model.req.PaymentReq
import com.breadtalk.app.data.remote.model.res.PaymentListRes
import com.breadtalk.app.data.remote.model.res.SuccessPaymentRes
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface BreadTalkPayment {
    @GET("payment/list")
    fun getPaymentList(): Single<PaymentListRes>

    @POST("midtrans/payment")
    fun getMidtransPayment(@Body body: PaymentReq): Single<SuccessPaymentRes>

    @POST("order/payment")
    fun orderPayment(@Body body: OrderPaymentReq): Single<SuccessPaymentRes>
}