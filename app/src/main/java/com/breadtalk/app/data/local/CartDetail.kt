package com.breadtalk.app.data.local

data class CartDetail(
    val paymentName: String?,
    var paymentIcon: String?,
    var price: String?,
    var deliveryServiceText: String?,
    var deliveryService:Int?,
    var totalPrice: String?,
    var notes: String?,
    val ecobag: String?,
    val ecobagPrice: String?,
    val ecobagSubTotal: String?,
    val ecobagQty: String?,
    val voucher: String?,
    val promo: Int?,
    val freeDeliveryText: String?,
    val freeDelivery: Int?,
    val totalDeliveryFee:String?,
    val isDelivery: Boolean,
) : BaseCell()