package com.breadtalk.app.data.repository

import com.breadtalk.app.data.remote.api.DirectJcoApi
import com.breadtalk.app.data.remote.helper.ErrorNetworkHandler
import com.breadtalk.app.data.remote.model.req.DirectCouponReq
import com.breadtalk.app.data.remote.model.req.DirectDetailRecentOrderReq
import com.breadtalk.app.data.remote.model.req.DirectRecentOrderReq
import com.breadtalk.app.data.remote.model.req.RecentOrderReq
import com.breadtalk.app.data.remote.model.res.CouponRes
import com.breadtalk.app.data.remote.model.res.DetailRecentOrderRes
import com.breadtalk.app.data.remote.model.res.RecentOrderRes
import io.reactivex.Single
import javax.inject.Inject

interface DirectJcoRepository {
    fun getCouponList(auth: String, body: DirectCouponReq): Single<CouponRes>
    fun getTransactionDetail(
        auth: String,
        body: DirectDetailRecentOrderReq,
    ): Single<DetailRecentOrderRes>

    fun getRecentOrder(auth: String, body: DirectRecentOrderReq): Single<RecentOrderRes>
}

class DirectJcoRepositoryImpl @Inject constructor(private val service: DirectJcoApi) :
    DirectJcoRepository, BaseRepository() {

    override fun getCouponList(auth: String, body: DirectCouponReq): Single<CouponRes> =
        composeSingle { service.showCouponList(auth, body) }
            .compose(ErrorNetworkHandler())

    override fun getTransactionDetail(
        auth: String,
        body: DirectDetailRecentOrderReq,
    ): Single<DetailRecentOrderRes> =
        composeSingle { service.getOrderDetail(auth, body) }.compose(ErrorNetworkHandler())

    override fun getRecentOrder(auth: String, body: DirectRecentOrderReq): Single<RecentOrderRes> =
        composeSingle { service.showRecentOrder(auth, body) }.compose(ErrorNetworkHandler())
}