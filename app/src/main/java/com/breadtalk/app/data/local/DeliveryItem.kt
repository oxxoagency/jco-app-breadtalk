package com.breadtalk.app.data.local

data class MemberAddressReq(
    val member_phone: String
)

data class MemberAddressRes(
    val `data`: List<MemberAddress>?,
    val status: Int,
    val message: String,
) : BaseCell()

data class MemberAddress(
    val member_address_id: Int?,
    val address_label: String?,
    val recipient_name: String?,
    val address: String?,
    val address_details: String?,
    val recipient_phone:String?,
    val cityordistrict:String?,
    val recipient_postcode:Int?,
    val latitude:String?,
    val longitude:String?
) : BaseCell()

data class AddressRes(
    val `data`: MemberAddress,
    val status: Int,
    val message: String,
) : BaseCell()

data class AddressReq(
    val member_phone: String?,
    val address_label: String?,
    val recipient_name: String?,
    val recipient_phone: String?,
    val recipient_postcode: String?,
    val cityordistrict: String?,
    val address: String?,
    val address_details: String?,
    val latitude: String?,
    val longitude: String?
)

data class AddMemberRes(
    val `data`: MemberAddress,
    val status: Int,
    val message: String,
) : BaseCell()

data class UpdateAddressReq(
    val member_address_id: Int?,
    val member_phone: String?,
    val address_label: String?,
    val recipient_name: String?,
    val recipient_phone: String?,
    val recipient_postcode: String?,
    val cityordistrict: String?,
    val address: String?,
    val address_details: String?,
    val latitude: String?,
    val longitude: String?
)

data class DeleteAddressReq(
    val member_address_id: Int?
)