package com.breadtalk.app.data.repository

import android.app.Application
import com.breadtalk.app.data.remote.api.BreadTalkApi
import com.breadtalk.app.data.remote.helper.ErrorNetworkHandler
import com.breadtalk.app.data.remote.model.req.*
import com.breadtalk.app.data.remote.model.res.*
import io.reactivex.Single
import javax.inject.Inject

interface HomeRepository {
    fun fetchHome(body: HomeReq): Single<HomeRes>
    fun getUpdateNotification(): Single<UpdateVersionRes>
    fun getProductDetail(body: ProductDetailReq): Single<ProductDetailRes>
    fun getProductByCategory(body: ProductByCategoryReq): Single<ProductsByCategoryRes>
    fun getAllCategory(body: ProductCategoryReq): Single<ProductCategoryRes>
    fun getRecentOrder(body: RecentOrderReq): Single<RecentOrderRes>
    fun getProductSearch(body: ProductSearchReq): Single<ProductSearchRes>
    fun setProductFavorite(body: ProductFavoriteReq): Single<SuccessRes>
    fun getProductFavorite(body: ProductFavReq): Single<ProductFavoriteRes>
    fun getHotPromo(body: ProductPromoReq): Single<ProductPromoRes>
    fun getBannerDetail(body: BannerReq): Single<BannerRes>
}

class HomeRepositoryImpl @Inject constructor(
    private val service: BreadTalkApi,
    private val application: Application
) : HomeRepository, BaseRepository() {

    override fun fetchHome(body: HomeReq): Single<HomeRes> =
        composeSingle { service.fetchAppHome(body) }.compose(ErrorNetworkHandler())

    override fun getUpdateNotification(): Single<UpdateVersionRes> =
        composeSingle { service.getUpdateNotification() }.compose(ErrorNetworkHandler())

    override fun getProductDetail(body: ProductDetailReq): Single<ProductDetailRes> =
        composeSingle { service.getProductDetail(body) }.compose(ErrorNetworkHandler())

    override fun getProductByCategory(body: ProductByCategoryReq): Single<ProductsByCategoryRes> =
        composeSingle { service.getProductByCategory(body) }.compose(ErrorNetworkHandler())

    override fun getAllCategory(body: ProductCategoryReq): Single<ProductCategoryRes> =
        composeSingle { service.showAllCategory(body) }.compose(ErrorNetworkHandler())

    override fun getRecentOrder(body: RecentOrderReq): Single<RecentOrderRes> =
        composeSingle { service.showRecentOrder(body) }
            .compose(ErrorNetworkHandler())

    override fun getProductSearch(body: ProductSearchReq): Single<ProductSearchRes> =
        composeSingle { service.showSearchProduct(body) }
            .compose(ErrorNetworkHandler())

    override fun setProductFavorite(body: ProductFavoriteReq): Single<SuccessRes> {
        return composeSingle { service.setProductFavorite(body) }
            .compose(ErrorNetworkHandler())
    }

    override fun getProductFavorite(body: ProductFavReq): Single<ProductFavoriteRes> =
        composeSingle { service.showProductFavorite(body) }
            .compose(ErrorNetworkHandler())

    override fun getHotPromo(body: ProductPromoReq): Single<ProductPromoRes> =
        composeSingle { service.showPromo(body) }.compose(ErrorNetworkHandler())

    override fun getBannerDetail(body: BannerReq): Single<BannerRes> =
        composeSingle {
            service.getBannerDetail(body)
        }
            .compose(ErrorNetworkHandler())
}
