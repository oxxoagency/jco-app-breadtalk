package com.breadtalk.app.data.local

data class HomeHeadSection(val title: String? = "", val point: String? = "") : BaseCell()