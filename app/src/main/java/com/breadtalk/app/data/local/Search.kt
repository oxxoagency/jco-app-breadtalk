package com.breadtalk.app.data.local

data class SearchLast(val id: Int?, val query: String?) : BaseCell()

data class SearchLastHeader(val title: String? = "") : BaseCell()

data class SearchCategoryHeader(val title: String? = "") : BaseCell()

data class SearchCategory(val tags:List<String>, val name:List<String>): BaseCell()