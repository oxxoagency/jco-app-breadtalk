package com.breadtalk.app.data.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.breadtalk.app.data.local.CartDetailVariant
import com.breadtalk.app.data.local.CartProduct
import com.breadtalk.app.data.local.CartProductPickUp
import com.breadtalk.app.data.local.DataConverter

@Database(
    entities = [CartProduct::class, CartDetailVariant::class, CartProductPickUp::class],
    version = 1
)
@TypeConverters(DataConverter::class)
abstract class BreadTalkDatabase : RoomDatabase() {
    abstract fun cartDao(): CartDao
}