package com.breadtalk.app.data.remote.model.res

data class HomeRes(
    val status: Int,
    val message: String,
    val user: User?,
    val promos: List<Promo>?,
    val category: List<Category>,
    val products: List<Product>,
    val version: Version?
)

data class ProductsByCategoryRes(
    val `data`: List<Product>,
    val status: Int
)

data class User(
    val member_name: String?,
    val member_point: String?,
    val member_phone: String?,
    val member_email: String?,
    val status_code: Int?
)

data class Promo(
    val menu_code: String,
    val menu_ordering: String,
    val category_name: String,
    val category_title: String,
    val menu_name: String,
    val menu_name_en: String,
    val menu_desc: String,
    val menu_image: String,
    val min_quantity: String,
    val max_quantity: String,
    val start_date: String,
    val end_date: String,
    val is_promo: String,
    val is_active: String,
    val is_freedelivery: String,
    val menu_amount: String,
    val menu_price: String,
    val menu_URL: String,
    val banner_img: String?,
    val banner_id: String?,
    val banner_description: String?,
    val related_menu: List<String>?
)

data class Category(
    val id:Int?,
    val category_name: String,
    val category_title: String,
    val category_img: String,
    val material: String,
    val is_order: String,
    val is_show: String,
    val is_promo: String
)

data class Product(
    val menu_code: String,
    val menu_ordering: String,
    val category_name: String,
    val category_title: String,
    val menu_name: String,
    val menu_name_en: String,
    val menu_desc: String,
    val menu_image: String,
    val min_quantity: String,
    val max_quantity: String,
    val start_date: String,
    val end_date: String,
    val is_promo: String,
    val is_active: String,
    val is_freedelivery: String,
    val menu_amount: String,
    val menu_price: String,
    val menu_URL: String,
    val is_favorite: String,
    val is_baseprice: Int?
)

data class Version(
    val version_code_latest: String?,
    val version_build_latest: Int?,
    val must_update: Boolean?
)

data class UpdateVersionRes(
    val status: Int?,
    val update_img: String?,
    val update_text: String?
)

data class ProductCategoryRes(
    val `data`: List<Category>,
    val status_code: Int?
)

data class ProductSearchRes(
    val `data`: List<Product>,
    val status_code: Int?
)

data class ProductFavoriteRes(
    val status: Int?,
    val message: String?,
    val data: List<Product>
)

data class ProductPromoRes(
    val status: Int?,
    val message: String?,
    val data: List<Promo>?
)

data class BannerRes(
    val status_code: Int?,
    val data: DataBanner
)

data class DataBanner(
    val banner_id: String?,
    val banner_img: String?,
    val banner_description: String?,
    val start_date: String?,
    val end_date: String?,
    val related_menu: List<RelatedMenuRes>?
)
