package com.breadtalk.app.data.remote.model.req

data class PaymentReq(
    val brand: Int?,
    val transaction_id: String?,
    val member_email: String?,
    val member_phone: String?,
    val fcm_token:String?,
    val fcm_topic:String?
)

data class OrderPaymentReq(
    val brand:Int?,
    val order_id:String?
)