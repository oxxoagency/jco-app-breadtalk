package com.breadtalk.app.data.local

data class LoadingPage(var temp: String = "") : BaseCell()

data class LoadingProductGrid(var temp: String = "") : BaseCell()