package com.breadtalk.app.data.remote.model.res

data class PaymentListRes(
    val status_code: Int?,
    val data: List<PaymentList>
)

data class PaymentList(
    val payment_method: String?,
    val payment_name: String?,
    val payment_icon: String?,
    val ordering: String?
)