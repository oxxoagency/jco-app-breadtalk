package com.breadtalk.app.data.repository

import android.app.Application
import com.breadtalk.app.data.local.*
import com.breadtalk.app.data.remote.api.BreadTalkApi
import com.breadtalk.app.data.remote.helper.ErrorNetworkHandler
import com.breadtalk.app.data.remote.model.res.SuccessRes
import io.reactivex.Single
import javax.inject.Inject

interface DeliveryRepository {
    fun getMemberAddress(body: MemberAddressReq): Single<MemberAddressRes>
    fun getAddress(id: Int): Single<AddressRes>
    fun setAddress(body: AddressReq): Single<AddMemberRes>
    fun updateAddress(body: UpdateAddressReq): Single<SuccessRes>
    fun deleteAddress(body: DeleteAddressReq): Single<SuccessRes>
}

class DeliveryRepositoryImpl @Inject constructor(
    private val service: BreadTalkApi,
    private val application: Application
) : DeliveryRepository, BaseRepository(){

    override fun getMemberAddress(body: MemberAddressReq): Single<MemberAddressRes> =
        composeSingle { service.getMemberAddress(body) }.compose(ErrorNetworkHandler())

    override fun getAddress(id: Int): Single<AddressRes> =
        composeSingle { service.getAddress(id) }
            .compose(ErrorNetworkHandler())

    override fun setAddress(body: AddressReq): Single<AddMemberRes> =
        composeSingle { service.setAddress(body) }
            .compose(ErrorNetworkHandler())

    override fun updateAddress(body: UpdateAddressReq): Single<SuccessRes> =
        composeSingle { service.updateAddress(body) }
            .compose(ErrorNetworkHandler())

    override fun deleteAddress(body: DeleteAddressReq): Single<SuccessRes> {
        return composeSingle { service.deleteAddress(body) }
            .compose(ErrorNetworkHandler())
    }

}