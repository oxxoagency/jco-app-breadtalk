package com.breadtalk.app.data.local

import com.breadtalk.app.data.remote.model.res.DetailVariant

data class ProductDetailContent(
    var menuCode: String?,
    var id_menu: String?,
    val desc: String?,
    val price: Int?,
    val priceText: String?,
    val productName: String?,
    var quantity: Int,
    val menu_image: String?,
    val type: String?,
    var isFavorite:Boolean,
    val variantNum: Int?,
    val idType: Int?,
    val isBasePrice: Int?,
    val variant: List<DetailVariant>?,
) : BaseCell()

data class ProductDetailVariantHeader(val title: String = "", val state: Boolean) : BaseCell()

data class ProductDetailVariantPackageHeader(val name: String, var qty: Int, val menuCode:String) : BaseCell()

data class ProductDetailVariants(
    val variants: List<ProductDetailVariant>?
) : BaseCell()

data class ProductDetailVariant(
    val menuCode: String?,
    val menuName: String?,
    val image: String?,
    val price: Int?,
    val priceText:String?,
    val isShowQty: Boolean?,
    val isShowPrice:Boolean?,
    var qty: Int
) : BaseCell()