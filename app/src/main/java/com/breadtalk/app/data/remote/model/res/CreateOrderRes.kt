package com.breadtalk.app.data.remote.model.res

data class CreateOrderRes(
    val status_code: Int?,
    val data: OrderId,
    val error: Error?,
)

data class OrderId(
    val order_id: Int?
)

data class Error(
    val order_address: String?,
    val order_outlet_code: String?,
    val order_outlet_ID:String?,
    val member_phone2:String?
)