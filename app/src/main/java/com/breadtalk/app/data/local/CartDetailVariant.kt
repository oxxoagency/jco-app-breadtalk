package com.breadtalk.app.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

@Entity
data class CartDetailVariant(
    @PrimaryKey
    val menuCode: String,
    var price: Double?,
    val name: String,
    var qty: Int = 0
)

class DataConverter {

    @TypeConverter
    fun fromDetailVariantList(value: List<CartDetailVariant>): String {
        val gson = Gson()
        val type = object : TypeToken<List<CartDetailVariant>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toCountryLangList(value: String): List<CartDetailVariant> {
        val gson = Gson()
        val type = object : TypeToken<List<CartDetailVariant>>() {}.type
        return gson.fromJson(value, type)
    }
}