package com.breadtalk.app.data.repository

import android.app.Application
import com.breadtalk.app.data.remote.api.BreadTalkPayment
import com.breadtalk.app.data.remote.helper.ErrorNetworkHandler
import com.breadtalk.app.data.remote.model.req.OrderPaymentReq
import com.breadtalk.app.data.remote.model.req.PaymentReq
import com.breadtalk.app.data.remote.model.res.PaymentListRes
import com.breadtalk.app.data.remote.model.res.SuccessPaymentRes
import io.reactivex.Single
import javax.inject.Inject

interface PaymentRepository {
    fun getPaymentList(): Single<PaymentListRes>
    fun getMidtransPayment(body: PaymentReq): Single<SuccessPaymentRes>
    fun orderPayment(body: OrderPaymentReq): Single<SuccessPaymentRes>
}

class PaymentRepositoryImpl @Inject constructor(
    private val service: BreadTalkPayment,
    private val application: Application
) : PaymentRepository, BaseRepository() {
    override fun getPaymentList(): Single<PaymentListRes> =
        composeSingle { service.getPaymentList() }.compose(ErrorNetworkHandler())

    override fun getMidtransPayment(body: PaymentReq): Single<SuccessPaymentRes> =
        composeSingle {
            service.getMidtransPayment(body)
        }
            .compose(ErrorNetworkHandler())

    override fun orderPayment(body: OrderPaymentReq): Single<SuccessPaymentRes> =
        composeSingle { service.orderPayment(body) }.compose(ErrorNetworkHandler())
}
