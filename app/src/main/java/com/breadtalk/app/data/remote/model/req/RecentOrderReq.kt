package com.breadtalk.app.data.remote.model.req

data class RecentOrderReq(
    val brand: Int?,
    val member_email: String?,
    val city: String?
)

data class DirectRecentOrderReq(
    val member_email: String?,
    val order_brand:String?,
    val order_status:Int?,
    val offset:Int?,
    val limit:Int?
)

data class DetailRecentOrderReq(
    val brand:Int?,
    val order_id:String?
)

data class DirectDetailRecentOrderReq(
    val order_id:Int?
)