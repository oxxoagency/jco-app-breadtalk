package com.breadtalk.app.data.local

data class EmptyItem(val title: String? = "") : BaseCell()