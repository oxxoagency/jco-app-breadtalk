package com.breadtalk.app.data.remote.helper

enum class Status {
    LOADING,
    SUCCESS,
    FAILED
}
