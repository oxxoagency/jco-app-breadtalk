package com.breadtalk.app.data.remote.model.res

import com.breadtalk.app.data.local.BaseCell

data class ProductDetailRes(
    val `data`: ProductDetail,
    val message: String,
    val status: Int?,
    val status_code: Int?
)

data class ProductDetail(
    val category_name: String,
    val category_title: String,
    val id_menu: String,
    var is_favorite: String,
    val is_freedelivery: String,
    val is_promo: String,
    val material: String,
    val menu_amount: String,
    val menu_code: String,
    val menu_desc: String,
    val menu_image: List<MenuImage>?,
    val menu_name: String,
    val menu_name_en: String,
    val menu_price: Int?,
    val min_quantity: String,
    val product_id: String,
    val start_date: String,
    val end_date: String,
//    val variant_group: List<VariantGroup>
    val details: List<DetailVariant>?,
    val menu_quantity: String?,
    val menu_detail: String?,
    val order_time: String?,
    val variant_num: String?,
    val id_type: Int?,
    val is_baseprice: Int?
)

data class MenuImage(
    val image: String?
)

data class DetailVariant(
    val package_image: String?,
    val package_name: String?,
    val package_name_en: String,
    val package_code: String?,
    val package_price: Int,
    val package_donut_list: List<PackageList>?
) : BaseCell()

data class PackageList(
    val menu_img: String?,
    val menu_name: String?
) : BaseCell()
