package com.breadtalk.app.data.remote.model.res

data class OutletRes(
    val status_code: Int?,
    val data: List<PickupFrom>,
    val error:String?
)

data class PickupFrom(
    val outlet_id: String?,
    val outlet_code: String,
    val outlet_name: String,
    val outlet_phone: String?,
    val outlet_address: String?,
    val outlet_postcode: String?,
    val outlet_city: String?,
    val outlet_province: String?,
    val outlet_latitude: String?,
    val outlet_longitude: String?,
    val outlet_timezone: String?,
    val distance: String?
)