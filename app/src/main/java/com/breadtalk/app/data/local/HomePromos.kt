package com.breadtalk.app.data.local

data class HomePromos(
    val promos: List<PromoBanner>?
):BaseCell()

data class PromoBanner(
    val menuCode:String?,
    val imgURL: String?
):BaseCell()

data class ModelHotPromo(val menuCode:String?,val img:String, val productName:String, val period:String):BaseCell()