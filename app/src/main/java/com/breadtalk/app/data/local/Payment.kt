package com.breadtalk.app.data.local

data class Payment(val paymentMethod: String?, val paymentName: String?, val paymentIcon: String?): BaseCell()