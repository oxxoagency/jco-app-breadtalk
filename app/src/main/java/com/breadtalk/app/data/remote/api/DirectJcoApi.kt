package com.breadtalk.app.data.remote.api

import com.breadtalk.app.data.remote.model.req.*
import com.breadtalk.app.data.remote.model.res.CouponRes
import com.breadtalk.app.data.remote.model.res.DetailRecentOrderRes
import com.breadtalk.app.data.remote.model.res.RecentOrderRes
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface DirectJcoApi {

    @Headers(
        "Content-Type: application/json"
    )
    @POST("coupon/coupon_list")
    fun showCouponList(
        @Header("Authorization") auth: String,
        @Body body: DirectCouponReq,
    ): Single<CouponRes>

    @POST("order/detail")
    @Headers(
        "Content-Type: application/json"
    )
    fun getOrderDetail(
        @Header("Authorization") auth: String,
        @Body body: DirectDetailRecentOrderReq,
    ): Single<DetailRecentOrderRes>

    @Headers(
        "Content-Type: application/json"
    )
    @POST("order/list")
    fun showRecentOrder(
        @Header("Authorization") auth: String,
        @Body body: DirectRecentOrderReq,
    ): Single<RecentOrderRes>
}