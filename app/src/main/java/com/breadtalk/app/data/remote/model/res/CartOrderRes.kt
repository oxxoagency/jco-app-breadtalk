package com.breadtalk.app.data.remote.model.res

data class CartOrderRes(
    val status_code: Int?,
    val message: String?,
    val error: String?,
    val `data`: CartRes
)

data class CartRes(
    val freeDelivery: Int?,
    val promo: PromoVoucher?,
    val cart_orders: List<CartOrders>,
    val subtotal: Int?,
    val delivery_fee: Int?,
    val grandtotal: Int?,
    val total_delivery_fee: Int?,
    val ecoBag: EcoBag?,
    val invalid_menu: List<InvalidMenu>?,
    val error: ErrorCart
)

data class InvalidMenu(
    val menu_code: String?,
    val menu_name: String?
)

data class ErrorCart(
    val coupon_code: String?
)


data class EcoBag(
    val ecobag_code: String?,
    val ecobag_name: String?,
    val ecobag_quantity: Int?,
    val ecobag_price: String?,
    val ecobag_subtotal: Int?
)

data class PromoVoucher(
    val promo_name: String?,
    val promo_value: Int?
)

data class CartOrders(
    val menu_code: String?,
    val menu_name: String?,
    val menu_quantity: String?,
    val menu_price: Int?,
    val sub_total: Int?
)