package com.breadtalk.app.data.local

data class HomeMenuHeader(val title: String? = "") : BaseCell()