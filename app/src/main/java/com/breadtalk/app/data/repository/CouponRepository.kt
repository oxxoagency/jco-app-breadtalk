package com.breadtalk.app.data.repository

import android.app.Application
import com.breadtalk.app.data.remote.api.BreadTalkApi
import com.breadtalk.app.data.remote.helper.ErrorNetworkHandler
import com.breadtalk.app.data.remote.model.req.CouponClaimReq
import com.breadtalk.app.data.remote.model.req.CouponMemberReq
import com.breadtalk.app.data.remote.model.req.CouponReq
import com.breadtalk.app.data.remote.model.res.CouponRes
import io.reactivex.Single
import javax.inject.Inject

interface CouponRepository {
    fun couponMember(body: CouponMemberReq): Single<CouponRes>
    fun getCouponList(body: CouponReq): Single<CouponRes>
    fun claimCoupon(body: CouponClaimReq): Single<CouponRes>
}

class CouponRepositoryImpl @Inject constructor(
    private val service: BreadTalkApi,
    private val application: Application,
) : CouponRepository, BaseRepository() {
    override fun couponMember(body: CouponMemberReq): Single<CouponRes> =
        composeSingle { service.couponMember(body) }
            .compose(ErrorNetworkHandler())

    override fun getCouponList(body: CouponReq): Single<CouponRes> =
        composeSingle { service.showCouponList(body) }
            .compose(ErrorNetworkHandler())

    override fun claimCoupon(body: CouponClaimReq): Single<CouponRes> =
        composeSingle { service.claimCoupon(body) }
            .compose(ErrorNetworkHandler())
}