package com.breadtalk.app.data.remote.model.req


data class CartOrder(
    val brand: Int?,
    val order_city: String?,
    val delivery_type: Int?,
    val coupon_code: String?,
    val order_payment: String?,
    val order_brand: String?,
    val cart_info: List<CartInfo>
)

data class CartInfo(
    val menu_code: String?,
    val menu_quantity: String?,
    val menu_detail:String?
)

data class CreateOrderDetailReq(
    val menu_code: String?,
    val menu_name: String?,
    val menu_quantity: String?,
    val material_unit_price: String?,
    val material_sub_total: String?,
    val menu_detail: String?
)