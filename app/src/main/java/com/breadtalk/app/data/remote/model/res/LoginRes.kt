package com.breadtalk.app.data.remote.model.res

data class LoginRes(
    val accessToken: String,
    val refreshToken: String,
    val status_code: Int,
    val error: String,
)

data class RefreshTokenRes(
    val status: Int,
    val message:String,
    val access_token:String
)

data class RegisterRes(
    val message: String?,
    val status: Int?,
    val token: Token?,
    val error:String?
)

data class Token(
    val accessToken: String?,
    val refreshToken: String?,
    val status_code: Int?
)

data class MemberRes(
    val `data`: Member,
    val message: String,
    val status_code: Int,
    val error:String?
)

data class Member(
    val member_email: String?,
    val member_name: String?,
    val member_phone: String?,
    val member_phone2: String?,
    val member_point: String?,
    val member_gender:Int?,
    val member_dob:String?
)