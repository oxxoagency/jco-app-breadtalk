package com.breadtalk.app.data.remote.model.req

data class LoginReq(
    val member_loginkey: String,
    val member_password: String
)

data class RegisterReq(
    val member_name: String?,
    val member_email: String?,
    val member_phone: String?,
    val member_phone2: String?,
    val member_password: String?,
    val member_confpassword: String?,
    val member_ip: String?,
    val member_useragent: String?
)

data class UpdateProfileReq(
    val member_name:String?,
    val member_phone:String?,
    val member_phone2:String?,
    val member_gender:Int?,
    val member_dob:String?,
    val member_photo:String
)

data class ChangePasswordReq(
    val member_password: String?,
    val member_newpassword: String?,
    val member_confpassword: String?
)

data class ChangePsswordReq(
    val member_loginkey: String,
    val member_newpassword: String,
    val member_confpassword: String
)