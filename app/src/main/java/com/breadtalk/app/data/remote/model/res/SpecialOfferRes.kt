package com.breadtalk.app.data.remote.model.res

data class RelatedMenuRes(
    val id_menu: String?,
    val menu_code: String?,
    val menu_name: String?,
    val menu_name_en: String?,
    val menu_image: String?,
    val is_promo: String?,
    val menu_price: String?
)