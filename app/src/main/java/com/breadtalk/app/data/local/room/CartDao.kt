package com.breadtalk.app.data.local.room

import androidx.room.*
import com.breadtalk.app.data.local.CartProduct
import com.breadtalk.app.data.local.CartProductPickUp
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface CartDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCart(cartProduct: CartProduct): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCartPickUp(cartProduct: CartProductPickUp): Completable

    @Query("select * from cart_product")
    fun getAllCartProduct(): Flowable<List<CartProduct>>

    @Query("select * from cart_product_pick_up")
    fun getAllCartProductPickUp(): Flowable<List<CartProductPickUp>>

    @Query("update cart_product set priceOriginal = :priceOriginal, price = :price where id = :id")
    fun updatePrice(priceOriginal: Double, price: Double, id: Int) : Completable

    @Query("update cart_product_pick_up set priceOriginal = :priceOriginal, price = :price where id = :id")
    fun updatePricePickUp(priceOriginal: Double, price: Double, id: Int) : Completable

    @Update
    fun updateCart(cartProduct: CartProduct): Completable

    @Update
    fun updateCartPickUp(cartProduct: CartProductPickUp): Completable

    @Delete
    fun deleteCart(cartProduct: CartProduct): Completable

    @Delete
    fun deleteCartPickUp(cartProduct: CartProductPickUp): Completable

    @Query("delete from cart_product")
    fun deleteAll(): Completable

    @Query("delete from cart_product_pick_up")
    fun deleteAllPickUp(): Completable
}