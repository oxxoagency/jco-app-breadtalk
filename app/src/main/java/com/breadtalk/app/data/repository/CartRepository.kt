package com.breadtalk.app.data.repository

import android.app.Application
import com.breadtalk.app.data.remote.api.BreadTalkAdminApi
import com.breadtalk.app.data.remote.helper.ErrorNetworkHandler
import com.breadtalk.app.data.remote.model.req.CartOrder
import com.breadtalk.app.data.remote.model.req.CreateOrderReq
import com.breadtalk.app.data.remote.model.req.DetailRecentOrderReq
import com.breadtalk.app.data.remote.model.res.CartOrderRes
import com.breadtalk.app.data.remote.model.res.CreateOrderRes
import com.breadtalk.app.data.remote.model.res.DetailRecentOrderRes
import io.reactivex.Single
import javax.inject.Inject

interface CartRepository {
    fun getCartOrders(body: CartOrder): Single<CartOrderRes>
    fun getCreateOrder(body: CreateOrderReq): Single<CreateOrderRes>
    fun getTransactionDetail(body: DetailRecentOrderReq): Single<DetailRecentOrderRes>
}

class CartRepositoryImpl @Inject constructor(
    private val service: BreadTalkAdminApi,
    private val application: Application
) : CartRepository, BaseRepository() {

    override fun getCartOrders(body: CartOrder): Single<CartOrderRes> =
        composeSingle { service.getCartOrders(body) }
            .compose(ErrorNetworkHandler())

    override fun getCreateOrder(body: CreateOrderReq): Single<CreateOrderRes> =
        composeSingle { service.createOrder(body) }
            .compose(ErrorNetworkHandler())

    override fun getTransactionDetail(body: DetailRecentOrderReq): Single<DetailRecentOrderRes> =
        composeSingle { service.getOrderDetail(body) }
            .compose(ErrorNetworkHandler())
}