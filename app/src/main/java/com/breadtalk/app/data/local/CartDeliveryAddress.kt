package com.breadtalk.app.data.local

data class CartDeliveryAddress(var address: String, var pickup:String) : BaseCell()