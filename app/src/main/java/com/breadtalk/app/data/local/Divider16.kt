package com.breadtalk.app.data.local

data class Divider16(var temp: String = "") : BaseCell()

data class Line(var temp: String = "") : BaseCell()