package com.breadtalk.app.data.remote.model.req

data class CouponReq(
    val brand: Int,
    val platform: String?,
    val coupon_type: String?,
)

data class DirectCouponReq(
    val platform: String?,
    val coupon_type: String?,
    val order_brand: String?,
)

data class CouponClaimReq(
    val brand: Int?,
    val code: String?,
    val member_phone: String?,
)

data class CouponMemberReq(
    val brand: Int?,
)