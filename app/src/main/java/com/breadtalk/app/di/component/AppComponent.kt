package com.breadtalk.app.di.component

import android.app.Application
import com.breadtalk.app.App
import com.breadtalk.app.di.module.*
import com.breadtalk.app.di.module.DatabaseModule
import com.breadtalk.app.di.scope.ApplicationScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication


@ApplicationScope
@Component(modules = [
    AppModule::class,
    ActivityModule::class,
    NetworkModule::class,
    RepositoryModule::class,
    ViewModelModule::class,
    ApplicationModule::class,
    DatabaseModule::class,
    AndroidSupportInjectionModule::class
])

interface AppComponent : AndroidInjector<DaggerApplication?> {

    fun inject(application: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}