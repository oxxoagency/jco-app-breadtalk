package com.breadtalk.app.di.module


import com.breadtalk.app.di.scope.ApplicationScope
import com.breadtalk.app.utils.AppSchedulerProvider
import com.breadtalk.app.utils.SchedulerProvider
import dagger.Module
import dagger.Provides


@Module
class AppModule {

    @Provides
    @ApplicationScope
    fun provideSchedulerProvider(): SchedulerProvider = AppSchedulerProvider()

}