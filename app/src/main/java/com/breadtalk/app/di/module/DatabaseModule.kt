package com.breadtalk.app.di.module

import android.content.Context
import androidx.room.Room
import com.breadtalk.app.data.local.room.BreadTalkDatabase
import com.breadtalk.app.data.local.room.CartDao
import com.breadtalk.app.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {

    @ApplicationScope
    @Provides
    fun provideWofDatabase(context: Context): BreadTalkDatabase {
        return Room.databaseBuilder(context, BreadTalkDatabase::class.java, "breadtalk_db").build()
    }

    @ApplicationScope
    @Provides
    fun provideProductDao(jcoDatabase: BreadTalkDatabase): CartDao {
        return jcoDatabase.cartDao()
    }
}