package com.breadtalk.app.di.module

import android.app.Application
import com.breadtalk.app.data.remote.api.*
import com.breadtalk.app.data.repository.*
import com.breadtalk.app.di.scope.ApplicationScope
import com.breadtalk.app.utils.SharedPreference
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    @ApplicationScope
    fun provideSharePref(app: Application): SharedPreference {
        return SharedPreference(app.applicationContext)
    }

    @Provides
    @ApplicationScope
    fun provideHomeRepository(service: BreadTalkApi, application: Application): HomeRepository {
        return HomeRepositoryImpl(service, application)
    }

    @Provides
    @ApplicationScope
    fun provideCouponRepository(service: BreadTalkApi, application: Application): CouponRepository {
        return CouponRepositoryImpl(service, application)
    }

    @Provides
    @ApplicationScope
    fun providePickupRepository(service: BreadTalkApi, application: Application): PickupRepository {
        return PickupRepositoryImpl(application, service)
    }

    @Provides
    @ApplicationScope
    fun provideAuthRepository(service: BreadTalkUserApi): AuthRepository {
        return AuthRepositoryImpl(service)
    }

    @Provides
    @ApplicationScope
    fun provideDirectJcoRepository(service: DirectJcoApi): DirectJcoRepository {
        return DirectJcoRepositoryImpl(service)
    }

    @Provides
    @ApplicationScope
    fun provideDeliveryRepository(
        service: BreadTalkApi,
        application: Application,
    ): DeliveryRepository {
        return DeliveryRepositoryImpl(service, application)
    }

    @Provides
    @ApplicationScope
    fun provideCartRepository(
        service: BreadTalkAdminApi,
        application: Application,
    ): CartRepository {
        return CartRepositoryImpl(service, application)
    }

    @Provides
    @ApplicationScope
    fun providePaymentRepository(
        service: BreadTalkPayment,
        application: Application,
    ): PaymentRepository {
        return PaymentRepositoryImpl(service, application)
    }
}