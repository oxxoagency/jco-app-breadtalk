package com.breadtalk.app.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.breadtalk.app.di.module.factory.ViewModelFactory
import com.breadtalk.app.di.scope.ApplicationScope
import com.breadtalk.app.di.scope.ViewModelKey
import com.breadtalk.app.ui.add_address.AddAddressViewModel
import com.breadtalk.app.ui.auth.change_password.ChangePasswordViewModel
import com.breadtalk.app.ui.auth.forgot_password.ForgotPasswordViewModel
import com.breadtalk.app.ui.auth.login.LoginViewModel
import com.breadtalk.app.ui.auth.register.RegisterViewModel
import com.breadtalk.app.ui.contact_us.ContactUsViewModel
import com.breadtalk.app.ui.delivery.DeliveryViewModel
import com.breadtalk.app.ui.detail_special_offer.DetailSpecialOfferViewModel
import com.breadtalk.app.ui.edit_profile.EditProfileViewModel
import com.breadtalk.app.ui.language.LanguageViewModel
import com.breadtalk.app.ui.main.base.MainViewModel
import com.breadtalk.app.ui.main.cart.CartViewModel
import com.breadtalk.app.ui.main.home.HomeViewModel
import com.breadtalk.app.ui.main.profile.ProfileViewModel
import com.breadtalk.app.ui.main.recent_order.RecentOrderViewModel
import com.breadtalk.app.ui.main.wishlist.WishlistViewModel
import com.breadtalk.app.ui.map.MapViewModel
import com.breadtalk.app.ui.payment_method.PaymentMethodViewModel
import com.breadtalk.app.ui.pickup.PickUpViewModel
import com.breadtalk.app.ui.product_detail.ProductDetailViewModel
import com.breadtalk.app.ui.search.SearchViewModel
import com.breadtalk.app.ui.special_offer.SpecialOfferViewModel
import com.breadtalk.app.ui.splash.SplashViewModel
import com.breadtalk.app.ui.transaction_detail.TransactionDetailViewModel
import com.breadtalk.app.ui.voucher.VoucherViewModel
import com.breadtalk.app.ui.voucher.claim_voucher.ClaimVoucherViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @ApplicationScope
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun providesMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun providesSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun providesHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecentOrderViewModel::class)
    internal abstract fun providesRecentOrderViewModel(viewModel: RecentOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CartViewModel::class)
    internal abstract fun providesCartViewModel(viewModel: CartViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WishlistViewModel::class)
    internal abstract fun providesWishlistViewModel(viewModel: WishlistViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    internal abstract fun providesProfileViewModel(viewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    internal abstract fun providesSearchViewModel(viewModel: SearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SpecialOfferViewModel::class)
    internal abstract fun providesSpecialOfferViewModel(viewModel: SpecialOfferViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailSpecialOfferViewModel::class)
    internal abstract fun providesDetailSpecialOfferViewModel(viewModel: DetailSpecialOfferViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailViewModel::class)
    internal abstract fun providesProductDetailViewModel(viewModel: ProductDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransactionDetailViewModel::class)
    internal abstract fun providesTransactionDetailViewModel(viewModel: TransactionDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DeliveryViewModel::class)
    internal abstract fun providesDeliveryViewModel(viewModel: DeliveryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddAddressViewModel::class)
    internal abstract fun providesAddAddressViewModel(viewModel: AddAddressViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun providesLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PaymentMethodViewModel::class)
    internal abstract fun providesPaymentMethodViewModel(viewModel: PaymentMethodViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    internal abstract fun providesRegisterViewModel(viewModel: RegisterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditProfileViewModel::class)
    internal abstract fun providesEditProfileViewModel(viewModel: EditProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChangePasswordViewModel::class)
    internal abstract fun providesChangePasswordViewModel(viewModel: ChangePasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LanguageViewModel::class)
    internal abstract fun providesLanguageViewModel(viewModel: LanguageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    internal abstract fun providesMapViewModel(viewModel: MapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ContactUsViewModel::class)
    internal abstract fun providesContactUsViewModel(viewModel: ContactUsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VoucherViewModel::class)
    internal abstract fun providesVoucherViewModel(viewModel: VoucherViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ClaimVoucherViewModel::class)
    internal abstract fun providesClaimVoucherViewModel(viewModel: ClaimVoucherViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForgotPasswordViewModel::class)
    internal abstract fun providesForgotPasswordViewModel(viewModel: ForgotPasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PickUpViewModel::class)
    internal abstract fun providesPickUpViewModel(viewModel: PickUpViewModel): ViewModel
}