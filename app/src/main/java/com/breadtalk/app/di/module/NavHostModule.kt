package com.breadtalk.app.di.module

import com.breadtalk.app.ui.base.InjectingNavHostFragment
import com.breadtalk.app.ui.base.InjectingNavHostFragmentMain
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class NavHostModule {

    @ContributesAndroidInjector(modules = [FragmentBindingModule::class])
    abstract fun navHostFragmentInjector(): InjectingNavHostFragment

    @ContributesAndroidInjector(modules = [FragmentBindingModule::class])
    abstract fun navHostFragmentInjectorMain(): InjectingNavHostFragmentMain
}