package com.breadtalk.app.di.module

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.breadtalk.app.di.module.factory.InjectingFragmentFactory
import com.breadtalk.app.di.scope.FragmentKey
import com.breadtalk.app.ui.add_address.AddAddressFragment
import com.breadtalk.app.ui.auth.change_password.ChangePasswordFragment
import com.breadtalk.app.ui.auth.forgot_password.ForgotPasswordFragment
import com.breadtalk.app.ui.auth.login.LoginFragment
import com.breadtalk.app.ui.delivery.DeliveryFragment
import com.breadtalk.app.ui.detail_special_offer.DetailSpecialOfferFragment
import com.breadtalk.app.ui.main.base.MainFragment
import com.breadtalk.app.ui.main.cart.CartFragment
import com.breadtalk.app.ui.main.home.HomeFragment
import com.breadtalk.app.ui.main.profile.ProfileFragment
import com.breadtalk.app.ui.main.recent_order.RecentOrderFragment
import com.breadtalk.app.ui.main.wishlist.WishlistFragment
import com.breadtalk.app.ui.payment_method.PaymentMethodFragment
import com.breadtalk.app.ui.product_detail.ProductDetailFragment
import com.breadtalk.app.ui.auth.register.RegisterFragment
import com.breadtalk.app.ui.contact_us.ContactUsFragment
import com.breadtalk.app.ui.edit_profile.EditProfileFragment
import com.breadtalk.app.ui.language.LanguageFragment
import com.breadtalk.app.ui.map.MapFragment
import com.breadtalk.app.ui.pickup.DialogCity
import com.breadtalk.app.ui.pickup.PickUpFragment
import com.breadtalk.app.ui.search.SearchFragment
import com.breadtalk.app.ui.special_offer.SpecialOfferFragment
import com.breadtalk.app.ui.splash.SplashFragment
import com.breadtalk.app.ui.transaction_detail.TransactionDetailFragment
import com.breadtalk.app.ui.voucher.VoucherFragment
import com.breadtalk.app.ui.voucher.claim_voucher.ClaimVoucherFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class FragmentBindingModule {

    @Binds
    abstract fun bindFragmentFactory(factory: InjectingFragmentFactory): FragmentFactory

    @Binds
    @IntoMap
    @FragmentKey(MainFragment::class)
    abstract fun bindMainFragment(mainFragment: MainFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SplashFragment::class)
    abstract fun bindSplashFragment(splashFragment: SplashFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(HomeFragment::class)
    abstract fun bindHomeFragment(homeFragment: HomeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RecentOrderFragment::class)
    abstract fun bindRecentOrderFragment(recentOrderFragment: RecentOrderFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CartFragment::class)
    abstract fun bindCartFragment(cartFragment: CartFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(WishlistFragment::class)
    abstract fun bindWishlistFragment(wishlistFragment: WishlistFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ProfileFragment::class)
    abstract fun bindProfileFragment(profileFragment: ProfileFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SearchFragment::class)
    abstract fun bindSearchFragment(searchFragment: SearchFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SpecialOfferFragment::class)
    abstract fun bindSpecialOfferFragment(specialOfferFragment: SpecialOfferFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DetailSpecialOfferFragment::class)
    abstract fun bindDetailSpecialOfferFragment(detailSpecialOfferFragment: DetailSpecialOfferFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ProductDetailFragment::class)
    abstract fun bindProductDetailFragment(productDetailFragment: ProductDetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransactionDetailFragment::class)
    abstract fun bindTransactionDetailFragment(transactionDetailFragment: TransactionDetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DeliveryFragment::class)
    abstract fun bindDeliveryFragment(deliveryFragment: DeliveryFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AddAddressFragment::class)
    abstract fun bindAddAddressFragment(addressFragment: AddAddressFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LoginFragment::class)
    abstract fun bindLoginFragment(loginFragment: LoginFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PaymentMethodFragment::class)
    abstract fun bindPaymentMethodFragment(paymentMethodFragment: PaymentMethodFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RegisterFragment::class)
    abstract fun bindRegisterFragment(registerFragment: RegisterFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EditProfileFragment::class)
    abstract fun bindEditProfileFragment(editProfileFragment: EditProfileFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ChangePasswordFragment::class)
    abstract fun bindChangePasswordFragment(changePasswordFragment: ChangePasswordFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LanguageFragment::class)
    abstract fun bindLanguageFragment(languageFragment: LanguageFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MapFragment::class)
    abstract fun bindMapFragment(mapFragment: MapFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ContactUsFragment::class)
    abstract fun bindContactUsFragment(contactUsFragment: ContactUsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VoucherFragment::class)
    abstract fun bindVoucherFragment(voucherFragment: VoucherFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ClaimVoucherFragment::class)
    abstract fun bindClaimVoucherFragment(claimVoucherFragment: ClaimVoucherFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ForgotPasswordFragment::class)
    abstract fun bindForgotPasswordFragment(forgotPasswordFragment: ForgotPasswordFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DialogCity::class)
    abstract fun bindDialogCity(fragment: DialogCity): BottomSheetDialogFragment

    @Binds
    @IntoMap
    @FragmentKey(PickUpFragment::class)
    abstract fun bindPickUpFragment(pickUpFragment: PickUpFragment): Fragment
}