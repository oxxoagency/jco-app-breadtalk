package com.breadtalk.app.ui.main.recent_order

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.R
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.EmptyItem
import com.breadtalk.app.data.local.LoadingPage
import com.breadtalk.app.data.local.RecentOrder
import com.breadtalk.app.data.remote.model.req.DirectRecentOrderReq
import com.breadtalk.app.data.repository.DirectJcoRepository
import com.breadtalk.app.data.repository.HomeRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.Converter
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import javax.inject.Inject

class RecentOrderViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val directJcoRepository: DirectJcoRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val app: Application,
) : BaseViewModel(), RecentOrderListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _showDetail = MutableLiveData<SingleEvents<RecentOrder>>()
    val showDetail: LiveData<SingleEvents<RecentOrder>>
        get() = _showDetail

    fun loadData() {
        val temp = mutableListOf<BaseCell>()
        sharedPreference.removeCodeVoucher()
        sharedPreference.removeVoucher()
        temp.add(LoadingPage())
        _datas.value = temp
        sharedPreference.removeCodeVoucher()
        sharedPreference.removeVoucher()
        val auth = sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN)?.let {
            "Bearer $it"
        }
        val body = DirectRecentOrderReq(sharedPreference.loadEmail(), "BT", 0, 0, 10)
        lastDisposable = directJcoRepository.getRecentOrder(auth ?: "", body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                Log.d("cekrecentorder",response.toString())
                temp.removeAll { it is LoadingPage }
                if (response.data.isNullOrEmpty()) {
                    temp.add(EmptyItem(app.getString(R.string.you_havent_ordered)))
                }


                response.data?.map { product ->
                    temp.add(
                        RecentOrder(
                            orderId = product.order_id,
                            outletName = product.order_outlet_code,
                            menuImage = product.menu_img,
                            orderTime = product.order_time,
                            orderDetail = product.order_detail,
                            price = Converter.rupiah(product.order_total.toString()),
                            menuName = product.menu_list,
                            orderStatus = product.order_status,
                            totalOrder = product.order_total_item,
                            orderPaymentStatus = product.order_payment_status
                        )
                    )

                }
                _datas.postValue(temp)

            }, {
                Log.d("cekrecentorder",it.toString())
                temp.removeAll { it is LoadingPage }
                temp.add(EmptyItem(app.getString(R.string.you_havent_ordered)))
                _datas.postValue(temp)
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }


    override fun onRecentOrderClick(position: Int) {
        _datas.value.let {
            _showDetail.value = SingleEvents(it?.get(position) as RecentOrder)
        }
    }

    override fun onReOrderClick(position: Int) {

    }
}