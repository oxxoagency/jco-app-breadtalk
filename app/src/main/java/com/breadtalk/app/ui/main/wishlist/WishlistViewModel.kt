package com.breadtalk.app.ui.main.wishlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.local.*
import com.breadtalk.app.data.remote.model.req.ProductFavReq
import com.breadtalk.app.data.remote.model.req.ProductFavoriteReq
import com.breadtalk.app.data.remote.model.res.Product
import com.breadtalk.app.data.repository.HomeRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.ui.main.home.HomeControllerListener
import com.breadtalk.app.utils.Converter
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import javax.inject.Inject

class WishlistViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
) : BaseViewModel(), HomeControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _openProductDetail = MutableLiveData<SingleEvents<HomeMenuItem>>()
    val openProductDetail: LiveData<SingleEvents<HomeMenuItem>>
        get() = _openProductDetail

    fun loadData() {
        val body = ProductFavReq(sharedPreference.loadPhoneNumber().toString(), "Jakarta Barat", 3)
        lastDisposable = homeRepository.getProductFavorite(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ data ->
                val temp = mutableListOf<BaseCell>()

                if (data.data.isNullOrEmpty()) {
                    temp.add(EmptyItem("Wishlist belum ditambahkan"))
                }

                data.data.map { model ->
                    temp.add(
                        HomeMenuItem(
                            name = localizationMenuName(model),
                            imgURL = model.menu_image,
                            price = model.menu_price,
                            priceText = Converter.rupiah(model.menu_price),
                            isStartFrom = false,
                            isPromo = model.is_promo == "1",
                            isFreeDelivery = model.is_freedelivery == "1",
                            isFavorite = model.is_favorite == "1",
                            menuCode = model.menu_code,
                            isShowBasePrice = model.is_baseprice == 1
                        )
                    )
                }

                _datas.value = temp


            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun localizationMenuName(product: Product): String =
        if (sharedPreference.loadLanguage()
                ?.contains("indonesia", true) == true
        ) product.menu_name
        else product.menu_name_en

    override fun onMenuItemClick(index: Int) {
        _datas.value?.let {
            _openProductDetail.value = SingleEvents(it[index] as HomeMenuItem)
        }
    }

    override fun onSearchClick() {

    }

    override fun onPromoSeeAllClick() {

    }

    override fun onPromosItemClick(promoBanner: PromoBanner) {

    }

    override fun onMenuItemFavoriteClick(index: Int) {
        _datas.value?.let {
            val temp = (it[index] as HomeMenuItem).copy()
            val favorite = if (temp.isFavorite) "0" else "1"
            val body = ProductFavoriteReq(
                favorite,
                sharedPreference.loadPhoneNumber().toString(),
                temp.menuCode
            )
            lastDisposable = homeRepository.setProductFavorite(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ model ->
                    temp.isFavorite = !temp.isFavorite
                    it[index] = temp
                    _datas.postValue(it)
                }, {
                    handleError(it)
                })

            lastDisposable?.let { compositeDisposable.add(it) }
        }
    }

    override fun onMenuCategoryClick(menuCategory: MenuCategory) {

    }

    override fun onSearchMenuCategoryClick(menuSearchTagName: SearchCategory, position: Int) {

    }

    override fun onSearchItem(query: String) {

    }
}