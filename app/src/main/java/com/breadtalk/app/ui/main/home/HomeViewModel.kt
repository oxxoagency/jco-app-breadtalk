package com.breadtalk.app.ui.main.home

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.R
import com.breadtalk.app.data.local.*
import com.breadtalk.app.data.remote.model.req.HomeReq
import com.breadtalk.app.data.remote.model.req.ProductByCategoryReq
import com.breadtalk.app.data.remote.model.req.ProductFavoriteReq
import com.breadtalk.app.data.remote.model.res.HomeRes
import com.breadtalk.app.data.remote.model.res.Product
import com.breadtalk.app.data.remote.model.res.UpdateVersionRes
import com.breadtalk.app.data.remote.model.res.User
import com.breadtalk.app.data.repository.AuthRepository
import com.breadtalk.app.data.repository.HomeRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.Converter
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val authRepository: AuthRepository,
    private val app: Application,
) : BaseViewModel(), HomeControllerListener {

    val menuSelected = MutableLiveData<String>()

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _showSearch = MutableLiveData<SingleEvents<String>>()
    val showSearch: LiveData<SingleEvents<String>>
        get() = _showSearch

    private var _showAllPromo = MutableLiveData<SingleEvents<String>>()
    val showALlPromo: LiveData<SingleEvents<String>>
        get() = _showAllPromo

    private var _showDetailPromo = MutableLiveData<SingleEvents<String>>()
    val showDetailPromo: LiveData<SingleEvents<String>>
        get() = _showDetailPromo

    private var _showProductDetail = MutableLiveData<SingleEvents<HomeMenuItem>>()
    val showProductDetail: LiveData<SingleEvents<HomeMenuItem>>
        get() = _showProductDetail

    private val _showUpdatePopUpMandatory = MutableLiveData<SingleEvents<String>>()
    val showUpdatePopUpMandatory: LiveData<SingleEvents<String>>
        get() = _showUpdatePopUpMandatory

    private val _showUpdatePopUp = MutableLiveData<SingleEvents<String>>()
    val showUpdatePopUp: LiveData<SingleEvents<String>>
        get() = _showUpdatePopUp

    private var _updateVersion = MutableLiveData<UpdateVersionRes>()
    val updateVersion: LiveData<UpdateVersionRes>
        get() = _updateVersion

    private val _showDialogCannotOrder = MutableLiveData<SingleEvents<String>>()
    val showDialogCannotOrder: LiveData<SingleEvents<String>>
        get() = _showDialogCannotOrder

    fun fetchHomeFromAPI() {
        val temp = mutableListOf<BaseCell>()
        temp.add(LoadingPage())
        _datas.value = temp
        val pInfo = app.packageManager.getPackageInfo(app.packageName, 0)
        val version = pInfo.versionName
        val body = HomeReq(
            3,
            "Jakarta Barat",
            sharedPreference.loadPhoneNumber(),
            "android",
            pInfo.versionCode
        )
        lastDisposable = homeRepository.fetchHome(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                parseFetchHome(model)
                checkTokenExpired(model.user)
                if (model.version?.version_code_latest?.equals(version) == false) {
                    if (model.version.must_update == true) {
                        _showUpdatePopUpMandatory.value =
                            SingleEvents("show-update-pop-up-mandatory")
                        updateVersion()
                    } else {
                        _showUpdatePopUp.value = SingleEvents("show-update-pop-up")
                        updateVersion()
                    }

                }
                //save json to sharepreference
//                val data = gson.toJson(model)
//                sharedPreference.save(SharedPreference.DATA_HOME, data)
            }, {
                handleError(it)
                Log.d("errorhome", "errorhome")
            })

        lastDisposable?.let { compositeDisposable.add(it) }
        menuSelected.postValue("")

    }

    private fun checkTokenExpired(user: User?) {
        if (user?.status_code == 401) {
            refreshToken()
        } else {
            sharedPreference.savePhoneNumber(
                user?.member_phone ?: sharedPreference.loadPhoneNumber()
            )
            sharedPreference.savePoints(user?.member_point)
            sharedPreference.saveEmail(user?.member_email)
            sharedPreference.saveName(user?.member_name)
        }
    }

    private fun refreshToken() {
        lastDisposable = authRepository.getRefreshToken()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                if (it.status == 200) {
                    sharedPreference.save(SharedPreference.ACCESS_TOKEN, it.access_token)
                }
            }, {

            })
    }

    private fun parseFetchHome(model: HomeRes) {
        val temp = mutableListOf<BaseCell>()
        temp.removeAll { it is LoadingPage }
        if (isLoggedIn()) {
            temp.add(HomeHeadSection(model.user?.member_name, model.user?.member_point))
        } else {
            temp.add(Divider16())
            temp.add(Divider16())
            temp.add(Divider16())
        }

        temp.add(HomePromoHeader("test"))
        val promos = mutableListOf<PromoBanner>()
        model.promos?.map {
            promos.add(PromoBanner(it.banner_id.toString(), it.banner_img.toString()))
        }
        temp.add(HomePromos(promos))

        temp.add(HomeMenuHeader(app.getString(R.string.what_are_you_looking_for)))

        val menus = mutableListOf<MenuCategory>()
        model.category.map {
            if (it.category_name == "all") {
                menus.add(
                    MenuCategory(
                        it.category_name, it.category_title, it.category_img,
                        true
                    )
                )
                menuSelected.postValue(it.category_name)
            } else {
                menus.add(
                    MenuCategory(
                        it.category_name, it.category_title, it.category_img,
                        false
                    )
                )
            }
        }

        temp.add(HomeMenuCategories(menus))

        model.products.map {
            temp.add(
                HomeMenuItem(
                    localizationMenuName(it),
                    it.menu_image,
                    it.menu_price,
                    Converter.thousandSeparator(it.menu_price),
                    false,
                    it.is_promo == "1",
                    it.is_freedelivery == "1",
                    it.is_favorite == "1",
                    it.menu_code,
                    it.is_baseprice == 1
                )
            )
        }
        _datas.value = temp
    }

    private fun localizationMenuName(product: Product): String =
        if (sharedPreference.loadLanguage()?.contains("indonesia", true) == true) product.menu_name
        else product.menu_name_en

    private fun updateVersion() {
        lastDisposable = homeRepository.getUpdateNotification()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                _updateVersion.value = it
            }, {

            })
    }

    private fun getProductByCategory(category: String) {
        datas.value?.let {
            it.removeAll { data -> data is HomeMenuItem }
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            _datas.value = it
        }

        val body = ProductByCategoryReq(
            category,
            "Jakarta Barat",
            sharedPreference.loadPhoneNumber() ?: "0",
            3
        )
        lastDisposable = homeRepository.getProductByCategory(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                if (model.status == 200) {
                    val temp: MutableList<BaseCell> =
                        datas.value?.toMutableList() ?: mutableListOf()
                    temp.removeAll { it is LoadingProductGrid }

                    model.data.map {
                        temp.add(
                            HomeMenuItem(
                                localizationMenuName(it),
                                it.menu_image,
                                it.menu_price,
                                Converter.thousandSeparator(it.menu_price),
                                false,
                                it.is_promo == "1",
                                it.is_freedelivery == "1",
                                it.is_favorite == "1",
                                it.menu_code,
                                it.is_baseprice == 1
                            )
                        )
                    }
                    _datas.value = temp
                }
            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onMenuItemClick(index: Int) {
        datas.value?.let {
            _showProductDetail.value = SingleEvents(it[index] as HomeMenuItem)
        }

    }

    override fun onSearchClick() {
        _showSearch.value = SingleEvents("show_search")
    }

    override fun onPromoSeeAllClick() {

        _showAllPromo.value = SingleEvents("show-all-promo")
    }

    override fun onPromosItemClick(promoBanner: PromoBanner) {
        _showDetailPromo.value = SingleEvents(promoBanner.menuCode.toString())
    }

    override fun onMenuItemFavoriteClick(index: Int) {
        if (isLoggedIn()) {
            _datas.value?.let {
                val temp = (it[index] as HomeMenuItem).copy()
                val favorite = if (temp.isFavorite) "0" else "1"
                val body = ProductFavoriteReq(
                    favorite,
                    sharedPreference.loadPhoneNumber().toString(),
                    temp.menuCode
                )
                lastDisposable = homeRepository.setProductFavorite(body)
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
                    .subscribe({ model ->
                        temp.isFavorite = !temp.isFavorite
                        it[index] = temp
                        _datas.postValue(it)
                    }, {
                        handleError(it)
                    })

                lastDisposable?.let { compositeDisposable.add(it) }
            }
        } else {
            showDlgCannotOrder()
        }
    }

    override fun onMenuCategoryClick(menuCategory: MenuCategory) {
        if (menuCategory.name != menuSelected.value) {
            menuSelected.postValue(menuCategory.name)
            if (menuCategory.name != "all") {
                getProductByCategory(menuCategory.name)
            } else {
                getProductByCategory("")
            }
        }

    }

    override fun onSearchMenuCategoryClick(menuSearchTagName: SearchCategory, position: Int) {

    }

    override fun onSearchItem(query: String) {

    }

    private fun showDlgCannotOrder() {
        _showDialogCannotOrder.value = SingleEvents("_showDialogCannotOrder")
    }

    private fun isLoggedIn(): Boolean {
        val data = sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN)
        return data != null && data != ""
    }
}