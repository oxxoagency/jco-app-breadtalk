package com.breadtalk.app.ui.product_detail

import com.breadtalk.app.data.local.ProductDetailVariant

interface ProductDetailControllerListener {
    fun onPlusClick(position: Int)
    fun onMinusClick(position: Int)
    fun onPlusDetailVariantClick(position: Int)
    fun onMinusDetailVariantClick(position: Int)
    fun onAddToCart()
    fun onBtnFavoriteClick()
    fun onPhotoClick(productDetailVariant: ProductDetailVariant)
    fun onPlusVariantClick(position: Int)
    fun onMinusVariantClick(position: Int)
}