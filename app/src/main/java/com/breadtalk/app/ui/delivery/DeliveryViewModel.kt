package com.breadtalk.app.ui.delivery

import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.local.*
import com.breadtalk.app.data.local.room.BreadTalkDatabase
import com.breadtalk.app.data.remote.model.req.OutletReq
import com.breadtalk.app.data.remote.model.req.ProductDetailReq
import com.breadtalk.app.data.repository.DeliveryRepository
import com.breadtalk.app.data.repository.HomeRepository
import com.breadtalk.app.data.repository.PickupRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class DeliveryViewModel @Inject constructor(
    private val deliveryRepository: DeliveryRepository,
    private val pickupRepository: PickupRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val database: BreadTalkDatabase,
    private val productRepository: HomeRepository,
) : BaseViewModel(), DeliveryControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _addNewAddress = MutableLiveData<SingleEvents<String>>()
    val addNewAddress: LiveData<SingleEvents<String>>
        get() = _addNewAddress

    private val _showLoading = MutableLiveData<SingleEvents<Boolean>>()
    val showLoading: LiveData<SingleEvents<Boolean>>
        get() = _showLoading

    private val _openCart = MutableLiveData<SingleEvents<String>>()
    val openCart: LiveData<SingleEvents<String>>
        get() = _openCart

    private val _openAddAddress = MutableLiveData<SingleEvents<AddressItem>>()
    val openAddAddress: LiveData<SingleEvents<AddressItem>>
        get() = _openAddAddress

    fun cartProduct(): Flowable<List<CartProduct>> =
        database.cartDao().getAllCartProduct()

    private fun updatePrice(priceOriginal: Double, price: Double, id: Int): Completable =
        database.cartDao().updatePrice(priceOriginal, price, id)

    fun loadData() {
        val body = MemberAddressReq(sharedPreference.loadPhoneNumber().toString())
        lastDisposable = deliveryRepository.getMemberAddress(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                val temp = mutableListOf<BaseCell>()
                model.data?.map {
                    temp.add(
                        AddressItem(
                            id = it.member_address_id,
                            labelAddress = it.address_label,
                            address = it.address,
                            detailAddress = it.address_details,
                            phoneNumber = it.recipient_phone,
                            name = it.recipient_name,
                            city = it.cityordistrict,
                            latitude = it.latitude,
                            longitude = it.longitude,
                            postCode = it.recipient_postcode
                        )
                    )
                }

                temp.add(SavePlace(""))
                _datas.value = temp
            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun getItemCarts(city: String) {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .take(1)
            .subscribe({
                it.forEach { cartProduct ->
                    if (cartProduct.isBasePrice == 0) {
                        loadProductDetail(
                            cartProduct.menuCode.toString(),
                            city,
                            cartProduct.qty,
                            cartProduct.id,
                            mutableListOf()
                        )
                    } else {
                        loadProductDetail(
                            cartProduct.menuCode.toString(),
                            city,
                            cartProduct.qty,
                            cartProduct.id,
                            cartProduct.detailVariant ?: return@subscribe
                        )

                    }
                }
            }, {

            })
        Handler(Looper.getMainLooper()).postDelayed({
            _showLoading.value = SingleEvents(false)
            _openCart.value = SingleEvents("open-cart")
        }, 1000)
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun doUpdateCart(priceOriginal: Double, price: Double, id: Int) {
        lastDisposable =
            updatePrice(priceOriginal, price, id)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe()
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun loadProductDetail(
        menuCode: String,
        city: String,
        qty: Int,
        id: Int,
        cartDetailVariant: MutableList<CartDetailVariant>,
    ) {
        val body = ProductDetailReq(
            "3",
            city,
            menuCode,
            sharedPreference.loadPhoneNumber().toString()
        )

        lastDisposable = productRepository.getProductDetail(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                val data = response.data
                var price: Int?
                if (response.status == 200) {
                    if (data.is_baseprice == 0) {
                        price = data.menu_price?.times(qty)
                        doUpdateCart(
                            data.menu_price?.toDouble() ?: 0.0,
                            price?.toDouble() ?: 0.0,
                            id
                        )
                    } else {
                        var priceBasePrice = 0
                        data.details?.map { detailVariant ->
                            cartDetailVariant.map { cartDetailVariant ->
                                if (detailVariant.package_code == cartDetailVariant.menuCode) {
                                    priceBasePrice += detailVariant.package_price.times(
                                        cartDetailVariant.qty)
                                    Log.d("cekprice", priceBasePrice.toString())
                                }
                            }

                        }
                        doUpdateCart(
                            data.menu_price?.toDouble() ?: 0.0,
                            priceBasePrice.toDouble(),
                            id
                        )

                    }
                }
            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun loadLocation(lat: String, lng: String) {
        val body = OutletReq(3, lat, lng)
        lastDisposable = pickupRepository.getOutletLocation(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                if (it.status_code == 200) {
                    sharedPreference.saveOutletIdForDelivery(it.data[0].outlet_id.toString())
                    sharedPreference.saveOutletCodeForDelivery(it.data[0].outlet_code)
                    sharedPreference.savePickUpAddress(it.data[0].outlet_address.toString())
                } else {

                }
            }, {

            })
    }

    override fun onClick(index: Int) {

    }

    override fun onFavoriteClick(index: Int) {

    }

    override fun onClickUpdateAddress(index: Int) {
        _datas.value?.let {
            _openAddAddress.value = SingleEvents(it[index] as AddressItem)
        }
    }

    override fun onClickAddAddress() {
        _addNewAddress.value = SingleEvents("add-address")
    }

    override fun onClickSaveDeliveryAddress(index: Int) {
        _datas.value.let {
            _showLoading.value = SingleEvents(true)
            val address = it?.get(index) as AddressItem
            sharedPreference.saveAddress(address.address.toString())
            sharedPreference.saveDetailAddress(address.detailAddress.toString())
            sharedPreference.saveSelectCity(address.city.toString())
            sharedPreference.saveSelectLatitudeAddress(address.latitude)
            sharedPreference.saveSelectLongitudeAddress(address.longitude)
            sharedPreference.saveSelectPhoneNumber(address.phoneNumber.toString())
            sharedPreference.saveOrderPostCodeForDelivery(address.postCode.toString())
            sharedPreference.saveRecipientName(address.name.toString())
            loadLocation(address.latitude.toString(), address.longitude.toString())
            getItemCarts(sharedPreference.loadSelectCity().toString())
        }
    }

    override fun onDeleteAddress(index: Int) {
        _datas.value.let { datas ->
            val address = datas?.get(index) as AddressItem
            lastDisposable = deliveryRepository.deleteAddress(DeleteAddressReq(address.id))
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
                    datas.removeAll { data -> data is AddressItem }
                    loadData()
                }, {

                })
        }
        lastDisposable?.let { compositeDisposable.add(it) }
    }
}
