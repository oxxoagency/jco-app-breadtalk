package com.breadtalk.app.ui.auth.login

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentLoginBinding
import com.breadtalk.app.ui.base.BaseFragment
import com.breadtalk.app.utils.DialogError
import com.breadtalk.app.utils.KeyboardUtil
import javax.inject.Inject
import android.content.Intent
import android.net.Uri
import com.bumptech.glide.Glide


class LoginFragment @Inject constructor() : BaseFragment<FragmentLoginBinding, LoginViewModel>() {

    override fun getViewModelClass(): Class<LoginViewModel> = LoginViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_login

    override fun onViewReady(savedInstance: Bundle?) {
        sharedPreference.apply {
            removeAddress()
            removeCity()
            removePickUp()
            removeSelectCity()
            removeSelectLatitudeAddress()
            removeSelectLongitudeAddress()
            removeSelectPhoneNumber()
        }

        binding.viewModel = viewModel
        binding.executePendingBindings()
        KeyboardUtil(requireActivity(), binding.root)
        binding.btnForgotPassword.setOnClickListener {
            navigateTo(R.string.linkForgot)
        }
        binding.btnRegister.setOnClickListener {
            navigateTo(R.string.linkRegister)
        }
        binding.btnHelpCenter.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://order.btdelivery.com/help/android"))
            startActivity(browserIntent)
        }

        binding.btnLogin.setOnClickListener {
            binding.edtPhoneNo.error = null
            binding.edtPassword.error = null
            viewModel.login(
                binding.edtPhoneNo.editText?.text.toString(),
                binding.edtPassword.editText?.text.toString()
            )
        }

        Glide.with(binding.imgJag.context)
            .load("https://cdn.app.jcodelivery.com/img/jagroup.jpg")
            .into(binding.imgJag)

        try {
            val pInfo =
                requireContext().packageManager.getPackageInfo(requireContext().packageName, 0)
            val version = pInfo.versionName
            binding.versionName.text = "V$version(${pInfo.versionCode})"
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        initObserver()
    }

    override fun onBackPress() {
        goToMainPage()
    }

    private fun initObserver() {
        viewModel.errorPhone.observe(this, {
            it.getContentIfNotHandled()?.let { error ->
                binding.edtPhoneNo.error = error
            }
        })
        viewModel.errorPassword.observe(this, {
            it.getContentIfNotHandled()?.let { error ->
                binding.edtPassword.error = error
            }
        })

        viewModel.closeLoginPage.observe(this, {
            it.getContentIfNotHandled()?.let {
                goToMainPage()
            }
        })

        viewModel.loginFailed.observe(this, {
            it.getContentIfNotHandled()?.let { message ->
                val dlg = DialogError(requireContext())
                dlg.showPopup(
                    message
                ) {
                    dlg.dismissPopup()
                }
            }
        })
    }

    private fun goToMainPage() {
        KeyboardUtil.hideKeyboard(requireActivity())
        navigatePopupInclusiveTo(R.id.loginFragment, R.string.linkMainFragment)
    }

}