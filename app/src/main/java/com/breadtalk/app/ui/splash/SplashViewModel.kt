package com.breadtalk.app.ui.splash

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.repository.AuthRepository
import com.breadtalk.app.data.repository.HomeRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.EspressoIdlingResource
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val app: Application
) : BaseViewModel() {


    private val _complete = MutableLiveData<SingleEvents<Boolean>>()
    val complete: LiveData<SingleEvents<Boolean>>
        get() = _complete

    lateinit var disposable: Disposable

    fun loadData() {
        lastDisposable = authRepository.getRefreshToken()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                countSplash()
                if (it.status == 200) {
                    sharedPreference.save(SharedPreference.ACCESS_TOKEN, it.access_token)
                }
            }, {
                _complete.value = SingleEvents(true)
            })
    }

    private fun countSplash() {
        disposable = Observable.timer(3, TimeUnit.SECONDS)
            .doOnSubscribe { EspressoIdlingResource.increment() }
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally {
                if (!EspressoIdlingResource.getIdlingResource().isIdleNow) {
                    EspressoIdlingResource.decrement()
                }
            }
            .subscribe {
                _complete.value = SingleEvents(true)
            }
    }
}