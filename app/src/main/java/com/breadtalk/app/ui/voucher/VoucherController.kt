package com.breadtalk.app.ui.voucher

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.EmptyItem
import com.breadtalk.app.data.local.Voucher
import com.breadtalk.app.data.local.VoucherHeader
import com.breadtalk.app.emptyItem
import com.breadtalk.app.voucherHeader
import com.breadtalk.app.voucherItems

class VoucherController(private val listener: VoucherControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is VoucherHeader -> addCouponHeader(listener)
                is Voucher -> addCouponList(cellData, index, listener)
                is EmptyItem -> addEmptyCart(cellData)
            }
        }
    }

    private fun addCouponList(cellData: Voucher, index: Int, listener: VoucherControllerListener) {
        voucherItems {
            id(cellData.id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addCouponHeader(listener: VoucherControllerListener) {
        voucherHeader {
            id("header")
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyItem) {
        emptyItem {
            id("empty-cart")
            data(cellData)
        }
    }
}