package com.breadtalk.app.ui.search

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.*
import com.breadtalk.app.data.local.*
import com.breadtalk.app.ui.main.home.HomeControllerListener

class SearchController(private val listener: HomeControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var menuSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var showSearchLatest = false
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
//                is SearchLastHeader -> addSearchLastHeader()
//                is SearchLast -> addSearchLast(cellData)
                is SearchCategoryHeader -> addSearchCategoryHeader()
                is SearchCategory -> addSearchCategory(cellData, listener, index)
                is HomeMenuItem -> addHomeMenuItem(cellData, listener, index)
                is LoadingProductGrid -> addLoadingProductGrid(cellData)
            }
        }
    }

    private fun addSearchLast(cellData: SearchLast) {
        searchLast {
            id(cellData.id)
            data(cellData)
            showSearchLatest(showSearchLatest)
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addSearchLastHeader() {
        searchLastHeader {
            id("search-last-header")
            showSearchLatest(showSearchLatest)
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addSearchCategoryHeader() {
        searchHeaderCategory {
            id("search-category-header")
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addSearchCategory(
        cellData: SearchCategory,
        listener: HomeControllerListener,
        position: Int
    ) {
        searchCategory {
            id(cellData.tags[position])
            position(position)
            data(cellData)
            listener(listener)
            selected(cellData.name[position] == menuSelected)
            position(position)
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addHomeMenuItem(
        cellData: HomeMenuItem,
        listener: HomeControllerListener,
        index: Int
    ) {
        homeMenuItem {
            id(cellData.menuCode)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addLoadingProductGrid(cellData: LoadingProductGrid) {
        lytLoadingProductGrid {
            id(cellData.hashCode())
        }
    }
}