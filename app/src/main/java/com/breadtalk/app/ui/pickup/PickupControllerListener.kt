package com.breadtalk.app.ui.pickup
interface PickupControllerListener {
    fun onClick(index: Int)
    fun onSearchClick()
}