package com.breadtalk.app.ui.voucher.claim_voucher

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.claimVoucherItems
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.EmptyItem
import com.breadtalk.app.data.local.Voucher
import com.breadtalk.app.emptyItem

class ClaimVoucherController(private val listener: ClaimVoucherControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is Voucher -> addCouponList(cellData, index, listener)
                is EmptyItem -> addEmptyCart(cellData)
            }
        }
    }

    private fun addCouponList(
        cellData: Voucher,
        index: Int,
        listener: ClaimVoucherControllerListener,
    ) {
        claimVoucherItems {
            id(cellData.id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyItem) {
        emptyItem {
            id("empty-cart")
            data(cellData)
        }
    }
}