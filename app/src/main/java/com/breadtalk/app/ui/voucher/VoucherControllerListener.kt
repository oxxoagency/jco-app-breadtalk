package com.breadtalk.app.ui.voucher

interface VoucherControllerListener {
    fun onUsedVoucherClick(position:Int)
    fun moreVoucherClick()
}