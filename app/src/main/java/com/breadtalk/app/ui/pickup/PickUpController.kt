package com.breadtalk.app.ui.pickup

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.LocationSearch
import com.breadtalk.app.data.local.PickupItem
import com.breadtalk.app.locationSearchview
import com.breadtalk.app.pickupItem

class PickUpController(private val listener: PickupControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is LocationSearch -> addLocationSearch(listener)
                is PickupItem -> addPickupItem(cellData, listener, index)
            }
        }
    }

    private fun addLocationSearch(listener: PickupControllerListener) {
        locationSearchview {
            id("locationSearch")
            listener(listener)
        }
    }

    private fun addPickupItem(
        cellData: PickupItem,
        listener: PickupControllerListener,
        index: Int,
    ) {
        pickupItem {
            id(cellData._id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }
}