package com.breadtalk.app.ui

import android.os.Bundle
import com.breadtalk.app.R
import com.breadtalk.app.databinding.ActivityMainBinding
import com.breadtalk.app.ui.base.BaseActivity
import com.breadtalk.app.utils.SharedPreference

class MainActivity : BaseActivity<ActivityMainBinding>() {

    private lateinit var sharedPreference: SharedPreference

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onViewReady(savedInstance: Bundle?) {
        sharedPreference = SharedPreference(this)
//        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
//            if (!task.isSuccessful) {
//                Log.w("TAG", "Fetching FCM registration token failed", task.exception)
//                return@OnCompleteListener
//            }
//
//            // Get new FCM registration token
//            val token = task.result
//            sharedPreference.saveFcmToken(token)
//
//            // Log and toast
//            Log.d("TAG", token)
//        })
    }
}