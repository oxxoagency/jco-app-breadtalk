package com.breadtalk.app.ui.splash

import android.os.Bundle
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentSplashBinding
import com.breadtalk.app.ui.base.BaseFragment
import javax.inject.Inject


class SplashFragment @Inject constructor() :
    BaseFragment<FragmentSplashBinding, SplashViewModel>() {

    override fun getViewModelClass(): Class<SplashViewModel> = SplashViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_splash

    override fun onViewReady(savedInstance: Bundle?) {
        viewModel.loadData()
        viewModel.complete.observe(this, {
            val action = SplashFragmentDirections.actionToMainFragment("")
            findNavController().navigate(
                action,
                FragmentNavigator.Extras.Builder()
                    .build()
            )
        })
    }
}