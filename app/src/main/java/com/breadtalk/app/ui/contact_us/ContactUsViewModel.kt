package com.breadtalk.app.ui.contact_us

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.R
import com.breadtalk.app.data.local.*
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.ui.main.profile.ProfileControllerListener
import com.breadtalk.app.utils.SingleEvents
import javax.inject.Inject

class ContactUsViewModel @Inject constructor() : BaseViewModel(), ProfileControllerListener {

    val datas = MutableLiveData<MutableList<BaseCell>>()

    private val _showMenuDetail = MutableLiveData<SingleEvents<Int>>()
    val showMenuDetail: LiveData<SingleEvents<Int>>
        get() = _showMenuDetail


    init {
        datas.value = mutableListOf()
    }

    fun loadData() {
        datas.value?.let {
            it.add(ProfileMenuHeader("Kontak"))
            it.add(ProfileMenu(R.drawable.ic_web, R.string.website_jco, WEB))
            it.add(ProfileMenu(R.drawable.ic_message, R.string.email_jco, EMAIL))
            it.add(ProfileMenu(R.drawable.ic_call, R.string.number_jco, PHONE))
            it.add(ProfileMenuHeader("Social Media"))
            it.add(SocmedMenu(R.drawable.ic_facebook, "BreadTalk Indonesia", FACEBOOK))
            it.add(SocmedMenu(R.drawable.ic_instagram, "@breadtalkindo", INSTAGRAM))
            it.add(SocmedMenu(R.drawable.ic_twitter, "@BreadtalkIndo", TWITTER))
            it.add(ProfileMenuHeader("Alamat"))
            it.add(AddressMenu("Mal Kelapa Gading 1 & 2 Lantai Dasar Unit G-140 C & D \n Jalan Boulevar Kelapa Gading \n Kelapa Gading \n Jakarta Utara"))
            it.add(ProfileMenuHeader("LAYANAN PENGADUAN KONSUMEN"))
            it.add(AddressMenu("DITJEN PERLINDUNGAN KONSUMEN & TERTIB NIAGA KEMENDAG RI \n 0853-1111-1010"))
            datas.postValue(it)
        }
    }

    override fun onMenuClick(index: Int) {
        datas.value?.let {
            val data = (it[index] as ProfileMenu).copy()
            _showMenuDetail.value = SingleEvents(data.type)

        }
    }

    override fun onSignOut() {

    }

    override fun onEditProfile() {

    }

    override fun onSocMedMnu(index: Int) {
        datas.value?.let {
            val data = (it[index] as SocmedMenu).copy()
            _showMenuDetail.value = SingleEvents(data.type)

        }
    }

    companion object {
        const val WEB = 1
        const val EMAIL = 2
        const val PHONE = 3
        const val FACEBOOK = 4
        const val INSTAGRAM = 5
        const val TWITTER = 5
    }
}