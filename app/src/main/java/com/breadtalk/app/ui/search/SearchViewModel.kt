package com.breadtalk.app.ui.search

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.local.*
import com.breadtalk.app.data.remote.model.req.ProductByCategoryReq
import com.breadtalk.app.data.remote.model.req.ProductCategoryReq
import com.breadtalk.app.data.remote.model.req.ProductFavoriteReq
import com.breadtalk.app.data.remote.model.req.ProductSearchReq
import com.breadtalk.app.data.remote.model.res.Product
import com.breadtalk.app.data.repository.HomeRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.ui.main.home.HomeControllerListener
import com.breadtalk.app.utils.Converter
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference
) : BaseViewModel(), HomeControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _showSearchLatest = MutableLiveData<Boolean>()
    val showSearchLatest: LiveData<Boolean>
        get() = _showSearchLatest

    private val _openProductDetail = MutableLiveData<SingleEvents<HomeMenuItem>>()
    val openProductDetail: LiveData<SingleEvents<HomeMenuItem>>
        get() = _openProductDetail

    val menuSelected = MutableLiveData<String>()

    fun loadData() {
        lastDisposable = homeRepository.getAllCategory(ProductCategoryReq(3, "Jakarta Barat"))
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ data ->

                val temp = mutableListOf<BaseCell>()
                temp.add(
                    SearchCategory(
                        data.data.map { category ->
                            category.category_title
                        },
                        data.data.map { category ->
                            category.category_name
                        },
                    )
                )
                _datas.postValue(temp)

            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

     fun loadSearch(search: String?) {
        _datas.value.let {
            it?.removeAll { data -> data is HomeMenuItem }
            it?.add(LoadingProductGrid())
            it?.add(LoadingProductGrid())
            it?.add(LoadingProductGrid())
            it?.add(LoadingProductGrid())
            it?.add(LoadingProductGrid())
            it?.add(LoadingProductGrid())
            lastDisposable =
                homeRepository.getProductSearch(ProductSearchReq(3, "Jakarta Barat", search))
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
                    .subscribe({ data ->
                        it?.removeAll { loading -> loading is LoadingProductGrid }
                        data.data.map { product ->
                            it?.add(
                                HomeMenuItem(
                                    name = localizationMenuName(product),
                                    imgURL = product.menu_image,
                                    price = product.menu_price,
                                    priceText = Converter.rupiah(product.menu_price),
                                    isStartFrom = false,
                                    isFreeDelivery = product.is_freedelivery == "1",
                                    isFavorite = product.is_favorite == "1",
                                    menuCode = product.menu_code,
                                    isPromo = product.is_promo == "1",
                                    isShowBasePrice = product.is_baseprice == 1
                                )
                            )
                        }
                        _datas.postValue(it)
                    }, {

                    })
        }
    }

    private fun getProductByCategory(category: String) {
        datas.value?.let {
            it.removeAll { data -> data is HomeMenuItem }
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            it.add(LoadingProductGrid())
            _datas.value = it
        }

        val body = ProductByCategoryReq(
            category,
            "Jakarta Barat",
            sharedPreference.loadPhoneNumber() ?: "",
            3
        )
        lastDisposable = homeRepository.getProductByCategory(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                if (model.status == 200) {
                    val temp: MutableList<BaseCell> =
                        datas.value?.toMutableList() ?: mutableListOf()
                    temp.removeAll { it is LoadingProductGrid }

                    model.data.map {
                        temp.add(
                            HomeMenuItem(
                                localizationMenuName(it),
                                it.menu_image,
                                it.menu_price,
                                Converter.rupiah(it.menu_price),
                                false,
                                it.is_promo == "1",
                                it.is_freedelivery == "1",
                                false,
                                it.menu_code,
                                isShowBasePrice = it.is_baseprice == 1
                            )
                        )
                    }
                    _datas.value = temp
                }
            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun localizationMenuName(product: Product): String =
        if (sharedPreference.loadLanguage()?.contains("indonesia", true) == true) product.menu_name
        else product.menu_name_en

    fun doShowSearchLatest(hasFocus: Boolean) {
        _showSearchLatest.value = hasFocus
    }

    override fun onMenuItemClick(index: Int) {
        _datas.value?.let {
            _openProductDetail.value = SingleEvents(it[index] as HomeMenuItem)
        }
    }

    override fun onSearchClick() {

    }

    override fun onPromoSeeAllClick() {

    }

    override fun onPromosItemClick(promoBanner: PromoBanner) {

    }

    override fun onMenuItemFavoriteClick(index: Int) {
        _datas.value?.let {
            val temp = (it[index] as HomeMenuItem).copy()
            val favorite = if (temp.isFavorite) "0" else "1"
            val body = ProductFavoriteReq(
                favorite,
                sharedPreference.loadPhoneNumber().toString(),
                temp.menuCode
            )
            lastDisposable = homeRepository.setProductFavorite(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ model ->
                    temp.isFavorite = !temp.isFavorite
                    it[index] = temp
                    _datas.postValue(it)
                }, {
                    handleError(it)
                })

            lastDisposable?.let { compositeDisposable.add(it) }
        }
    }

    override fun onMenuCategoryClick(menuCategory: MenuCategory) {

    }

    override fun onSearchMenuCategoryClick(menuSearchTagName: SearchCategory, position: Int) {
        if (menuSearchTagName.name[position] != menuSelected.value) {
            menuSelected.postValue(menuSearchTagName.name[position])
            if (menuSearchTagName.name[position] != "all") {
                getProductByCategory(menuSearchTagName.name[position])
            } else {
                getProductByCategory("")
            }
        }
        Log.d("cekclicked", "cekclicked")
    }

    override fun onSearchItem(query: String) {

    }
}