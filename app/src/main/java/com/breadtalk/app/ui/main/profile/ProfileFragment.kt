package com.breadtalk.app.ui.main.profile

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentProfileBinding
import com.breadtalk.app.ui.base.BaseFragment
import javax.inject.Inject


class ProfileFragment @Inject constructor() :
    BaseFragment<FragmentProfileBinding, ProfileViewModel>() {

    override fun getViewModelClass(): Class<ProfileViewModel> = ProfileViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_profile

    override fun onViewReady(savedInstance: Bundle?) {
        binding.topBar.btnBack.visibility = View.GONE
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = ProfileController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.setData(it)
        })
    }

    private fun initObserver() {
        viewModel.clearData.observe(this, {
            it.getContentIfNotHandled().let {
                context?.cacheDir?.deleteRecursively()
            }
        })

        viewModel.showMenuDetail.observe(this, {
            it.getContentIfNotHandled()?.let { index ->
                when (index) {
                    ProfileViewModel.CHANGE_PASSWORD -> {
                        navigateTo(R.string.linkChangePasswordFragment)
                    }
                    ProfileViewModel.ORDER -> {
                        val action =
                            ProfileFragmentDirections.actionProfileToRecentOrder()
                        findNavController()
                            .navigate(
                                action,
                                FragmentNavigator.Extras.Builder()
                                    .build()
                            )
                    }
                    ProfileViewModel.LANGUAGE -> {
                        navigateTo(R.string.linkLanguageFragment)
                    }
                    ProfileViewModel.CONTACT_US -> {
                        navigateTo(R.string.linkContactUsFragment)
                    }
                    ProfileViewModel.EDIT -> {
                        navigateTo(R.string.linkEditProfileFragment)
                    }
                }
            }
        })
    }
}