package com.breadtalk.app.ui.voucher.claim_voucher

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.EmptyItem
import com.breadtalk.app.data.local.Voucher
import com.breadtalk.app.data.remote.model.req.CouponClaimReq
import com.breadtalk.app.data.remote.model.req.DirectCouponReq
import com.breadtalk.app.data.repository.CouponRepository
import com.breadtalk.app.data.repository.DirectJcoRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import com.breadtalk.app.utils.parseDateWithFormat
import javax.inject.Inject

class ClaimVoucherViewModel @Inject constructor(
    private val couponRepository: CouponRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val directJcoRepository: DirectJcoRepository,
) : BaseViewModel(), ClaimVoucherControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _claimVoucher = MutableLiveData<SingleEvents<String>>()
    val claimVoucher: LiveData<SingleEvents<String>>
        get() = _claimVoucher

    fun loadData() {
        val body = DirectCouponReq("2", "1", "BT")
        val auth = sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN)?.let {
            "Bearer $it"
        }
        val temp = mutableListOf<BaseCell>()
        lastDisposable = directJcoRepository.getCouponList(auth.toString(), body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                val data = it.data
                if (it.status_code == 200) {
                    data.map { coupon ->
                        temp.add(
                            Voucher(
                                id = coupon.coupon_id,
                                name = coupon.coupon_name,
                                image = coupon.image,
                                date = "${coupon.start_time?.parseDateWithFormat("dd MMM yyyy")} - ${
                                    coupon.end_time?.parseDateWithFormat("dd MMM yyyy")
                                }",
                                description = coupon.description?.replace("\\n", "\n"),
                                code = coupon.code,
                                isClaim = coupon.is_claim,
                                isUsed = coupon.is_used
                            )
                        )
                    }
                } else {
                    temp.add(EmptyItem(it.error.toString()))
                }
                _datas.postValue(temp)
            }, {
                handleError(it)
                temp.add(EmptyItem("Kupon tidak ditemukan"))
                Log.d("cekcoupon","Kupon tidak ditemukan")
                _datas.postValue(temp)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onClaimClick(position: Int) {
        _datas.value.let {
            val coupon = (it?.get(position) as Voucher)

            val body = CouponClaimReq(3, coupon.code, sharedPreference.loadPhoneNumber())
            lastDisposable = couponRepository.claimCoupon(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
                    if (coupon.isClaim == 0) {
                        _claimVoucher.value = SingleEvents("claim-voucher")
                    }
                }, {
                    handleError(it)
                })

        }
        lastDisposable?.let { compositeDisposable.add(it) }
    }
}