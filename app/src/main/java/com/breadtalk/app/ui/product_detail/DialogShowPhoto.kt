package com.breadtalk.app.ui.product_detail

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import com.breadtalk.app.R
import com.breadtalk.app.databinding.DlgShowPhotoBinding
import com.bumptech.glide.Glide

class DialogShowPhoto(private val context: Context) {
    var dialog: Dialog? = null

    lateinit var binding: DlgShowPhotoBinding

    init {
        dialog = Dialog(context, R.style.AppTheme_AppCompat_Dialog_Alert_NoFloating)
    }

    fun showPopup(
        image: String,
        onClickListener: View.OnClickListener
    ) {

        binding = DlgShowPhotoBinding.inflate(LayoutInflater.from(context))
        Glide.with(binding.image.context)
            .load(image)
            .into(binding.image)
        binding.close.setOnClickListener(onClickListener)
        //initializing dialog screen

        dialog?.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(false)
        dialog?.setContentView(binding.root)
//        dialog?.window!!.attributes.windowAnimations = R.style.Theme_MaterialComponents_BottomSheetDialog
        dialog?.show()

    }

    fun dismissPopup() = dialog?.let { dialog?.dismiss() }

}