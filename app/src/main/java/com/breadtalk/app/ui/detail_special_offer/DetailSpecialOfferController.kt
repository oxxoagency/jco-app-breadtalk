package com.breadtalk.app.ui.detail_special_offer

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.DetailSpecialOffer
import com.breadtalk.app.detailSpecialOffer

class DetailSpecialOfferController(private val listener: DetailSpecialOfferControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is DetailSpecialOffer -> addDetailSpecialOffer(cellData, listener, index)
            }
        }
    }

    private fun addDetailSpecialOffer(
        cellData: DetailSpecialOffer,
        listener: DetailSpecialOfferControllerListener,
        position: Int
    ) {
        detailSpecialOffer {
            id(cellData.id)
            data(cellData)
            listener(listener)
            position(position)
        }
    }
}