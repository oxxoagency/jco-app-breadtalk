package com.breadtalk.app.ui.contact_us

import com.airbnb.epoxy.EpoxyAsyncUtil
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.TypedEpoxyController
import com.breadtalk.app.contactUsAddress
import com.breadtalk.app.data.local.*
import com.breadtalk.app.profileMenu
import com.breadtalk.app.profileMenuHeader
import com.breadtalk.app.socmedMenu
import com.breadtalk.app.ui.main.profile.ProfileControllerListener

/**
 * Showcases [EpoxyController] with sticky header support
 */
class ContactUsController(
    private val listener: ProfileControllerListener,
) : TypedEpoxyController<List<BaseCell>>(
    EpoxyAsyncUtil.getAsyncBackgroundHandler(),
    EpoxyAsyncUtil.getAsyncBackgroundHandler()
) {

    override fun buildModels(data: List<BaseCell>?) {
        data?.forEachIndexed() { index, cellData ->
            when (cellData) {
                is ProfileMenuHeader -> addProfileMenuHeader(cellData)
                is ProfileMenu -> addProfileMenu(cellData, index, listener)
                is SocmedMenu -> addSocmedMenu(cellData, index, listener)
                is AddressMenu -> addAddress(cellData)
            }
        }
    }

    private fun addProfileMenuHeader(cellData: ProfileMenuHeader) {
        profileMenuHeader {
            id("delivery_address")
            data(cellData)
        }
    }

    private fun addProfileMenu(
        cellData: ProfileMenu,
        index: Int,
        listener: ProfileControllerListener,
    ) {
        profileMenu {
            id("pickup_address")
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addSocmedMenu(
        cellData: SocmedMenu,
        index: Int,
        listener: ProfileControllerListener,
    ) {
        socmedMenu {
            id("pickup_address")
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addAddress(cellData: AddressMenu) {
        contactUsAddress {
            id("address")
            data(cellData)
        }
    }

}
