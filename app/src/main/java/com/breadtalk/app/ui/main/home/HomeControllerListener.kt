package com.breadtalk.app.ui.main.home

import com.breadtalk.app.data.local.HomeMenuItem
import com.breadtalk.app.data.local.MenuCategory
import com.breadtalk.app.data.local.PromoBanner
import com.breadtalk.app.data.local.SearchCategory

interface HomeControllerListener {
    fun onMenuItemClick(index: Int)
    fun onSearchClick()
    fun onPromoSeeAllClick()
    fun onPromosItemClick(promoBanner: PromoBanner)
    fun onMenuItemFavoriteClick(index: Int)
    fun onMenuCategoryClick(menuCategory: MenuCategory)
    fun onSearchMenuCategoryClick(menuSearchTagName: SearchCategory, position: Int)
    fun onSearchItem(query:String)
}