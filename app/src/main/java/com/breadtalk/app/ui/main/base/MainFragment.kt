package com.breadtalk.app.ui.main.base

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentMainBinding
import com.breadtalk.app.ui.base.BaseFragment
import javax.inject.Inject


class MainFragment @Inject constructor() : BaseFragment<FragmentMainBinding, MainViewModel>() {

    override fun getViewModelClass(): Class<MainViewModel> = MainViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_main

    override fun onViewReady(savedInstance: Bundle?) {
        viewModel.loginCheck()
        initObserver()
        setUpNavigation()
    }

    private fun setUpNavigation() {
        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.navHostFragmentMain) as NavHostFragment

        val navController = navHostFragment.navController
        binding.bottomNavigation.setupWithNavController(navController)
        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            NavigationUI.onNavDestinationSelected(item, navController)
        }

        arguments?.let {
            if (it.getString("menu").equals("cart")) {
                binding.bottomNavigation.selectedItemId = R.id.cart
                it.putString("menu", "")
            } else if (it.getString("menu").equals("profile")) {
                binding.bottomNavigation.selectedItemId = R.id.profile
                it.putString("menu", "")
            }
        }
    }

    private fun initObserver() {
        viewModel.showBottomNavigation.observe(this, {
            it.getContentIfNotHandled()?.let {
                binding.bottomNavigation.visibility = View.VISIBLE
            }
        })
        viewModel.hideBottomNavigation.observe(this, {
            it.getContentIfNotHandled()?.let {
                binding.bottomNavigation.visibility = View.GONE
            }
        })


        viewModel.sizeCart.observe(viewLifecycleOwner, {
            val badge = binding.bottomNavigation.getOrCreateBadge(R.id.cart)
            badge.isVisible = it > 0
            badge.number = it
        })
    }

    fun backToHome() {
        binding.bottomNavigation.selectedItemId = R.id.home
    }
}