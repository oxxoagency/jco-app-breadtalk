package com.breadtalk.app.ui.transaction_detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.local.*
import com.breadtalk.app.data.local.room.BreadTalkDatabase
import com.breadtalk.app.data.remote.model.req.DirectDetailRecentOrderReq
import com.breadtalk.app.data.repository.CartRepository
import com.breadtalk.app.data.repository.DirectJcoRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.*
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class TransactionDetailViewModel @Inject constructor(
    private val cartRepository: CartRepository,
    private val schedulers: SchedulerProvider,
    private val directJcoRepository: DirectJcoRepository,
    private val database: BreadTalkDatabase,
    private val sharedPreference: SharedPreference,
) : BaseViewModel(),
    TransactionDetailControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas


    private val _onCopy = MutableLiveData<SingleEvents<String>>()
    val onCopy: LiveData<SingleEvents<String>>
        get() = _onCopy

    private val _onCopyVa = MutableLiveData<SingleEvents<String>>()
    val onCopyVa: LiveData<SingleEvents<String>>
        get() = _onCopyVa

    private val _showHotline = MutableLiveData<SingleEvents<String>>()
    val showHotline: LiveData<SingleEvents<String>>
        get() = _showHotline

    private val _addToCart = MutableLiveData<SingleEvents<String>>()
    val addToCart: LiveData<SingleEvents<String>>
        get() = _addToCart

    private fun insertToCart(cartProduct: CartProduct): Completable =
        database.cartDao().insertCart(cartProduct)

    private fun insertToCartPickUp(cartProduct: CartProductPickUp): Completable =
        database.cartDao().insertCartPickUp(cartProduct)

    private fun doInsertToCart(cartProduct: CartProduct) {
        compositeDisposable.add(
            insertToCart(cartProduct).subscribeOn(schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    handleError(it)
                })
        )
    }

    private fun doInsertToCartPickUp(cartProduct: CartProductPickUp) {
        compositeDisposable.add(
            insertToCartPickUp(cartProduct).subscribeOn(schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    handleError(it)
                })
        )
    }

    fun loadData(id: String) {
        val auth = sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN)?.let {
            "Bearer $it"
        }
        lastDisposable =
            directJcoRepository.getTransactionDetail(auth ?: "",
                DirectDetailRecentOrderReq(id.toInt()))
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ response ->
                    Log.d("cektransactiondetail", response.toString())
                    val temp = mutableListOf<BaseCell>()
                    val date = response.data.order_time?.parseDateAndTimeWithFormat(
                        "dd MMM yyyy"
                    )
                    val time = response.data.order_time?.parseDateAndTimeWithFormat("HH:mm")
                    val vaNumber = response.data.order_payment?.gw_va_numbers
                    val sdf = SimpleDateFormat("HH:mm").parse(time.toString())
                    val calendar = Calendar.getInstance()
                    calendar.time = sdf
                    calendar.add(Calendar.HOUR, 1)
                    val timePlus = calendar.time.parseToTime()
                    temp.add(
                        TransactionDetailHeader(
                            response.data.order_status.toString(),
                            response.data.order_payment_status
                        )
                    )
                    temp.add(
                        TransactionDetailShippingInformation(
                            id = response.data.order_id,
                            orderAddress = response.data.order_address,
                            orderInfo = response.data.order_addressinfo,
                            outletAddress = response.data.order_outlet_address,
                            outletName = response.data.order_outlet_name,
                            deliveryType = response.data.delivery_type,
                            memberPhone = "Hp : ${response.data.member_phone}",
                            outletPhone = response.data.outlet_phone
                        )
                    )
                    temp.add(
                        TransactionDetailOrderSummary(
                            orderId = response.data.order_id,
                            purchaseDate = response.data.order_time?.parseDateAndTimeWithFormat(
                                "dd MMM yyyy"
                            ),
                            vaNumber = response.data.order_payment?.gw_va_numbers,
                            paymentName = response.data.order_payment?.payment_name,
                            orderStatus = response.data.order_status,
                            instructionBCA = "<b>Harap melakukan pembayaran sebelum $timePlus $date </b>\n" +
                                    "\n" +
                                    "<b><big>CARA PEMBAYARAN VIRTUAL ACCOUNT BCA:\n</big></b>" +
                                    "\n" +
                                    "<big>Cara pembayaran via ATM BCA\n</big>" +
                                    "1. Pada menu utama, pilih \"Transaksi Lainnya\"\n" +
                                    "2. Pilih \"Transfer\"\n" +
                                    "3. Pilih ke Rek \"BCA Virtual Account\"\n" +
                                    "4. Masukkan nomor $vaNumber lalu tekan \"Benar\"\n" +
                                    "5. Pada halaman konfirmasi transfer akan muncul detail pembayaran Anda. Jika informasi telah sesuai tekan \"Ya\"\n" +
                                    "\n" +
                                    "\n" +
                                    "<big>Cara pembayaran via Klik BCA\n</big>" +
                                    "1. Pilih menu \"Transfer Dana\"\n" +
                                    "2. Pilih \"Transfer ke BCA Virtual Account\"\n" +
                                    "3. Masukkan nomor BCA Virtual Account $vaNumber \n" +
                                    "4. Jumlah yang akan ditransfer, nomor rekening dan nama merchant akan muncul di halaman konfirmasi pembayaran, jika informasi benar klik \"Lanjutkan\"\n" +
                                    "5. Masukkan respon KEYBCA APPLI 1 yang muncul pada Token BCA Anda, lalu klik tombol \"Kirim\"\n" +
                                    "6. Transaksi Anda selesai\n" +
                                    "\n" +
                                    "\n" +
                                    "<big>Cara pembayaran via m-BCA\n</big>" +
                                    "1. Pilih \"m-Transfer\"\n" +
                                    "2. Pilih \"Transfer\"\n" +
                                    "3. Pilih \"BCA Virtual Account\"\n" +
                                    "4. Pilih nomor rekening yang akan digunakan untuk pembayaran\n" +
                                    "5. Masukkan nomor BCA Virtual Account $vaNumber, lalu pilih \"OK\"\n" +
                                    "6. Nomor BCA Virtual Account dan nomor Rekening Anda akan terlihat di halaman konfirmasi rekening\n" +
                                    "7. Pilih \"OK\" pada halaman konfirmasi pembayaran\n" +
                                    "8. Masukkan PIN BCA untuk mengotorisasi pembayaran\n" +
                                    "9. Transaksi Anda selesai",
                            instructionMandiri = "<b>Harap melakukan pembayaran sebelum $timePlus $date \n</b>" +
                                    "\n" +
                                    "<b><big>CARA PEMBAYARAN MANDIRI BILL PAYMENT\n</big></b>" +
                                    "\n" +
                                    "<big>Pembayaran melalui ATM Mandiri:\n</big>" +
                                    "1. Masukkan PIN Anda\n" +
                                    "2. Pada menu utama pilih menu \"Pembayaran\" kemudian pilih menu \"Multi Payment\"\n" +
                                    "3. Masukan \"Kode Perusahaan\" dengan angka 70012\n" +
                                    "4. Masukan Kode Pembayaran $vaNumber \n" +
                                    "5. Konfirmasi pembayaran Anda\n" +
                                    "\n" +
                                    "\n" +
                                    "<big>Cara membayar melalui Internet Banking Mandiri:\n</big>" +
                                    "1. Login ke Mandiri Internet Banking\n" +
                                    "2. Di Menu Utama silakan pilih \"Bayar\" kemudian pilih \"Multi Payment\"\n" +
                                    "3. Pilih akun anda di \"Dari Rekening\", kemudian di \"Penyedia Jasa\" pilih midtrans\n" +
                                    "4. Masukkan Kode Pembayaran $vaNumber dan klik \"Lanjutkan\"\n" +
                                    "5. Konfirmasi pembayaran anda menggunakan Mandiri Token",
                            note = response.data.order_note,
                            sendDate = response.data.order_send?.parseDateAndTimeWithFormat(
                                "dd MMM yyyy"
                            ),
                            isDelivery = response.data.delivery_type?.contains("0") == true
                        )
                    )
                    response.data.order_detail.map { detail ->
                        temp.add(
                            TransactionDetailProduct(
                                nameProduct = detail.menu_name,
                                qty = detail.menu_quantity,
                                menuCode = detail.menu_code,
                                unitPrice = Converter.rupiah(detail.menu_unitprice.toString()),
                                price = Converter.rupiah(detail.menu_price.toString()),
                                menuDetail = detail.menu_detail
                            )
                        )
                    }
                    temp.add(
                        TransactionDetailTotal(
                            subtotal = Converter.rupiah(
                                response.data.order_subtotal?.toDouble()
                                    ?: 0.0
                            ),
                            deliveryFee = Converter.rupiah(
                                response.data.order_fee?.toDouble() ?: 0.0
                            ),
                            ecobag = response.data.ecoBag?.ecobag_name,
                            ecobagPrice = Converter.rupiah(response.data.ecoBag?.ecobag_price.toString()),
                            total = Converter.rupiah(
                                response.data.order_total?.toDouble()
                                    ?: 0.0
                            ),
                            paymentName = response.data.order_payment?.payment_name,
                            paymentIcon = response.data.order_payment?.payment_icon,
                            orderDetail = response.data.order_detail,
                            freeDeliveryText = Converter.rupiah(
                                response.data.freeDelivery?.toDouble() ?: 0.0
                            ),
                            freeDelivery = response.data.freeDelivery,
                            promo = response.data.order_promo,
                            promoText = Converter.rupiah(response.data.order_promo?.toDouble()
                                ?: 0.0),
                            isDelivery = response.data.delivery_type?.contains("0") == true
                        )
                    )
                    _datas.postValue(temp)
                }, {
                    handleError(it)
                })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onReOrderClick(position: Int) {
        _datas.value.let {
            val details = (it?.get(position) as TransactionDetailTotal).copy()
            details.orderDetail.map { detail ->
                if (detail.menu_name?.contains("eco bag", true) == false) {
                    doInsertToCart(
                        CartProduct(
                            menuCode = detail.menu_code,
                            name = detail.menu_name,
                            imgURL = detail.menu_img,
                            price = detail.menu_price?.toDouble() ?: 0.0,
                            productType = "",
                            qty = detail.menu_quantity?.toInt() ?: 0,
                            priceOriginal = detail.menu_unitprice?.toDouble() ?: 0.0,
                            detailVariant = mutableListOf(),
                            variantname = "",
                            idType = 0,
                            isBasePrice = 0,
                            menuDetail = ""
                        )
                    )

                    doInsertToCartPickUp(
                        CartProductPickUp(
                            menuCode = detail.menu_code,
                            name = detail.menu_name,
                            imgURL = detail.menu_img,
                            price = detail.menu_price?.toDouble() ?: 0.0,
                            productType = "",
                            qty = detail.menu_quantity?.toInt() ?: 0,
                            priceOriginal = detail.menu_unitprice?.toDouble() ?: 0.0,
                            detailVariant = mutableListOf(),
                            variantname = "",
                            idType = 0,
                            isBasePrice = 0,
                            menuDetail = ""
                        )
                    )
                }
            }
            _addToCart.value = SingleEvents("add-to-cart")
            _datas.value = it
        }
    }

    override fun onCopyClick(transactionDetailOrderSummary: TransactionDetailOrderSummary) {
        _datas.value.let {
            _onCopy.value = SingleEvents(transactionDetailOrderSummary.orderId.toString())
        }
    }

    override fun onCopyVAClick(transactionDetailOrderSummary: TransactionDetailOrderSummary) {
        _datas.value.let {
            _onCopyVa.value = SingleEvents(transactionDetailOrderSummary.vaNumber.toString())
        }
    }

    override fun onHotlineClick() {
        _showHotline.value = SingleEvents("show-hotline")
    }
}
