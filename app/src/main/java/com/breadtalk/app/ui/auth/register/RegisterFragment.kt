package com.breadtalk.app.ui.auth.register

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentRegisterBinding
import com.breadtalk.app.ui.base.BaseFragment
import com.breadtalk.app.utils.DialogError
import com.breadtalk.app.utils.DlgLoadingProgressBar
import com.breadtalk.app.utils.KeyboardUtil
import com.bumptech.glide.Glide
import javax.inject.Inject


class RegisterFragment @Inject constructor() : BaseFragment<FragmentRegisterBinding,RegisterViewModel>() {

    private var dialog: DlgLoadingProgressBar? = null

    override fun getViewModelClass(): Class<RegisterViewModel> = RegisterViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_register

    override fun onViewReady(savedInstance: Bundle?) {
        KeyboardUtil(requireActivity(), binding.root)
        dialog = DlgLoadingProgressBar(requireContext())
        Glide.with(binding.imgJag.context)
            .load("https://cdn.app.jcodelivery.com/img/jagroup.jpg")
            .into(binding.imgJag)
        binding.btnCreate.setOnClickListener {
            binding.edtFullName.error = null
            binding.edtEmail.error = null
            binding.edtPassword.error = null
            binding.edtVerifyPassword.error = null

            val phoneNumber =
                if (binding.edtPhoneNoReg.editText?.text.toString()[3] == '0') binding.edtPhoneNoReg.editText?.text.toString()
                    .removeRange(3..3) else binding.edtPhoneNoReg.editText?.text.toString()

            viewModel.register(
                binding.edtFullName.editText?.text.toString(),
                binding.edtEmail.editText?.text.toString(),
                phoneNumber,
                binding.edtPassword.editText?.text.toString(),
                binding.edtVerifyPassword.editText?.text.toString()
            )
        }

        initObserver()
    }

    private fun initObserver() {

        viewModel.errorFullName.observe(this, {
            it.getContentIfNotHandled()?.let { error ->
                binding.edtFullName.error = error
            }
        })

        viewModel.errorEmail.observe(this, {
            it.getContentIfNotHandled()?.let { error ->
                binding.edtEmail.error = error
            }
        })

        viewModel.errorPassword.observe(this, {
            it.getContentIfNotHandled()?.let { error ->
                binding.edtPassword.error = error
            }
        })

        viewModel.errorConfirmPassword.observe(this, {
            it.getContentIfNotHandled()?.let { error ->
                binding.edtVerifyPassword.error = error
            }
        })



        viewModel.showToast.observe(this, {
            it.getContentIfNotHandled()?.let { message ->
                Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
            }
        })
        viewModel.closeRegisterPage.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateTo(R.string.linkMainFragment)

            }
        })

        viewModel.registerFailed.observe(this, {
            it.getContentIfNotHandled()?.let { message ->
                val dlg = DialogError(requireContext())
                dlg.showPopup(
                    message,
                    {
                        dlg.dismissPopup()
                    })
            }
        })

        viewModel.showLoadingOTP.observe(this,{
            if (it){
                dialog?.showPopUp()
            } else {
                dialog?.dismissPopup()
            }
        })
    }
}