package com.breadtalk.app.ui.product_detail

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentProductDetailBinding
import com.breadtalk.app.ui.base.BaseFragment
import com.breadtalk.app.ui.main.home.DialogCannotOrder
import com.breadtalk.app.utils.Converter
import javax.inject.Inject


class ProductDetailFragment @Inject constructor() :
    BaseFragment<FragmentProductDetailBinding, ProductDetailViewModel>() {

    override fun getViewModelClass(): Class<ProductDetailViewModel> =
        ProductDetailViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_product_detail

    override fun onViewReady(savedInstance: Bundle?) {
        if (!isFragmentFromPaused) {
            arguments?.let {
                it.getString("id")?.let { it1 -> viewModel.loadData(it1) }
            }
        }

        binding.apply {
            topBar.btnBack.setOnClickListener { onBackPress() }
            viewModel = this@ProductDetailFragment.viewModel
            executePendingBindings()
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = ProductDetailController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })

        viewModel.price.observe(this, {
            controller.price = Converter.thousandSeparator(it.toString())
        })
    }

    private fun initObserver() {
        viewModel.apply {
            photoSelected.observe(viewLifecycleOwner, {
                it.getContentIfNotHandled().let { photo ->
                    val dlg = DialogShowPhoto(requireContext())
                    dlg.showPopup(
                        image = photo.toString(),
                        onClickListener = {
                            dlg.dismissPopup()
                        }
                    )
                    if (photo != null) {
                        Log.d("cekimage", photo)
                    }
                }
            })
            showAlert.observe(viewLifecycleOwner, {
                it.getContentIfNotHandled()?.let { message ->
                    Toast.makeText(
                        context,
                        message,
                        Toast.LENGTH_LONG
                    ).show()
                }
            })

            addToCart.observe(viewLifecycleOwner, {
                it.getContentIfNotHandled()?.let {
                    val dlg = DialogAddToCart(requireContext())
                    dlg.showPopup({
                        dlg.dismissPopup()
                        onBackPress()
                    },
                        {
                            dlg.dismissPopup()
                            val action =
                                ProductDetailFragmentDirections.actionFromProductToMainFragment("cart")
                            findNavController().navigate(
                                action,
                                FragmentNavigator.Extras.Builder()
                                    .build()
                            )
                        })
                }
            })

            showDialogCannotOrder.observe(viewLifecycleOwner, {
                it.getContentIfNotHandled()?.let {

                    val dlg = DialogCannotOrder()
                    dlg.showDialog(
                        requireActivity().supportFragmentManager,
                        "ProductDetailFragment",
                        object : DialogCannotOrder.OnDialogClickListener {
                            override fun onLoginClick() {
                                dlg.dissmissDialog()
                                navigateTo(R.string.linkLogin)
                            }

                            override fun onRegisterClick() {
                                dlg.dissmissDialog()
                                navigateTo(R.string.linkRegister)
                            }
                        })
                }
            })
        }
    }
}