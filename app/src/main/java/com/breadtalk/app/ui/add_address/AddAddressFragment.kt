package com.breadtalk.app.ui.add_address

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.core.util.Preconditions
import com.breadtalk.app.R
import com.breadtalk.app.data.local.AddressReq
import com.breadtalk.app.data.local.UpdateAddressReq
import com.breadtalk.app.databinding.FragmentAddAddressBinding
import com.breadtalk.app.ui.base.BaseFragment
import com.google.android.gms.common.api.ApiException
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.FetchPlaceResponse
import de.mateware.snacky.Snacky
import javax.inject.Inject


class AddAddressFragment @Inject constructor() :
    BaseFragment<FragmentAddAddressBinding, AddAddressViewModel>() {

    private var memberId = 0

    override fun getViewModelClass(): Class<AddAddressViewModel> = AddAddressViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_add_address

    override fun onViewReady(savedInstance: Bundle?) {
        binding.topBar.btnBack.setOnClickListener { onBackPress() }
        initObserver()
        arguments?.let {
            it.getString("id")?.let { it1 -> viewModel.getAddress(it1.toInt()) }
        }

        binding.apply {
            btnAddToCart.setOnClickListener { save() }
            location.setOnClickListener {
                val dlg = DialogSearchLocation()
                dlg.showDialog(
                    requireActivity().supportFragmentManager,
                    AddAddressFragment::class.java.simpleName,
                    object : DialogSearchLocation.DialogSearchLocationListener {
                        override fun latitude(): String =
                            sharedPreference.loadLatitude().toString()

                        override fun longitude(): String =
                            sharedPreference.loadLongitude().toString()

                        override fun address(autocompletePrediction: AutocompletePrediction): String {
                            Log.d("cekaddress", autocompletePrediction.getFullText(null).toString())
                            binding.location.text = "${autocompletePrediction.getFullText(null)}"
                            val placeField =
                                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)
                            val request = FetchPlaceRequest.newInstance(
                                autocompletePrediction.placeId,
                                placeField
                            )
                            val placesClient = Places.createClient(requireContext())
                            placesClient.fetchPlace(request)
                                .addOnSuccessListener { response: FetchPlaceResponse ->
                                    val place = response.place
                                    val latlng = place.latLng.toString().split(",")
                                    val lat = latlng[0].replace("lat/lng: (", "")
                                    val long = latlng[1].replace(")", "")
                                    sharedPreference.apply {
                                        saveLatitudeAddress(lat)
                                        saveLongitudeAddress(long)
                                    }
                                    viewModel.loadLocation(lat, long)
                                }.addOnFailureListener { exception: Exception ->
                                    if (exception is ApiException) {
                                        Log.e("TAG", "Place not found: ${exception.message}")
                                        val statusCode = exception.statusCode
                                        TODO("Handle error with given status code")
                                    }
                                }
                            return "${autocompletePrediction.getFullText(null)}"
                        }
                    }
                )
            }
        }
        resultAddressFromMap()
    }

    @SuppressLint("RestrictedApi")
    private fun resultAddressFromMap() {
        parentFragmentManager.setFragmentResultListener("address", this,
            { requestKey, result ->
                Preconditions.checkState("address" == requestKey)
                binding.location.text =
                    result.toString().removePrefix("Bundle[{address=").removeSuffix("}]")
            })
    }

    private fun save() {
        binding.apply {
            if (arguments?.getString("id") == "0") {
                if (fullName.text.isNullOrEmpty()
                    || phoneNumber.text.isNullOrEmpty()
                    || location.text.isNullOrEmpty()
                    || detailAddress.text.isNullOrEmpty()
                ) {
                    Snacky.builder()
                        .setActivity(activity)
                        .setText("Mohon isi alamat dengan lengkap")
                        .setDuration(Snacky.LENGTH_SHORT)
                        .error()
                        .show()
                } else {
                    viewModel.setAddress(
                        AddressReq(
                            member_phone = sharedPreference.loadPhoneNumber(),
                            address_label = binding.labelAddress.text.toString(),
                            recipient_name = binding.fullName.text.toString(),
                            recipient_postcode = sharedPreference.loadPostCode(),
                            recipient_phone = binding.phoneNumber.text.toString(),
                            cityordistrict = sharedPreference.loadCity(),
                            address = binding.location.text.toString(),
                            address_details = binding.detailAddress.text.toString(),
                            latitude = sharedPreference.loadLatitudeAddress(),
                            longitude = sharedPreference.loadLongitudeAddress()
                        )
                    )
                }
            } else {
                viewModel.updateAddress(
                    UpdateAddressReq(
                        member_address_id = memberId,
                        member_phone = sharedPreference.loadPhoneNumber(),
                        address_label = binding.labelAddress.text.toString(),
                        recipient_name = binding.fullName.text.toString(),
                        recipient_postcode = sharedPreference.loadPostCode(),
                        recipient_phone = binding.phoneNumber.text.toString(),
                        cityordistrict = sharedPreference.loadCity(),
                        address = binding.location.text.toString(),
                        address_details = binding.detailAddress.text.toString(),
                        latitude = sharedPreference.loadLatitudeAddress(),
                        longitude = sharedPreference.loadLongitudeAddress()
                    )
                )
                sharedPreference.saveSelectPhoneNumber(binding.phoneNumber.text.toString())
            }
        }
    }

    private fun initObserver() {
        viewModel.successAddAddress.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                navigatePopupInclusiveTo(R.id.addAddressFragment, R.string.linkDeliveryFragment)
            }
        })

        viewModel.memberAddress.observe(viewLifecycleOwner, {
            binding.apply {
                fullName.setText(it.data.recipient_name)
                phoneNumber.setText(it.data.recipient_phone)
                location.text = it.data.address
                detailAddress.setText(it.data.address_details)
                memberId = it.data.member_address_id ?: 0
                labelAddress.setText(it.data.address_label.toString())
            }
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { message ->
                Snacky.builder()
                    .setActivity(activity)
                    .setText(message)
                    .setDuration(Snacky.LENGTH_SHORT)
                    .error()
                    .show()
            }
        })
    }
}