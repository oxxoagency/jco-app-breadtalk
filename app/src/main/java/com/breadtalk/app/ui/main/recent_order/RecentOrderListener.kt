package com.breadtalk.app.ui.main.recent_order

interface RecentOrderListener {
    fun onRecentOrderClick(position: Int)
    fun onReOrderClick(position: Int)
}