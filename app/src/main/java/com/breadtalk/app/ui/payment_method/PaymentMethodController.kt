package com.breadtalk.app.ui.payment_method

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.Payment
import com.breadtalk.app.paymentMethod

class PaymentMethodController(private val listener: PaymentMethodControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is Payment -> addPaymentMethod(cellData,listener,index)
            }
        }
    }

    private fun addPaymentMethod(
        cellData: Payment,
        listener: PaymentMethodControllerListener,
        position: Int
    ) {
        paymentMethod {
            id(cellData.paymentMethod)
            data(cellData)
            listener(listener)
            position(position)
        }
    }
}