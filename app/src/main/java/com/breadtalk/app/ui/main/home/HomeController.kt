package com.breadtalk.app.ui.main.home

import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.epoxy.Carousel
import com.airbnb.epoxy.carousel
import com.breadtalk.app.*
import com.breadtalk.app.data.local.*

class HomeController(private val listener: HomeControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var promoSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var menuSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is HomeHeadSection -> addHomeHeadSection(cellData, listener)
                is HomePromoHeader -> addHomePromoHeader(listener)
                is HomePromos -> addHomePromos(cellData, listener, index)
                is HomeMenuHeader -> addHomeMenuHeader()
                is HomeMenuItem -> addHomeMenuItem(cellData, listener, index)
                is HomeMenuCategories -> addHomeMenuCategory(cellData, listener)
                is LoadingProductGrid -> addLoadingProductGrid(cellData)
                is LoadingPage -> addLoadingPage(cellData)
            }
        }
    }

    private fun addHomeHeadSection(cellData: HomeHeadSection, listener: HomeControllerListener) {
        homeHeader {
            id("header")
            spanSizeOverride { _, _, _ -> 2 }
            listener(listener)
            data(cellData)
        }
    }

    private fun addHomePromoHeader(listener: HomeControllerListener) {
        homePromoHeader {
            id("carousel-header")
            spanSizeOverride { _, _, _ -> 2 }
            listener(listener)
        }
    }

    private fun addHomePromos(cellData: HomePromos, listener: HomeControllerListener, index: Int) {
        if (!cellData.promos.isNullOrEmpty()) {
            homePromo {
                cellData.promos?.map {
                    this.id(it.imgURL)
                        .listener(listener)
                        .index(index)
                        .selected(it.menuCode == promoSelected)
                }
                this.data(cellData)
                spanSizeOverride { _, _, _ -> 2 }
            }
        }

    }

    private fun addHomeMenuHeader() {
        homeMenuHeader {
            id("menu_header")
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addHomeMenuItem(
        cellData: HomeMenuItem,
        listener: HomeControllerListener,
        index: Int
    ) {
        homeMenuItem {
            id(cellData.menuCode)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addHomeMenuCategory(
        cellData: HomeMenuCategories,
        listener: HomeControllerListener
    ) {
        val models = cellData.homeMenus.map {
            HomeMenuBindingModel_()
                .id(it.name)
                .data(it)
                .selected(it.name == menuSelected)
                .listener(listener)
        }

        carousel {
            id("menuHeader")
            padding(Carousel.Padding.dp(16, 4, 16, 16, 8))
            models(models)
            spanSizeOverride { _, _, _ -> 2 }
        }
    }

    private fun addLoadingProductGrid(cellData: LoadingProductGrid) {
        lytLoadingProductGrid {
            id(cellData.hashCode())
        }
    }

    private fun addLoadingPage(cellData: LoadingPage) {
        lytLoadingShimmer {
            id(cellData.hashCode())
            spanSizeOverride { _, _, _ -> 2 }
        }
    }
}