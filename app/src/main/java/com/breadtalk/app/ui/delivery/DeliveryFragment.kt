package com.breadtalk.app.ui.delivery

import android.net.Uri
import android.os.Bundle
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentDeliveryBinding
import com.breadtalk.app.ui.base.BaseFragment
import com.breadtalk.app.utils.DlgLoadingProgressBar
import javax.inject.Inject


class DeliveryFragment @Inject constructor() :
    BaseFragment<FragmentDeliveryBinding, DeliveryViewModel>() {

    private var dialog: DlgLoadingProgressBar? = null

    override fun getViewModelClass(): Class<DeliveryViewModel> = DeliveryViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_delivery

    override fun onViewReady(savedInstance: Bundle?) {
        dialog = DlgLoadingProgressBar(requireContext())
        binding.topBar.btnBack.setOnClickListener { onBackPress() }
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = DeliveryController(viewModel)
        binding.recyclerView.setController(controller)
        binding.recyclerView.itemAnimator = null
        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.addNewAddress.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                val url =
                    getString(R.string.linkAddAddressFragment).replace("{id}", "0")
                val uri = Uri.parse(url)
                navigatePopupInclusiveTo(R.id.deliveryFragment, uri)
            }
        })

        viewModel.showLoading.observe(this, {
            it.getContentIfNotHandled()?.let { state ->
                if (state) {
                    dialog?.showPopUp()
                } else {
                    dialog?.dismissPopup()
                }
            }
        })


        viewModel.openCart.observe(this, {
            it.getContentIfNotHandled()?.let {
                val action =
                    DeliveryFragmentDirections.actionFromDeliveryToMainFragment("cart")
                findNavController()
                    .navigate(
                        action,
                        FragmentNavigator.Extras.Builder()
                            .build()
                    )
            }
        })

        viewModel.openAddAddress.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { data ->
                val url =
                    getString(R.string.linkAddAddressFragment).replace("{id}", data.id.toString())
                val uri = Uri.parse(url)
                navigatePopupInclusiveTo(R.id.deliveryFragment, uri)
            }
        })
    }
}