package com.breadtalk.app.ui.product_detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.local.*
import com.breadtalk.app.data.local.room.BreadTalkDatabase
import com.breadtalk.app.data.remote.model.req.ProductDetailReq
import com.breadtalk.app.data.remote.model.req.ProductFavoriteReq
import com.breadtalk.app.data.remote.model.res.ProductDetailRes
import com.breadtalk.app.data.repository.HomeRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.Converter
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class ProductDetailViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val database: BreadTalkDatabase,
) : BaseViewModel(),
    ProductDetailControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private var _image = MutableLiveData<String>()
    val image: LiveData<String>
        get() = _image

    private var _photoSelected = MutableLiveData<SingleEvents<String>>()
    val photoSelected: LiveData<SingleEvents<String>>
        get() = _photoSelected

    private val _showAlert = MutableLiveData<SingleEvents<String>>()
    val showAlert: LiveData<SingleEvents<String>>
        get() = _showAlert

    private val _addToCart = MutableLiveData<SingleEvents<String>>()
    val addToCart: LiveData<SingleEvents<String>>
        get() = _addToCart

    private val _price = MutableLiveData<Int>()
    val price: LiveData<Int>
        get() = _price

    private val _showDialogCannotOrder = MutableLiveData<SingleEvents<String>>()
    val showDialogCannotOrder: LiveData<SingleEvents<String>>
        get() = _showDialogCannotOrder

    private var tempCartProduct: CartProduct? = null
    private var tempCartProductPickUp: CartProductPickUp? = null
    private var tempCartDetailVariant = mutableListOf<CartDetailVariant>()
    private var tempCartVariant = mutableListOf<ProductDetailVariantPackageHeader>()

    private var totalQtyVariant: Int = 0
    private var totalPriceVariant: Int = 0

    private fun insertToCart(cartProduct: CartProduct): Completable =
        database.cartDao().insertCart(cartProduct)

    private fun insertToCartPickUp(cartProduct: CartProductPickUp): Completable =
        database.cartDao().insertCartPickUp(cartProduct)

    fun loadData(menuCode: String?) {
        val temp = mutableListOf<BaseCell>()
        temp.add(LoadingPage())
        val body = ProductDetailReq(
            "3", sharedPreference.loadSelectCity().toString(),
            menuCode.toString(),
            sharedPreference.loadPhoneNumber().toString()
        )
        sharedPreference.removeCodeVoucher()
        sharedPreference.removeVoucher()
        lastDisposable = homeRepository.getProductDetail(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                temp.removeAll { it is LoadingPage }
                val data = it.data
                checkPrice(data.is_baseprice == 1, data.menu_price ?: 0)
                defaultTempCartProduct(it)
                data.menu_image?.map { menuImage ->
                    _image.postValue(menuImage.image.toString())
                }
                temp.add(
                    ProductDetailContent(
                        menuCode = data.menu_code,
                        id_menu = data.id_menu,
                        desc = data.menu_desc,
                        price = data.menu_price,
                        priceText = Converter.thousandSeparator(data.menu_price.toString()),
                        productName = localizationMenuName(it),
                        quantity = 1,
                        menu_image = data.menu_image?.get(0)?.image,
                        type = data.category_title,
                        variantNum = data.variant_num?.toInt(),
                        variant = data.details,
                        idType = data.id_type,
                        isBasePrice = data.is_baseprice,
                        isFavorite = data.is_favorite == "1"
                    )
                )
                if (!data.details.isNullOrEmpty()) {
                    temp.add(ProductDetailVariantHeader(state = data.details[0].package_donut_list.isNullOrEmpty()))
                    mutableListOf<ProductDetailVariant>()
                    data.details.map { variant ->
                        if (variant.package_donut_list.isNullOrEmpty()) {
                            if (data.id_type == 1 && data.is_baseprice == 1) {
                                temp.add(
                                    ProductDetailVariant(
                                        variant.package_code,
                                        variant.package_name,
                                        variant.package_image,
                                        variant.package_price,
                                        Converter.thousandSeparator(variant.package_price.toDouble()),
                                        isShowQty = true,
                                        isShowPrice = true,
                                        qty = 0,
                                    )
                                )
                            } else {
                                temp.add(
                                    ProductDetailVariant(
                                        variant.package_code,
                                        variant.package_name,
                                        variant.package_image,
                                        variant.package_price,
                                        Converter.thousandSeparator(variant.package_price.toDouble()),
                                        isShowQty = true,
                                        isShowPrice = false,
                                        qty = 0,
                                    )
                                )
                            }
                            tempCartDetailVariant.add(
                                CartDetailVariant(
                                    menuCode = variant.package_code ?: "",
                                    price = variant.package_price.toDouble(),
                                    name = variant.package_name ?: "",
                                    qty = 0
                                )
                            )
                        } else {
                            temp.add(
                                ProductDetailVariantPackageHeader(
                                    variant.package_name.toString(),
                                    0,
                                    data.menu_code
                                )
                            )
                            variant.package_donut_list.map { packageList ->
                                temp.add(
                                    ProductDetailVariant(
                                        "",
                                        packageList.menu_name,
                                        packageList.menu_img,
                                        0,
                                        "",
                                        false,
                                        isShowPrice = false,
                                        qty = 0
                                    )
                                )
                            }
                            tempCartVariant.add(
                                ProductDetailVariantPackageHeader(
                                    variant.package_name.toString(),
                                    0,
                                    data.menu_code
                                )
                            )
                            temp.add(Line())
                        }
                    }
                }
                _datas.value = temp
            }, {

            })

    }

    private fun defaultTempCartProduct(
        content: ProductDetailRes,
    ) {
        tempCartProduct = CartProduct(
            menuCode = content.data.menu_code,
            name = localizationMenuName(content),
            imgURL = content.data.menu_image?.get(0)?.image,
            price = content.data.menu_price?.toDouble() ?: 0.0,
            productType = content.data.category_title,
            qty = 1,
            detailVariant = mutableListOf(),
            priceOriginal = content.data.menu_price?.toDouble() ?: 0.0,
            variantname = "",
            idType = content.data.id_type,
            isBasePrice = content.data.is_baseprice,
            menuDetail = ""
        )
    }

    private fun addTempCartProduct(content: ProductDetailContent, fixPrice: Int?) {
        tempCartProduct = CartProduct(
            menuCode = content.menuCode,
            name = content.productName,
            imgURL = content.menu_image,
            price = fixPrice?.toDouble() ?: 0.0,
            productType = content.type,
            qty = content.quantity,
            detailVariant = mutableListOf(),
            priceOriginal = content.price?.toDouble() ?: 0.0,
            idType = content.idType,
            isBasePrice = content.isBasePrice,
            menuDetail = ""
        )
    }

    private fun addTempCartProductFromDetailVariant(
        variant: ProductDetailVariant,
        header: ProductDetailContent,
        fixPrice: Int?,
        variantName: String,
        menuDetail: String,
        detailVariant: MutableList<CartDetailVariant>,
    ) {
        if (header.idType == 1 && header.isBasePrice == 1) {
            tempCartProduct = CartProduct(
                menuCode = header.menuCode,
                name = header.productName,
                imgURL = header.menu_image,
                price = fixPrice?.toDouble() ?: 0.0,
                productType = header.type,
                qty = 1,
                detailVariant = detailVariant,
                priceOriginal = variant.price?.toDouble() ?: 0.0,
                variantname = variantName,
                idType = header.idType,
                isBasePrice = header.isBasePrice,
                menuDetail = menuDetail
            )
        } else {
            tempCartProduct = CartProduct(
                menuCode = header.menuCode,
                name = header.productName,
                imgURL = header.menu_image,
                price = header.quantity.times(header.price?.toDouble() ?: 0.0),
                productType = header.type,
                qty = header.quantity,
                detailVariant = mutableListOf(),
                priceOriginal = header.price?.toDouble() ?: 0.0,
                variantname = variantName,
                idType = header.idType,
                isBasePrice = header.isBasePrice,
                menuDetail = menuDetail
            )
        }
    }

    private fun addTempCartProductFromVariant(
        header: ProductDetailContent,
        variantName: String,
    ) {
        tempCartProduct = CartProduct(
            menuCode = header.menuCode,
            name = header.productName,
            imgURL = header.menu_image,
            price = header.quantity.times(header.price?.toDouble() ?: 0.0),
            productType = header.type,
            qty = header.quantity,
            detailVariant = mutableListOf(),
            priceOriginal = header.price?.toDouble() ?: 0.0,
            variantname = variantName,
            idType = header.idType,
            isBasePrice = header.isBasePrice,
            menuDetail = variantName
        )
    }

    private fun checkPrice(isBasePrice: Boolean, price: Int) {
        if (isBasePrice) {
            _price.postValue(totalPriceVariant)
        } else {
            _price.value = price
        }
    }


    private fun localizationMenuName(product: ProductDetailRes): String =
        if (sharedPreference.loadLanguage()
                ?.contains("indonesia", true) == true
        ) product.data.menu_name
        else product.data.menu_name_en

    private fun doInsertToCart(cartProduct: CartProduct, quantity: Int) {
        if (quantity > 0) {
            compositeDisposable.add(
                insertToCart(cartProduct).subscribeOn(schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({}, {
                        handleError(it)
                    })
            )
        }
    }

    private fun doInsertToCartPickUp(cartProduct: CartProductPickUp, quantity: Int) {
        if (quantity > 0) {
            compositeDisposable.add(
                insertToCartPickUp(cartProduct).subscribeOn(schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({}, {
                        handleError(it)
                    })
            )
        }
    }


    override fun onPlusClick(position: Int) {
        _datas.value.let {
            val content = (it?.get(position) as ProductDetailContent).copy()
            content.quantity++

            if (content.variant.isNullOrEmpty()) {
                val fixPrice = content.price?.times(content.quantity)
                addTempCartProduct(content, fixPrice)
            }

            it[position] = content
            _datas.postValue(it)
        }
    }

    override fun onMinusClick(position: Int) {
        _datas.value.let {
            val content = (it?.get(position) as ProductDetailContent).copy()

            if (content.quantity > 0) {
                content.quantity--

                val fixPrice = content.price?.times(content.quantity)
                addTempCartProduct(content, fixPrice)
            }

            it[position] = content
            _datas.postValue(it)
        }
    }

    private fun addTempCartProductPickUp() {
        tempCartProductPickUp = CartProductPickUp(
            menuCode = tempCartProduct?.menuCode,
            name = tempCartProduct?.name,
            imgURL = tempCartProduct?.imgURL,
            price = tempCartProduct?.price ?: 0.0,
            productType = tempCartProduct?.productType,
            qty = tempCartProduct?.qty ?: 0,
            detailVariant = tempCartProduct?.detailVariant,
            priceOriginal = tempCartProduct?.priceOriginal ?: 0.0,
            variantname = tempCartProduct?.variantname ?: "",
            idType = tempCartProduct?.idType,
            isBasePrice = tempCartProduct?.isBasePrice,
            menuDetail = tempCartProduct?.menuDetail
        )
    }

    override fun onAddToCart() {
        _datas.value.let {
            addTempCartProductPickUp()
            val header = (it?.get(0) as ProductDetailContent).copy()
            val max = header.quantity.times(header.variantNum ?: 0)
            if (header.quantity <= 0) {
                _showAlert.value = SingleEvents("Harap tambah quantity Anda")
            } else {
                if (totalQtyVariant < max) {
                    if (header.isBasePrice == 1 && totalQtyVariant >= header.variantNum ?: 0) {
                        doInsertToCart(tempCartProduct ?: return, tempCartProduct?.qty ?: 0)
                        doInsertToCartPickUp(tempCartProductPickUp ?: return,
                            tempCartProductPickUp?.qty ?: 0)
                        checkLogin()
                    } else {
                        _showAlert.value =
                            SingleEvents("Pastikan anda tambah ${max - totalQtyVariant} item lagi")
                    }
                } else {
                    doInsertToCart(tempCartProduct ?: return, tempCartProduct?.qty ?: 0)
                    doInsertToCartPickUp(tempCartProductPickUp ?: return,
                        tempCartProductPickUp?.qty ?: 0)
                    checkLogin()
                }
            }
        }
    }

    override fun onBtnFavoriteClick() {
        _datas.value?.let {
            val temp = (it[0] as ProductDetailContent).copy()
            val favorite = if (temp.isFavorite) "0" else "1"
            val body = ProductFavoriteReq(
                favorite,
                sharedPreference.loadPhoneNumber().toString(),
                temp.menuCode ?: ""
            )
            lastDisposable = homeRepository.setProductFavorite(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ model ->
                    temp.isFavorite = !temp.isFavorite
                    it[0] = temp
                    _datas.postValue(it)
                }, {
                    handleError(it)
                })

            lastDisposable?.let { compositeDisposable.add(it) }
        }
    }

    override fun onPhotoClick(productDetailVariant: ProductDetailVariant) {
        _photoSelected.value = SingleEvents(productDetailVariant.image ?: "")
    }

    override fun onPlusDetailVariantClick(position: Int) {
        _datas.value.let {
            val header = (it?.get(0) as ProductDetailContent).copy()
            val variant = (it[position] as ProductDetailVariant).copy()
            val variantName = mutableListOf<String>()
            val menuDetail = mutableListOf<String>()
            val detailVariant = mutableListOf<CartDetailVariant>()
            variant.qty++
            totalQtyVariant++
            totalPriceVariant += variant.price ?: 0
            val max = header.quantity.times(header.variantNum ?: 0)

            if (header.isBasePrice == 1) {
                header.quantity = totalQtyVariant
            } else if (totalQtyVariant > max && header.isBasePrice == 0) {
                header.quantity++
            }

            tempCartDetailVariant.map { detail ->
                if (detail.menuCode == variant.menuCode) {
                    detail.qty = variant.qty
                }

                if (detail.qty > 0) {
                    variantName.add("${detail.name}(${detail.qty})")
                    menuDetail.add("${detail.menuCode}-${detail.qty}")
                    detailVariant.add(CartDetailVariant(
                        menuCode = detail.menuCode,
                        price = detail.price,
                        name = detail.name,
                        qty = detail.qty
                    ))

                }
            }

            checkPrice(header.isBasePrice == 1, header.price ?: 0)

            addTempCartProductFromDetailVariant(
                variant,
                header,
                totalPriceVariant,
                variantName.toString().removePrefix("[").removeSuffix("]"),
                menuDetail.toString().removePrefix("[").removeSuffix("]"),
                detailVariant
            )

            it[0] = header
            it[position] = variant
            _datas.postValue(it)
        }
    }

    override fun onMinusDetailVariantClick(position: Int) {
        _datas.value.let {
            val header = (it?.get(0) as ProductDetailContent).copy()
            val variant = (it[position] as ProductDetailVariant).copy()
            val variantName = mutableListOf<String>()
            val menuDetail = mutableListOf<String>()
            val detailVariant = mutableListOf<CartDetailVariant>()
            if (variant.qty > 0) {
                variant.qty--
                totalQtyVariant--
                totalPriceVariant -= variant.price ?: 0
                if (totalQtyVariant.rem(header.variantNum ?: 0) == 0 && header.isBasePrice == 0) {
                    header.quantity--
                } else if (header.isBasePrice == 1) {
                    header.quantity--
                }

                tempCartDetailVariant.map { detail ->
                    if (detail.menuCode == variant.menuCode) {
                        detail.qty = variant.qty
                    }

                    if (detail.qty > 0) {
                        variantName.add("${detail.name}(${detail.qty})")
                        menuDetail.add("${detail.menuCode}-${detail.qty}")
                        detailVariant.add(CartDetailVariant(
                            menuCode = detail.menuCode,
                            price = detail.price,
                            name = detail.name,
                            qty = detail.qty
                        ))
                    }
                }

                checkPrice(header.isBasePrice == 1, header.price ?: 0)

                addTempCartProductFromDetailVariant(
                    variant,
                    header,
                    totalPriceVariant,
                    variantName.toString().removePrefix("[").removeSuffix("]"),
                    menuDetail.toString().removePrefix("[").removeSuffix("]"),
                    detailVariant
                )
            }
            it[0] = header
            it[position] = variant
            _datas.postValue(it)
        }
    }

    override fun onPlusVariantClick(position: Int) {
        _datas.value.let {
            val header = (it?.get(0) as ProductDetailContent).copy()
            val variant = (it[position] as ProductDetailVariantPackageHeader).copy()
            val variantName = mutableListOf<String>()
            variant.qty++
            totalQtyVariant += header.variantNum ?: 0
            val max = header.quantity.times(header.variantNum ?: 0)
            if (totalQtyVariant > max) {
                header.quantity++
            }

            tempCartVariant.map { detail ->
                if (detail.menuCode == variant.menuCode) {
                    detail.qty = variant.qty
                }

                if (detail.qty > 0) {
                    variantName.add("${detail.name}(${detail.qty})")
                }
            }
            addTempCartProductFromVariant(
                header,
                variantName.toString().removePrefix("[").removeSuffix("]")
            )

            it[0] = header
            it[position] = variant
            _datas.postValue(it)
        }
    }

    override fun onMinusVariantClick(position: Int) {
        _datas.value.let {
            val header = (it?.get(0) as ProductDetailContent).copy()
            val variant = (it[position] as ProductDetailVariantPackageHeader).copy()
            val variantName = mutableListOf<String>()
            if (variant.qty > 0) {
                variant.qty--
                totalQtyVariant -= header.variantNum ?: 0
                header.quantity.times(header.variantNum ?: 0)
                if (totalQtyVariant.rem(header.variantNum ?: 0) == 0) {
                    header.quantity--
                }

                tempCartVariant.map { detail ->
                    if (detail.menuCode == variant.menuCode) {
                        detail.qty = variant.qty
                    }

                    if (detail.qty > 0) {
                        variantName.add("${detail.name}(${detail.qty})")
                    }
                }
                addTempCartProductFromVariant(
                    header,
                    variantName.toString().removePrefix("[").removeSuffix("]")
                )
            }

            it[0] = header
            it[position] = variant
            _datas.postValue(it)
        }
    }

    private fun isLoggedIn(): Boolean {
        val data = sharedPreference.getValueString(SharedPreference.ACCESS_TOKEN)
        return data != null && data != ""
    }

    private fun showDlgCannotOrder() {
        _showDialogCannotOrder.value = SingleEvents("_showDialogCannotOrder")
    }

    private fun addToCart() {
        _addToCart.value = SingleEvents("addToCart")
    }

    private fun checkLogin() {
        if (isLoggedIn()) {
            addToCart()
        } else {
            showDlgCannotOrder()
        }
    }

}