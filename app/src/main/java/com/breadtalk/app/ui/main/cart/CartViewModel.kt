package com.breadtalk.app.ui.main.cart

import android.app.Application
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.webkit.WebView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.R
import com.breadtalk.app.data.local.*
import com.breadtalk.app.data.local.room.BreadTalkDatabase
import com.breadtalk.app.data.remote.model.req.*
import com.breadtalk.app.data.repository.CartRepository
import com.breadtalk.app.data.repository.PaymentRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.*
import com.midtrans.sdk.corekit.models.ItemDetails
import io.reactivex.Completable
import io.reactivex.Flowable
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class CartViewModel @Inject constructor(
    private val schedulers: SchedulerProvider,
    private val database: BreadTalkDatabase,
    private val sharedPreference: SharedPreference,
    private val cartRepository: CartRepository,
    private val paymentRepository: PaymentRepository,
    private val app: Application,
) : BaseViewModel(), CartControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _showDelivery = MutableLiveData<SingleEvents<String>>()
    val showDelivery: LiveData<SingleEvents<String>>
        get() = _showDelivery

    private val _errorMessage = MutableLiveData<SingleEvents<String>>()
    val errorMessage: LiveData<SingleEvents<String>>
        get() = _errorMessage

    private val _grossAmount = MutableLiveData<Double>()
    val grossAmount: LiveData<Double>
        get() = _grossAmount

    private val _cartOrders = MutableLiveData<MutableList<ItemDetails>>()
    val cartOrders: LiveData<MutableList<ItemDetails>>
        get() = _cartOrders

    private val _showDialogNote = MutableLiveData<SingleEvents<String>>()
    val showDialogNote: LiveData<SingleEvents<String>>
        get() = _showDialogNote

    private val _showPaymentMethod = MutableLiveData<SingleEvents<String>>()
    val showPaymentMethod: LiveData<SingleEvents<String>>
        get() = _showPaymentMethod

    private val _reFetchCart = MutableLiveData<SingleEvents<String>>()
    val reFetchCart: LiveData<SingleEvents<String>>
        get() = _reFetchCart

    private val _removeFromCart = MutableLiveData<SingleEvents<String>>()
    val removeFromCart: LiveData<SingleEvents<String>>
        get() = _removeFromCart

    private val _showAddOrder = MutableLiveData<SingleEvents<String>>()
    val showAddOrder: LiveData<SingleEvents<String>>
        get() = _showAddOrder

    private val _showLoading = MutableLiveData<SingleEvents<Boolean>>()
    val showLoading: LiveData<SingleEvents<Boolean>>
        get() = _showLoading

    private val _orderId = MutableLiveData<Int?>()
    val orderId: LiveData<Int?>
        get() = _orderId

    private val _showMidtrans = MutableLiveData<SingleEvents<String>>()
    val showMidtrans: LiveData<SingleEvents<String>>
        get() = _showMidtrans

    private val _alertTime = MutableLiveData<SingleEvents<String>>()
    val alertTIme: LiveData<SingleEvents<String>>
        get() = _alertTime

    private val _showVoucher = MutableLiveData<SingleEvents<String>>()
    val showVoucher: LiveData<SingleEvents<String>>
        get() = _showVoucher

    private val _showPickup = MutableLiveData<SingleEvents<String>>()
    val showPickup: LiveData<SingleEvents<String>>
        get() = _showPickup

    private val _showDate = MutableLiveData<SingleEvents<String>>()
    val showDate: LiveData<SingleEvents<String>>
        get() = _showDate

    private fun cartProduct(): Flowable<List<CartProduct>> =
        database.cartDao().getAllCartProduct()

    private fun cartProductPickUp(): Flowable<List<CartProductPickUp>> =
        database.cartDao().getAllCartProductPickUp()

    private fun updateCart(cartProduct: CartProduct): Completable =
        database.cartDao().updateCart(cartProduct)

    private fun updateCartPickUp(cartProduct: CartProductPickUp): Completable =
        database.cartDao().updateCartPickUp(cartProduct)

    private fun deleteCart(cartProduct: CartProduct): Completable =
        database.cartDao().deleteCart(cartProduct)

    private fun deleteCartPickUp(cartProduct: CartProductPickUp): Completable =
        database.cartDao().deleteCartPickUp(cartProduct)

    private fun deleteAll(): Completable =
        database.cartDao().deleteAll()

    private fun deleteAllPickUp(): Completable =
        database.cartDao().deleteAllPickUp()

    private var deliveryType = 0
    private var orderDetail: MutableList<CreateOrderDetailReq> = mutableListOf()
    private lateinit var date: String

    fun setDate(date:String){
        this.date = date
    }

    fun loadData() {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .take(1)
            .subscribe({ data ->
                Log.d("cekcart", data.toString())
                val temp = mutableListOf<BaseCell>()
                temp.add(CartSwitch(false))
                temp.add(
                    CartDeliveryAddress(
                        sharedPreference.loadAddress().toString(),
                        sharedPreference.loadPickUp().toString()
                    )
                )
                if (data.isNullOrEmpty()) {
                    temp.add(EmptyItem(app.getString(R.string.cart_is_empty)))
                }
                data.map { cartProduct ->
                    temp.add(
                        CartProduct(
                            cartProduct.id,
                            cartProduct.menuCode,
                            cartProduct.name,
                            cartProduct.imgURL,
                            cartProduct.price,
                            cartProduct.productType,
                            cartProduct.qty,
                            cartProduct.priceOriginal,
                            cartProduct.variantname,
                            cartProduct.idType,
                            cartProduct.isBasePrice,
                            cartProduct.menuDetail,
                            cartProduct.detailVariant,
                        )
                    )
                }
                val cartInfo = mutableListOf<CartInfo>()
                data.map { cartProduct ->
                    cartInfo.add(
                        CartInfo(
                            cartProduct.menuCode.toString(),
                            cartProduct.qty.toString(),
                            cartProduct.menuDetail
                        )
                    )
                }
                orderDetail.clear()
                data.map { cartProduct ->
                    orderDetail.add(
                        CreateOrderDetailReq(
                            menu_code = cartProduct.menuCode.toString(),
                            menu_name = cartProduct.name,
                            menu_quantity = cartProduct.qty.toString(),
                            material_unit_price = cartProduct.priceOriginal.toString(),
                            material_sub_total = cartProduct.price.toString(),
                            menu_detail = cartProduct.menuDetail
                        )
                    )
                }
                loadCart(cartInfo, sharedPreference.loadSelectCity() ?: "")
                _datas.value = temp
            }, {

            })

    }

    fun getAllItemCartForSwitch(deliveryType: Int?) {
        _datas.value.let {
            it?.removeAll { data -> data is CartDetail }
            it?.removeAll { data -> data is CartProduct }
            it?.removeAll { data -> data is CartProductPickUp }
            it?.removeAll { data -> data is EmptyItem }
            _datas.value = it
        }
        if (deliveryType == 0) {
            lastDisposable = cartProduct().subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ data ->
                    Log.d("cekcart", data.toString())
                    datas.value.let {
                        if (data.isNullOrEmpty()) {
                            it?.add(EmptyItem(app.getString(R.string.cart_is_empty)))
                        }
                        data.map { cartProduct ->
                            it?.add(
                                CartProduct(
                                    cartProduct.id,
                                    cartProduct.menuCode,
                                    cartProduct.name,
                                    cartProduct.imgURL,
                                    cartProduct.price,
                                    cartProduct.productType,
                                    cartProduct.qty,
                                    cartProduct.priceOriginal,
                                    cartProduct.variantname,
                                    cartProduct.idType,
                                    cartProduct.isBasePrice,
                                    cartProduct.menuDetail,
                                    cartProduct.detailVariant,
                                )
                            )
                        }
                        val cartInfo = mutableListOf<CartInfo>()
                        data.map { cartProduct ->
                            cartInfo.add(
                                CartInfo(
                                    cartProduct.menuCode.toString(),
                                    cartProduct.qty.toString(),
                                    cartProduct.menuDetail
                                )
                            )
                        }
                        orderDetail.clear()
                        data.map { cartProduct ->
                            orderDetail.add(
                                CreateOrderDetailReq(
                                    menu_code = cartProduct.menuCode.toString(),
                                    menu_name = cartProduct.name,
                                    menu_quantity = cartProduct.qty.toString(),
                                    material_unit_price = cartProduct.priceOriginal.toString(),
                                    material_sub_total = cartProduct.price.toString(),
                                    menu_detail = cartProduct.menuDetail
                                )
                            )
                        }
                        loadCart(cartInfo, sharedPreference.loadSelectCity() ?: "")
//                    loadCreateOrder(0)

                        _datas.value = it
                    }
                }, {

                })
        } else {
            lastDisposable = cartProductPickUp().subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ data ->
                    Log.d("cekcart", data.toString())
                    datas.value.let {
                        data.map { cartProduct ->
                            it?.add(
                                CartProductPickUp(
                                    cartProduct.id,
                                    cartProduct.menuCode,
                                    cartProduct.name,
                                    cartProduct.imgURL,
                                    cartProduct.price,
                                    cartProduct.productType,
                                    cartProduct.qty,
                                    cartProduct.priceOriginal,
                                    cartProduct.variantname,
                                    cartProduct.idType,
                                    cartProduct.isBasePrice,
                                    cartProduct.menuDetail,
                                    cartProduct.detailVariant,
                                )
                            )
                        }
                        val cartInfo = mutableListOf<CartInfo>()
                        data.map { cartProduct ->
                            cartInfo.add(
                                CartInfo(
                                    cartProduct.menuCode.toString(),
                                    cartProduct.qty.toString(),
                                    cartProduct.menuDetail
                                )
                            )
                        }
                        orderDetail.clear()
                        data.map { cartProduct ->
                            orderDetail.add(
                                CreateOrderDetailReq(
                                    menu_code = cartProduct.menuCode.toString(),
                                    menu_name = cartProduct.name,
                                    menu_quantity = cartProduct.qty.toString(),
                                    material_unit_price = cartProduct.priceOriginal.toString(),
                                    material_sub_total = cartProduct.price.toString(),
                                    menu_detail = cartProduct.menuDetail
                                )
                            )
                        }
                        loadCart(cartInfo, sharedPreference.loadPickUpCity() ?: "Jakarta Barat")
//                    loadCreateOrder(0)

                        _datas.value = it
                    }
                }, {

                })
        }
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun loadCart(cartInfo: MutableList<CartInfo>, city: String) {
        val body = CartOrder(
            3,
            city,
            deliveryType,
            sharedPreference.loadCodeVoucher(),
            sharedPreference.loadPaymentMethod(),
            "BT",
            cartInfo
        )
        lastDisposable = cartRepository.getCartOrders(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                Log.d("cekmodel", model.toString())
                datas.value.let {
                    if (model.status_code == 200) {
                        if (!model.data.error.coupon_code.isNullOrEmpty()) {
                            _errorMessage.value = SingleEvents(model.data.error.coupon_code)
                            sharedPreference.removeCodeVoucher()
                            sharedPreference.removeVoucher()
                        }
//                        saveCartDetail(model)
                        it?.add(
                            CartDetail(
                                paymentName = sharedPreference.loadPaymentName().toString(),
                                paymentIcon = sharedPreference.loadPaymentIcon().toString(),
                                price = Converter.rupiah(
                                    model.data.subtotal?.toDouble()
                                        ?: 0.0
                                ),
                                deliveryServiceText = Converter.rupiah(
                                    model.data.total_delivery_fee?.toDouble() ?: 0.0
                                ),
                                totalPrice = Converter.rupiah(
                                    model.data.grandtotal?.toDouble()
                                        ?: 0.0
                                ),
                                notes = sharedPreference.loadNotes(),
                                ecobag = model.data.ecoBag?.ecobag_name,
                                ecobagPrice = Converter.rupiah(
                                    model.data.ecoBag?.ecobag_price.toString(),
                                ),
                                ecobagSubTotal = Converter.rupiah(
                                    model.data.ecoBag?.ecobag_subtotal?.toDouble() ?: 0.0
                                ),
                                ecobagQty = model.data.ecoBag?.ecobag_quantity.toString(),
                                voucher = sharedPreference.loadVoucher(),
                                promo = model.data.promo?.promo_value,
                                freeDeliveryText = Converter.rupiah(
                                    model.data.freeDelivery?.toDouble() ?: 0.0
                                ),
                                freeDelivery = model.data.freeDelivery,
                                deliveryService = model.data.delivery_fee,
                                totalDeliveryFee = Converter.rupiah(
                                    model.data.delivery_fee?.toDouble() ?: 0.0
                                ),
                                isDelivery = deliveryType == 0
                            )
                        )
                        _grossAmount.value = model.data.grandtotal?.toDouble()
                        val tempCart = mutableListOf<ItemDetails>()
                        model.data.cart_orders.map { cartProduct ->
                            tempCart.add(
                                ItemDetails(
                                    cartProduct.menu_code.toString(),
                                    cartProduct.menu_price?.toDouble() ?: 0.0,
                                    cartProduct.menu_quantity?.toInt() ?: 0,
                                    cartProduct.menu_name?.take(50)
                                )
                            )
                        }
                        if (model.data.delivery_fee ?: 0 >= 0) {
                            tempCart.add(
                                ItemDetails(
                                    "1111111111-000-1",
                                    model.data.delivery_fee?.toDouble() ?: 0.0,
                                    1,
                                    "Delivery Fee"
                                )
                            )
                        }

                        if (!model.data.ecoBag?.ecobag_name.isNullOrEmpty()) {
                            tempCart.add(
                                ItemDetails(
                                    model.data.ecoBag?.ecobag_code,
                                    model.data.ecoBag?.ecobag_price?.toDouble() ?: 0.0,
                                    model.data.ecoBag?.ecobag_quantity ?: 0,
                                    model.data.ecoBag?.ecobag_name
                                )
                            )
                        }

                        if (model.data.promo?.promo_value != 0) {
                            tempCart.add(
                                ItemDetails(
                                    "1111111111-000-2",
                                    model.data.promo?.promo_value?.toDouble() ?: 0.0,
                                    1,
                                    "Promo"
                                )
                            )
                        }


                        if (model.data.cart_orders.isNullOrEmpty()) {
                            _errorMessage.value =
                                SingleEvents("Harap ubah alamat pengiriman karena berada di luar jangkauan")
                        } else if (!model.data.invalid_menu.isNullOrEmpty()) {
                            var items = ""
                            model.data.invalid_menu.map { invalidMenu ->
                                items = invalidMenu.menu_name.toString()
                            }
                            _errorMessage.value =
                                SingleEvents("Harap ubah pesanan Anda karena produk ini tidak tersedia $items")
                        }

                        _cartOrders.value = tempCart
                    }

                    /**
                     * Implement later
                     */

//                    else {
//                        sharedPreference.removeCodeVoucher()
//                        sharedPreference.removeVoucher()
//                        it?.add(
//                            CartDetail(
//                                paymentName = sharedPreference.loadPaymentName().toString(),
//                                paymentIcon = sharedPreference.loadPaymentIcon().toString(),
//                                price = sharedPreference.loadPriceCart(),
//                                deliveryServiceText = sharedPreference.loadTotalDeliveryFee(),
//                                totalPrice = sharedPreference.loadTotalPriceCart(),
//                                notes = sharedPreference.loadNotes(),
//                                ecobag = sharedPreference.loadEcobagCart(),
//                                ecobagPrice = sharedPreference.loadEcobagPriceCart(),
//                                ecobagSubTotal = sharedPreference.loadEcobagSubtotalCart(),
//                                ecobagQty = sharedPreference.loadEcobagQtyCart(),
//                                voucher = sharedPreference.loadVoucher(),
//                                promo = 0,
//                                freeDeliveryText = sharedPreference.loadFreeDelivery(),
//                                freeDelivery = sharedPreference.loadFreeDelivery()?.toInt(),
//                                deliveryService = sharedPreference.loadDeliveryFeeCart()?.toInt(),
//                                totalDeliveryFee = sharedPreference.loadDeliveryFeeCart()
//                            )
//                        )
//                        _errorMessage.value =
//                            SingleEvents("Pesanan kamu belum memenuhi syarat dan ketentuan voucher yang berlaku")
//                        _grossAmount.value = sharedPreference.loadGrossAmount()?.toDouble()
//                        Log.d("cekitemdetails", sharedPreference.loadItemDetail().toString())
//                        val tempCart = mutableListOf<ItemDetails>()
//                        sharedPreference.loadItemDetail().cart_orders.map { cartProduct ->
//                            tempCart.add(
//                                ItemDetails(
//                                    cartProduct.menu_code.toString(),
//                                    cartProduct.menu_price?.toDouble() ?: 0.0,
//                                    cartProduct.menu_quantity?.toInt() ?: 0,
//                                    cartProduct.menu_name
//                                )
//                            )
//                        }
//                        if (sharedPreference.loadItemDetail().freeDelivery == 0) {
//                            tempCart.add(
//                                ItemDetails(
//                                    "1111111111-000-1",
//                                    sharedPreference.loadItemDetail().delivery_fee?.toDouble()
//                                        ?: 0.0,
//                                    1,
//                                    "Delivery Fee"
//                                )
//                            )
//                        }
//
//                        if (!sharedPreference.loadItemDetail().ecoBag?.ecobag_name.isNullOrEmpty()) {
//                            tempCart.add(
//                                ItemDetails(
//                                    sharedPreference.loadItemDetail().ecoBag?.ecobag_code,
//                                    sharedPreference.loadItemDetail().ecoBag?.ecobag_price?.toDouble()
//                                        ?: 0.0,
//                                    sharedPreference.loadItemDetail().ecoBag?.ecobag_quantity ?: 0,
//                                    sharedPreference.loadItemDetail().ecoBag?.ecobag_name
//                                )
//                            )
//                        }
//
//                        if (sharedPreference.loadItemDetail().promo?.promo_value != 0) {
//                            tempCart.add(
//                                ItemDetails(
//                                    "1111111111-000-2",
//                                    sharedPreference.loadItemDetail().promo?.promo_value?.toDouble()
//                                        ?: 0.0,
//                                    1,
//                                    "Promo"
//                                )
//                            )
//                        }
//
//                        _cartOrders.value = tempCart
//                    }
                    _datas.value = it
                }
            }, {
                handleError(it)
                Log.d("cekhandleerror", "handle")
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun loadCreateOrder(deliveryType: Int?) {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val currentDateAndTime: String = sdf.format(Date())
        val pInfo = app.packageManager.getPackageInfo(app.packageName, 0)
        val version = pInfo.versionName
        val body = if (deliveryType == 0) {
            CreateOrderReq(
                brand = 3,
                order_type = "8",
                payment_type = "3",
                delivery_type = deliveryType,
                member_name = sharedPreference.loadName(),
                member_email = sharedPreference.loadEmail(),
                member_phone = sharedPreference.loadPhoneNumber(),
                member_phone2 = sharedPreference.loadSelectPhoneNumber(),
                order_trx_time = currentDateAndTime,
                order_city = sharedPreference.loadSelectCity(),
                order_address = sharedPreference.loadAddress(),
                order_address_info = sharedPreference.loadDetailAddress(),
                order_postcode = sharedPreference.loadOrderPosttCodeForDelivery(), //hardcode for test emu
                order_latitude = sharedPreference.loadSelectLatitudeAddress(), //hardcode for test emu
                order_longitude = sharedPreference.loadSelectLongitudeAddress(), //hardcode for test emu
                order_note = sharedPreference.loadNotes(),
                order_company = "BID",
                order_outlet_ID = sharedPreference.loadOutletIdForDelivery(), //hardcode for test emu
                order_outlet_code = sharedPreference.loadOutletCodeForDelivery(), //hardcode for test emu
                order_ip = NetworkUtils.getIPAddress(true),
                order_useragent = "${WebView(app).settings.userAgentString} $version",
                order_detail = orderDetail,
                coupon_code = sharedPreference.loadCodeVoucher(),
                order_payment = sharedPreference.loadPaymentMethod(),
                order_brand = "BT",
                order_recipient_name = sharedPreference.loadRecipientName(),
                order_recipient_phone = sharedPreference.loadSelectPhoneNumber(),
                order_send_time = date
            )
        } else {
            CreateOrderReq(
                brand = 3,
                order_type = "8",
                payment_type = "3",
                delivery_type = deliveryType,
                member_name = sharedPreference.loadName(),
                member_email = sharedPreference.loadEmail(),
                member_phone = sharedPreference.loadPhoneNumber(),
                member_phone2 = sharedPreference.loadSelectPhoneNumber(),
                order_trx_time = currentDateAndTime,
                order_city = sharedPreference.loadPickUpCity(),
                order_address = sharedPreference.loadPickUpAddress(),
                order_address_info = sharedPreference.loadPickUpAddress(),
                order_postcode = sharedPreference.loadPostCode(), //hardcode for test emu
                order_latitude = sharedPreference.loadSelectLatitudePickUp(), //hardcode for test emu
                order_longitude = sharedPreference.loadSelectLongitudePickup(), //hardcode for test emu
                order_note = sharedPreference.loadNotes(),
                order_company = "BID",
                order_outlet_ID = sharedPreference.loadOutletId(), //hardcode for test emu
                order_outlet_code = sharedPreference.loadOutletCode(), //hardcode for test emu
                order_ip = NetworkUtils.getIPAddress(true),
                order_useragent = "${WebView(app).settings.userAgentString} $version",
                order_detail = orderDetail,
                coupon_code = sharedPreference.loadCodeVoucher(),
                order_payment = sharedPreference.loadPaymentMethod(),
                order_brand = "BT",
                order_recipient_name = sharedPreference.loadName(),
                order_recipient_phone = sharedPreference.loadPhoneNumber(),
                order_send_time = date
            )
        }

        lastDisposable = cartRepository.getCreateOrder(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                _showLoading.value = SingleEvents(false)
                try {
                    _orderId.value = it.data.order_id
                } catch (e: Exception) {
                    _orderId.value = 0
                }
                if (it.status_code == 200) {
                    _showMidtrans.value = SingleEvents("show-midtrans")
                } else {
                    if (!it.error?.order_address.isNullOrEmpty()) {
                        _errorMessage.value = SingleEvents(it.error?.order_address.toString())
                    } else if (!it.error?.order_outlet_code.isNullOrEmpty()) {
                        _errorMessage.value = SingleEvents(it.error?.order_outlet_code.toString())
                    } else if (!it.error?.member_phone2.isNullOrEmpty()) {
                        _errorMessage.value = SingleEvents(it.error?.member_phone2.toString())
                    }
//                    else {
//                        _errorMessage.value =
//                            SingleEvents("Harap ubah pesanan Anda karena produk ini tidak tersedia")
//                    }
                }

            }, {
                _showLoading.value = SingleEvents(false)
                _errorMessage.value = SingleEvents("Harap lengkapi proses pembayaran")

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun midtransPayment(transactionId: String?) {
        if (deliveryType == 0) {
            val body = PaymentReq(
                1,
                transactionId,
                sharedPreference.loadEmail(),
                sharedPreference.loadPhoneNumber(),
                sharedPreference.loadFcmToken(),
                sharedPreference.loadOutletIdForDelivery()
            )
            lastDisposable = paymentRepository.getMidtransPayment(body).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({

                }, {

                })
        } else {
            val body = PaymentReq(
                1,
                transactionId,
                sharedPreference.loadEmail(),
                sharedPreference.loadPhoneNumber(),
                sharedPreference.loadFcmToken(),
                sharedPreference.loadOutletId()
            )
            lastDisposable = paymentRepository.getMidtransPayment(body).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({

                }, {

                })
        }
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun doUpdateCart(cartProduct: CartProduct) {
        lastDisposable =
            updateCart(cartProduct).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
                }, {
                    handleError(it)
                })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun doUpdateCartPickUp(cartProduct: CartProductPickUp) {
        lastDisposable =
            updateCartPickUp(cartProduct).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({
                }, {
                    handleError(it)
                })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getOrderPayment(orderId: String) {
        val body = OrderPaymentReq(
            brand = 1,
            order_id = orderId
        )

        lastDisposable = paymentRepository.orderPayment(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({

            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun doDeleteCart(cartProduct: CartProduct) {
        lastDisposable =
            deleteCart(cartProduct).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({}, {
                    handleError(it)
                })
        lastDisposable?.let { compositeDisposable.add(it) }

    }

    private fun doDeleteCartPickUp(cartProduct: CartProductPickUp) {
        lastDisposable =
            deleteCartPickUp(cartProduct).subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({}, {
                    handleError(it)
                })
        lastDisposable?.let { compositeDisposable.add(it) }

    }

    fun doDeleteAllCart() {
        _datas.value.let {
            it?.removeAll { data -> data is CartDetail }
            it?.removeAll { data -> data is CartProduct }
            it?.removeAll { data -> data is CartProductPickUp }
            _datas.value = it
        }
        lastDisposable = deleteAll().subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribe({}, {})
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun doDeleteAllPickUpCart() {
        _datas.value.let {
            it?.removeAll { data -> data is CartDetail }
            it?.removeAll { data -> data is CartProduct }
            it?.removeAll { data -> data is CartProductPickUp }
            _datas.value = it
        }
        lastDisposable = deleteAllPickUp().subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribe({}, {})
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onClick(index: Int) {

    }

    override fun onSwitchChange(checked: Boolean) {
        _datas.value?.let {
            if (checked) {
                it[0] = CartSwitch(true)
                it[1] = CartPickupAddress(sharedPreference.loadPickUp().toString())
                deliveryType = 1
                getAllItemCartForSwitch(1)

//                loadCreateOrder(1)
            } else {
                it[0] = CartSwitch(false)
                it[1] = CartDeliveryAddress(
                    sharedPreference.loadAddress().toString(),
                    sharedPreference.loadPickUp().toString()
                )
                deliveryType = 0
                getAllItemCartForSwitch(0)
//                loadCreateOrder(0)
            }
            _datas.postValue(it)
        }
    }

    override fun onPickupAddressClick(index: Int) {
        _showPickup.value = SingleEvents("pickup")
    }

    override fun onDeliveryAddressClick(index: Int) {
        _showDelivery.value = SingleEvents("delivery")
    }

    override fun onOrderClick(index: Int) {
        if (deliveryType == 1) {
            val currentTime = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
            if (currentTime in 22 downTo 9) {
                _showLoading.value = SingleEvents(true)
                loadCreateOrder(deliveryType)

            } else {
                _alertTime.value = SingleEvents("Outlet sedang tutup pada jam 21:00 - 10:00")
            }

        } else {
            _showLoading.value = SingleEvents(true)
            loadCreateOrder(deliveryType)
        }
    }


    override fun onChangePaymentClick(index: Int) {
        _showPaymentMethod.value = SingleEvents("payment-method")
    }

    override fun onProductClick(index: Int) {

    }

    override fun onProductPlusClick(index: Int) {
        _datas.value?.let {
            val content = (it[index] as CartProduct).copy()
            content.qty++
            content.price = content.priceOriginal * content.qty
            it[index] = content
            doUpdateCart(content)
            doUpdateCartPickUp(CartProductPickUp(
                menuCode = content.menuCode,
                id = content.id,
                name = content.name,
                imgURL = content.imgURL,
                price = content.price,
                productType = content.productType,
                qty = content.qty,
                variantname = content.variantname,
                detailVariant = content.detailVariant,
                priceOriginal = content.priceOriginal,
                idType = content.idType,
                isBasePrice = content.isBasePrice,
                menuDetail = content.menuDetail
            ))
            Handler(Looper.getMainLooper()).postDelayed({
                _reFetchCart.value = SingleEvents("removeFromCart")
            }, 3000)
            _datas.value = it
        }
    }

    override fun onProductMinusClick(index: Int) {
        _datas.value?.let {
            val content = (it[index] as CartProduct).copy()
            if (content.qty > 1) {
                content.qty--
                content.price = content.priceOriginal * content.qty
                val price = content.priceOriginal.times(content.qty)
                it[index] = content
                doUpdateCart(
                    CartProduct(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = price,
                        productType = content.productType,
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal,
                        idType = content.idType,
                        isBasePrice = content.isBasePrice,
                        menuDetail = content.menuDetail
                    )
                )
                doUpdateCartPickUp(
                    CartProductPickUp(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = price,
                        productType = content.productType,
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal,
                        idType = content.idType,
                        isBasePrice = content.isBasePrice,
                        menuDetail = content.menuDetail
                    )
                )
                Handler(Looper.getMainLooper()).postDelayed({
                    _reFetchCart.value = SingleEvents("removeFromCart")
                }, 1500)
            } else {
                doDeleteCart(
                    CartProduct(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = content.price,
                        productType = content.productType,
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal,
                        idType = content.idType,
                        isBasePrice = content.isBasePrice,
                        menuDetail = content.menuDetail
                    )
                )

                doDeleteCartPickUp(
                    CartProductPickUp(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = content.price,
                        productType = content.productType,
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal,
                        idType = content.idType,
                        isBasePrice = content.isBasePrice,
                        menuDetail = content.menuDetail
                    )
                )
                Handler(Looper.getMainLooper()).postDelayed({
                    _removeFromCart.value = SingleEvents("removeFromCart")
                }, 0)
            }
            _datas.value = it
        }
    }

    override fun onProductNotesClick(index: Int) {
        _showDialogNote.value = SingleEvents("notes")
    }

    override fun onAddOrderClick() {
        _showAddOrder.value = SingleEvents("showAddOrder")
    }

    override fun onDeleteAllClick() {
        doDeleteAllCart()
        doDeleteAllPickUpCart()
        Handler(Looper.getMainLooper()).postDelayed({
            _removeFromCart.value = SingleEvents("removeFromCart")
        }, 1)
    }

    override fun onVoucherClick() {
        _showVoucher.value = SingleEvents("show-voucher")
    }

    override fun onProductPlusPickUpClick(index: Int) {
        _datas.value?.let {
            val content = (it[index] as CartProductPickUp).copy()
            content.qty++
            content.price = content.priceOriginal * content.qty
            it[index] = content
            doUpdateCartPickUp(content)
            doUpdateCart(CartProduct(
                menuCode = content.menuCode,
                id = content.id,
                name = content.name,
                imgURL = content.imgURL,
                price = content.price,
                productType = content.productType,
                qty = content.qty,
                variantname = content.variantname,
                detailVariant = content.detailVariant,
                priceOriginal = content.priceOriginal,
                idType = content.idType,
                isBasePrice = content.isBasePrice,
                menuDetail = content.menuDetail
            ))
            Handler(Looper.getMainLooper()).postDelayed({
                _reFetchCart.value = SingleEvents("removeFromCart")
            }, 3000)
            _datas.value = it
        }
    }

    override fun onProductMinusPickUpClick(index: Int) {
        _datas.value?.let {
            val content = (it[index] as CartProductPickUp).copy()
            if (content.qty > 1) {
                content.qty--
                content.price = content.priceOriginal * content.qty
                val price = content.priceOriginal.times(content.qty)
                it[index] = content
                doUpdateCart(
                    CartProduct(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = price,
                        productType = content.productType,
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal,
                        idType = content.idType,
                        isBasePrice = content.isBasePrice,
                        menuDetail = content.menuDetail
                    )
                )
                doUpdateCartPickUp(
                    CartProductPickUp(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = price,
                        productType = content.productType,
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal,
                        idType = content.idType,
                        isBasePrice = content.isBasePrice,
                        menuDetail = content.menuDetail
                    )
                )
                Handler(Looper.getMainLooper()).postDelayed({
                    _reFetchCart.value = SingleEvents("removeFromCart")
                }, 1500)
            } else {
                doDeleteCart(
                    CartProduct(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = content.price,
                        productType = content.productType,
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal,
                        idType = content.idType,
                        isBasePrice = content.isBasePrice,
                        menuDetail = content.menuDetail
                    )
                )

                doDeleteCartPickUp(
                    CartProductPickUp(
                        menuCode = content.menuCode,
                        id = content.id,
                        name = content.name,
                        imgURL = content.imgURL,
                        price = content.price,
                        productType = content.productType,
                        qty = content.qty,
                        variantname = content.variantname,
                        detailVariant = content.detailVariant,
                        priceOriginal = content.priceOriginal,
                        idType = content.idType,
                        isBasePrice = content.isBasePrice,
                        menuDetail = content.menuDetail
                    )
                )
                Handler(Looper.getMainLooper()).postDelayed({
                    _removeFromCart.value = SingleEvents("removeFromCart")
                }, 0)
            }
            _datas.value = it
        }
    }

    override fun onDateClick() {
        _showDate.value = SingleEvents("show-date")
    }
}