package com.breadtalk.app.ui.special_offer

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.SpecialOffer
import com.breadtalk.app.specialOffer

class SpecialOfferController(private val listener: SpecialOfferControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var promoSelected = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is SpecialOffer -> addSpecialOffer(cellData, listener, index)
            }
        }
    }

    private fun addSpecialOffer(
        cellData: SpecialOffer,
        listener: SpecialOfferControllerListener,
        position: Int
    ) {
        specialOffer {
            id(cellData.menuCode)
            data(cellData)
            selected(cellData.menuCode == promoSelected)
            index(position)
            listener(listener)
        }
    }
}