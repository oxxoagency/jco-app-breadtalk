package com.breadtalk.app.ui.transaction_detail

import com.breadtalk.app.data.local.TransactionDetailOrderSummary

interface TransactionDetailControllerListener {
    fun onReOrderClick(position: Int)
    fun onCopyClick(transactionDetailOrderSummary: TransactionDetailOrderSummary)
    fun onCopyVAClick(transactionDetailOrderSummary: TransactionDetailOrderSummary)
    fun onHotlineClick()
}