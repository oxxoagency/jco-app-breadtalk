package com.breadtalk.app.ui.main.recent_order

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.EmptyItem
import com.breadtalk.app.data.local.LoadingPage
import com.breadtalk.app.data.local.RecentOrder
import com.breadtalk.app.emptyItem
import com.breadtalk.app.lytLoadingShimmer
import com.breadtalk.app.recentOrder

class RecentOrderController(private val recentOrderListener: RecentOrderListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is RecentOrder -> addRecentOrder(cellData, index, recentOrderListener)
                is EmptyItem -> addEmptyCart(cellData)
                is LoadingPage -> addLoadingPage(cellData)
            }
        }
    }

    private fun addRecentOrder(
        cellData: RecentOrder,
        position: Int,
        listener: RecentOrderListener
    ) {
        recentOrder {
            id(cellData.orderId)
            data(cellData)
            position(position)
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyItem) {
        emptyItem {
            id("empty-cart")
            data(cellData)
        }
    }

    private fun addLoadingPage(cellData: LoadingPage) {
        lytLoadingShimmer {
            id(cellData.hashCode())
        }
    }
}