package com.breadtalk.app.ui.main.wishlist

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.EmptyItem
import com.breadtalk.app.data.local.HomeMenuItem
import com.breadtalk.app.emptyItem
import com.breadtalk.app.homeMenuItem
import com.breadtalk.app.ui.main.home.HomeControllerListener

class WishlistController(private val listener: HomeControllerListener) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when(cellData) {
                is HomeMenuItem -> addHomeMenuItem(cellData, listener, index)
                is EmptyItem -> addEmptyCart(cellData)
            }
        }
    }

    private fun addHomeMenuItem(
        cellData: HomeMenuItem,
        listener: HomeControllerListener,
        index: Int
    ) {
        homeMenuItem {
            id(cellData.hashCode())
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addEmptyCart(cellData: EmptyItem) {
        emptyItem {
            id("empty-cart")
            data(cellData)
        }
    }
}