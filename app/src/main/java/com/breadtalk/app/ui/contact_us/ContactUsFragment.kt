package com.breadtalk.app.ui.contact_us

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentContactUsBinding
import com.breadtalk.app.ui.base.BaseFragment
import javax.inject.Inject


class ContactUsFragment @Inject constructor() :
    BaseFragment<FragmentContactUsBinding, ContactUsViewModel>() {

    override fun getViewModelClass(): Class<ContactUsViewModel> = ContactUsViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_contact_us

    override fun onViewReady(savedInstance: Bundle?) {
        initRecyclerview()
        initObserver()

        binding.topBar.btnBack.setOnClickListener {
            onBackPress()
        }

        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }
    }

    private fun initRecyclerview() {
        val controller = ContactUsController(viewModel)
        binding.recyclerview.setController(controller)
        viewModel.datas.observe(this, {
            controller.setData(it)
        })
    }

    private fun initObserver() {
        viewModel.showMenuDetail.observe(this, {
            it.getContentIfNotHandled().let { index ->
                when (index) {
                    ContactUsViewModel.PHONE -> {
                        val url = "https://api.whatsapp.com/send?phone=+628158998000"
                        val i = Intent(Intent.ACTION_VIEW)
                        i.data = Uri.parse(url)
                        startActivity(i)
                    }
                }
            }
        })
    }
}