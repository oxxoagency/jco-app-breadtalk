package com.breadtalk.app.ui.special_offer

import com.breadtalk.app.data.local.SpecialOffer

interface SpecialOfferControllerListener {
    fun onClick(speciaoffer: SpecialOffer)
}