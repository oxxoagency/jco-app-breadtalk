package com.breadtalk.app.ui.auth.change_password

import android.os.Bundle
import androidx.core.content.ContextCompat
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentChangePasswordBinding
import com.breadtalk.app.ui.base.BaseFragment
import de.mateware.snacky.Snacky
import javax.inject.Inject


class ChangePasswordFragment @Inject constructor() :
    BaseFragment<FragmentChangePasswordBinding, ChangePasswordViewModel>() {

    override fun getViewModelClass(): Class<ChangePasswordViewModel> =
        ChangePasswordViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_change_password

    override fun onViewReady(savedInstance: Bundle?) {
        binding.topBar.btnBack.setOnClickListener { onBackPress() }
        binding.apply {
            btnSave.setOnClickListener {
                edtConfirmPassword.error = null
                edtNewPassword.error = null
                edtConfirmPassword.error = null

                viewModel.changePassword(
                    edtCurrentPassword.editText?.text.toString(),
                    edtNewPassword.editText?.text.toString(),
                    edtConfirmPassword.editText?.text.toString())
            }

            btnCancel.setOnClickListener { onBackPress() }
        }

        initObserver()
    }

    private fun initObserver() {
        viewModel.successChangePassword.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled().let {
                Snacky.builder()
                    .setActivity(activity)
                    .setText(getString(R.string.success_change_password))
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.grey
                        )
                    )
                    .success()
                    .show()

                onBackPress()
            }
        })

        viewModel.failChangePassword.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled().let { message ->
                Snacky.builder()
                    .setActivity(activity)
                    .setText(message)
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.grey
                        )
                    )
                    .success()
                    .show()
            }
        })
    }
}