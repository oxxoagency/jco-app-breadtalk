package com.breadtalk.app.ui.pickup

import android.os.Bundle
import com.breadtalk.app.R
import com.breadtalk.app.data.remote.model.res.CityDataRes
import com.breadtalk.app.databinding.FragmentPickUpBinding
import com.breadtalk.app.ui.base.BaseFragment
import com.breadtalk.app.utils.DlgLoadingProgressBar
import javax.inject.Inject


class PickUpFragment @Inject constructor() :
    BaseFragment<FragmentPickUpBinding, PickUpViewModel>() {

    var dialog: DlgLoadingProgressBar? = null

    override fun getViewModelClass(): Class<PickUpViewModel> = PickUpViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_pick_up

    override fun onViewReady(savedInstance: Bundle?) {
        dialog = DlgLoadingProgressBar(requireContext())
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        binding.topBar.btnBack.setOnClickListener { onBackPress() }
        initObserver()
        initRecyclerView()
    }

    private fun initRecyclerView() {
        val controller = PickUpController(viewModel)
        binding.recyclerView.setController(controller)
        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.openCart.observe(this, {
            onBackPress()
        })

        viewModel.showLoading.observe(this, {
            it.getContentIfNotHandled()?.let { state ->
                if (state) {
                    dialog?.showPopUp()
                } else {
                    dialog?.dismissPopup()
                }
            }
        })

        viewModel.showCity.observe(this, {
            it.getContentIfNotHandled()?.let {
                viewModel.getCity()
                var searchCity = emptyList<CityDataRes>()
                val dlg = DialogCity()
                viewModel.cities.observe(this, { cities ->
                    dlg.showDialog(
                        requireActivity().supportFragmentManager,
                        PickUpFragment::class.java.simpleName,
                        object : DialogCity.DialogCityListener {
                            override fun getCity(): List<CityDataRes> = cities
                            override fun saveCity(city: CityDataRes): String {
                                viewModel.getOutletByCity(city.city)
                                return city.city.toString()
                            }

                            override fun searchCity(city: String): List<CityDataRes> {
                                if (city.isNotEmpty()) {
                                    viewModel.getSearchCity(city)
                                }

                                viewModel.cities.observe(viewLifecycleOwner, {
                                    searchCity = it
                                })
                                return searchCity
                            }
                        })
                })
            }
        })
    }
}