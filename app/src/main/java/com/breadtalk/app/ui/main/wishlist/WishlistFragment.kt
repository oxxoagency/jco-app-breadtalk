package com.breadtalk.app.ui.main.wishlist

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentWishlistBinding
import com.breadtalk.app.ui.base.BaseFragment
import javax.inject.Inject

class WishlistFragment @Inject constructor() :
    BaseFragment<FragmentWishlistBinding, WishlistViewModel>() {

    override fun getViewModelClass(): Class<WishlistViewModel> = WishlistViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_wishlist

    override fun onViewReady(savedInstance: Bundle?) {
        binding.topBar.btnBack.visibility = View.GONE
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = WishlistController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver(){
        viewModel.openProductDetail.observe(this, {
            it.getContentIfNotHandled()?.let { data ->
                val url = getString(R.string.linkProductDetail).replace("{id}", data.menuCode)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })
    }

}