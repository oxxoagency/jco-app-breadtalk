package com.breadtalk.app.ui.add_address

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.local.AddressReq
import com.breadtalk.app.data.local.AddressRes
import com.breadtalk.app.data.local.UpdateAddressReq
import com.breadtalk.app.data.remote.model.req.OutletReq
import com.breadtalk.app.data.repository.DeliveryRepository
import com.breadtalk.app.data.repository.PickupRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import javax.inject.Inject

class AddAddressViewModel @Inject constructor(
    private val deliveryRepository: DeliveryRepository,
    private val pickupRepository: PickupRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference
) : BaseViewModel() {

    private val _memberAddress = MutableLiveData<AddressRes>()
    val memberAddress: LiveData<AddressRes>
        get() = _memberAddress

    private val _successAddAddress = MutableLiveData<SingleEvents<String>>()
    val successAddAddress: LiveData<SingleEvents<String>>
        get() = _successAddAddress

    private val _errorMessage = MutableLiveData<SingleEvents<String>>()
    val errorMessage: LiveData<SingleEvents<String>>
        get() = _errorMessage

    fun getAddress(id: Int) {
        lastDisposable = deliveryRepository.getAddress(id)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                if (model.status == 200) _memberAddress.postValue(model)

            }, {
                handleError(it)
            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun loadLocation(lat: String, lng: String) {
        val body = OutletReq(3, lat, lng)
        lastDisposable = pickupRepository.getOutletLocation(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                if (it.status_code == 200) {
                    sharedPreference.saveCity(it.data[0].outlet_city.toString())
                    sharedPreference.savePostCode(it.data[0].outlet_postcode.toString())
                } else {
                    sharedPreference.saveCity("")
                }
            }, {

            })
    }

    fun setAddress(body: AddressReq) {
        lastDisposable = deliveryRepository.setAddress(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                if (model.status == 200) {
                    _successAddAddress.value = SingleEvents("success_add_address")
                } else {
                    _errorMessage.value =
                        SingleEvents("Alamat tidak bisa ditambahkan, karena diluar radius pengantaran")
                }
            }, {
                handleError(it)

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun updateAddress(body: UpdateAddressReq) {
        lastDisposable = deliveryRepository.updateAddress(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                if (model.status == 200) _successAddAddress.value =
                    SingleEvents("success_add_address")
            }, {
                handleError(it)
            })
        _successAddAddress.value = SingleEvents("success_add_address")
        lastDisposable?.let { compositeDisposable.add(it) }
    }

}