package com.breadtalk.app.ui.special_offer

import android.net.Uri
import android.os.Bundle
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentSpecialOfferBinding
import com.breadtalk.app.ui.base.BaseFragment
import javax.inject.Inject


class SpecialOfferFragment @Inject constructor() :
    BaseFragment<FragmentSpecialOfferBinding, SpecialOfferViewModel>() {

    override fun getViewModelClass(): Class<SpecialOfferViewModel> =
        SpecialOfferViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_special_offer

    override fun onViewReady(savedInstance: Bundle?) {
        binding.topBar.btnBack.setOnClickListener { onBackPress() }
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        initRecyclerView()
    }

    private fun initRecyclerView() {
        val controller = SpecialOfferController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })

        viewModel.promoSelected.observe(viewLifecycleOwner,{
            it.getContentIfNotHandled()?.let { menuCode ->
                controller.promoSelected = menuCode
                val url = getString(R.string.linkProductDetail).replace("{id}",menuCode)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })
    }

}