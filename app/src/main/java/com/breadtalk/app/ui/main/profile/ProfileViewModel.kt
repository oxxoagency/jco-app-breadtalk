package com.breadtalk.app.ui.main.profile

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.R
import com.breadtalk.app.data.local.*
import com.breadtalk.app.data.local.room.BreadTalkDatabase
import com.breadtalk.app.data.repository.AuthRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import io.reactivex.Completable
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val app: Application,
    private val database: BreadTalkDatabase
) : BaseViewModel(), ProfileControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _clearData = MutableLiveData<SingleEvents<String>>()
    val clearData: LiveData<SingleEvents<String>>
        get() = _clearData

    private val _showMenuDetail = MutableLiveData<SingleEvents<Int>>()
    val showMenuDetail: LiveData<SingleEvents<Int>>
        get() = _showMenuDetail

    private fun deleteAll(): Completable =
        database.cartDao().deleteAll()

    private fun deleteAllPickUp(): Completable =
        database.cartDao().deleteAllPickUp()

    fun loadData() {
        val temp = mutableListOf<BaseCell>()
        temp.add(
            ProfileHeader(
                img = "https://drive.google.com/uc?id=1gtrQXUFKsrkexSwWA9eN3Mh2Xtau3S3p",
                sharedPreference.loadName() ?: "",
                "",
                ""
            )
        )
        temp.add(
            ProfileMenuHeader(
                app.getString(R.string.account)
            )
        )
        temp.add(
            ProfileMenu(
                icon = R.drawable.ic_edit,
                R.string.edit_profile,
                1
            )
        )
        temp.add(
            ProfileMenu(
                icon = R.drawable.ic_lock,
                R.string.change_password,
                2
            )
        )
        temp.add(
            ProfileMenu(
                icon = R.drawable.ic_paper,
                R.string.my_order,
                3
            )
        )
        temp.add(
            ProfileMenu(
                icon = R.drawable.ic_translation_2,
                R.string.language,
                4
            )
        )
        temp.add(
            ProfileMenu(
                icon = R.drawable.ic_call,
                R.string.contact_us,
                5
            )
        )
        temp.add(
            ProfileMenuHeader(
                app.getString(R.string.general)
            )
        )
        temp.add(
            ProfileMenu(
                icon = R.drawable.ic_info_circle,
                R.string.term_conditions,
                6
            )
        )
        temp.add(
            ProfileMenu(
                icon = R.drawable.ic_shield_done,
                R.string.privacy_policy,
                7
            )
        )
        temp.add(ProfileFooter())
        _datas.value = temp
    }

    private fun showLoadingLogout(show: Boolean) {
        _datas.value?.let {
            val data = (it[it.size - 1] as ProfileFooter).copy()
            data.showLoading = show
            it[it.size - 1] = data
            lastDisposable = deleteAll().subscribeOn(schedulers.io())
                .observeOn(schedulers.io())
                .subscribe({}, {})
            lastDisposable = deleteAllPickUp().subscribeOn(schedulers.io())
                .observeOn(schedulers.io())
                .subscribe({}, {})
            lastDisposable?.let { compositeDisposable.add(it) }
            _datas.postValue(it)

        }
    }

    override fun onMenuClick(index: Int) {
        _datas.value?.let {
            val data = it[index] as ProfileMenu
            _showMenuDetail.value = SingleEvents(data.type)
        }
    }

    override fun onSignOut() {
        showLoadingLogout(true)

        lastDisposable = authRepository.logout()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                EventBus.getDefault().post(LogoutEvent())
                sharedPreference.apply {
                    removeAddress()
                    removeCity()
                    removePickUp()
                    removeSelectCity()
                    removeSelectLatitudeAddress()
                    removeSelectLongitudeAddress()
                    removeSelectPhoneNumber()
                }
                _clearData.value = SingleEvents("clear-data")
            }, { e ->
                showLoadingLogout(false)
                handleError(e)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    override fun onEditProfile() {

    }

    override fun onSocMedMnu(index: Int) {

    }

    companion object {
        const val EDIT = 1
        const val CHANGE_PASSWORD = 2
        const val ORDER = 3
        const val LANGUAGE = 4
        const val CONTACT_US = 5
        const val TERM_CONDITION = 6
        const val PRIVACY_POLICY = 7
    }
}