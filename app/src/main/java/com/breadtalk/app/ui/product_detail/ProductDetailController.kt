package com.breadtalk.app.ui.product_detail

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.*
import com.breadtalk.app.data.local.*

class ProductDetailController(private val listener: ProductDetailControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var price: String = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is ProductDetailContent -> addProductDetailContent(cellData, listener, index)
                is ProductDetailVariantHeader -> addProductDetailVariantHeader(cellData)
                is ProductDetailVariant -> addProductDetailVariant(cellData, listener, index)
                is ProductDetailVariantPackageHeader -> addProductDetailVariantPackageHeader(
                    cellData,
                    listener,
                    index
                )
                is Line -> addLine()
                is LoadingPage -> addLoadingPage(cellData)
            }
        }
    }

    private fun addProductDetailContent(
        cellData: ProductDetailContent,
        listener: ProductDetailControllerListener,
        position: Int
    ) {
        productDetailContent {
            id(cellData.menuCode)
            data(cellData)
            listener(listener)
            position(position)
            spanSizeOverride { _, _, _ -> 3 }
            price(price)
        }
    }

    private fun addProductDetailVariantHeader(cellData: ProductDetailVariantHeader) {
        productDetailVariantHeader {
            id("header")
            data(cellData)
            spanSizeOverride { _, _, _ -> 3 }
        }
    }

    private fun addProductDetailVariant(
        cellData: ProductDetailVariant,
        listener: ProductDetailControllerListener,
        position: Int
    ) {
        productDetailVariant {
            id(cellData.menuName)
            data(cellData)
            listener(listener)
            position(position)
        }

    }

    private fun addProductDetailVariantPackageHeader(
        cellData: ProductDetailVariantPackageHeader,
        listener: ProductDetailControllerListener,
        position: Int
    ) {
        productDetailVariantPackageHeader {
            id(cellData.name)
            data(cellData)
            listener(listener)
            position(position)
            spanSizeOverride { _, _, _ -> 3 }
        }

    }

    private fun addLine() {
        line {
            id("line")
            spanSizeOverride { _, _, _ -> 3 }
        }
    }

    private fun addLoadingPage(cellData: LoadingPage) {
        shimmerDetail {
            id(cellData.hashCode())
            spanSizeOverride { _, _, _ -> 3 }
        }
    }
}