package com.breadtalk.app.ui.main.profile

interface ProfileControllerListener {
    fun onMenuClick(index: Int)
    fun onSignOut()
    fun onEditProfile()
    fun onSocMedMnu(index: Int)
}