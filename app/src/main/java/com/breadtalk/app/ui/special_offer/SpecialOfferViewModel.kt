package com.breadtalk.app.ui.special_offer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.ModelHotPromo
import com.breadtalk.app.data.local.SpecialOffer
import com.breadtalk.app.data.remote.model.req.ProductPromoReq
import com.breadtalk.app.data.remote.model.res.Promo
import com.breadtalk.app.data.repository.HomeRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import com.breadtalk.app.utils.parseDateWithFormat
import javax.inject.Inject

class SpecialOfferViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference
) : BaseViewModel(),
    SpecialOfferControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private var _promoSelected = MutableLiveData<SingleEvents<String>>()
    val promoSelected: LiveData<SingleEvents<String>>
        get() = _promoSelected

    fun loadData() {
        val temp = _datas.value?.toMutableList() ?: mutableListOf()
        val body = ProductPromoReq(3, "Jakarta Barat", sharedPreference.loadPhoneNumber())
        lastDisposable = homeRepository.getHotPromo(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ model ->
                model.data?.map {
                    temp.add(
                        SpecialOffer(
                            menuCode = it.menu_code,
                            img = it.menu_image,
                            productName = localizationPromoName(it),
                            period = "${it.start_date.parseDateWithFormat("dd MMM yyyy")} - ${
                                it.end_date.parseDateWithFormat("dd MMM yyyy")
                            }"
                        )
                    )
                }
                _datas.postValue(temp)
            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun localizationPromoName(promo: Promo): String =
        if (sharedPreference.loadLanguage()?.contains("indonesia", true) == true) promo.menu_name
        else promo.menu_name_en

    override fun onClick(speciaoffer: SpecialOffer) {
        _promoSelected.value = SingleEvents(speciaoffer.menuCode.toString())
    }
}