package com.breadtalk.app.ui.delivery

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.data.local.AddressItem
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.SavePlace
import com.breadtalk.app.deliverSavedPlace
import com.breadtalk.app.deliveryAddAddressItem

class DeliveryController(private val listener: DeliveryControllerListener) :
    AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is AddressItem -> addAddressItem(cellData, listener, index)
                is SavePlace -> savePlace(listener)
            }
        }
    }

    private fun addAddressItem(
        cellData: AddressItem,
        listener: DeliveryControllerListener,
        index: Int
    ) {
        deliveryAddAddressItem {
            id(cellData.id)
            data(cellData)
            listener(listener)
            index(index)
        }
    }

    private fun savePlace(listener: DeliveryControllerListener) {
        deliverSavedPlace {
            id("add-save-place")
            listener(listener)
        }
    }
}