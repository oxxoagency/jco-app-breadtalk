package com.breadtalk.app.ui.detail_special_offer

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.local.*
import com.breadtalk.app.data.local.room.BreadTalkDatabase
import com.breadtalk.app.data.remote.model.req.BannerReq
import com.breadtalk.app.data.remote.model.res.RelatedMenuRes
import com.breadtalk.app.data.repository.HomeRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.*
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class DetailSpecialOfferViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val database: BreadTalkDatabase,
    private val app: Application,
) : BaseViewModel(),
    DetailSpecialOfferControllerListener {

    private val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _addToCart = MutableLiveData<SingleEvents<String>>()
    val addToCart: LiveData<SingleEvents<String>>
        get() = _addToCart

    private val _showMenu = MutableLiveData<SingleEvents<String>>()
    val showMenu: LiveData<SingleEvents<String>>
        get() = _showMenu

    private val _update = MutableLiveData<SingleEvents<String>>()
    val update: LiveData<SingleEvents<String>>
        get() = _update

    private val _relatedMenus = MutableLiveData<List<RelatedMenuRes>>()
    val relaatedMenus: LiveData<List<RelatedMenuRes>>
        get() = _relatedMenus

    private fun insertToCart(cartProduct: CartProduct): Completable =
        database.cartDao().insertCart(cartProduct)

    private fun insertToCartPickUp(cartProduct: CartProductPickUp): Completable =
        database.cartDao().insertCartPickUp(cartProduct)

    fun doInsertToCart(cartProduct: CartProduct) {
        compositeDisposable.add(
            insertToCart(cartProduct).subscribeOn(schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    handleError(it)
                })
        )
    }

    fun doInsertToCartPickUp(cartProduct: CartProductPickUp) {
        compositeDisposable.add(
            insertToCartPickUp(cartProduct).subscribeOn(schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    handleError(it)
                })
        )
    }

    fun loadData(id: String?) {
        val pInfo = app.packageManager.getPackageInfo(app.packageName, 0)
        val version = pInfo.versionCode
        val body = BannerReq(3, id, "android", version)

        lastDisposable = homeRepository.getBannerDetail(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                val temp = mutableListOf<BaseCell>()
                val banner = response.data
                temp.add(
                    DetailSpecialOffer(
                        id = banner.banner_id,
                        bannerDescription = banner.banner_description,
                        bannerImage = banner.banner_img,
                        promoPeriod = "${
                            banner.start_date?.parseDateAndTimeWithFormat("dd MMM yyyy").toString()
                        } - ${
                            banner.end_date?.parseDateAndTimeWithFormat("dd MMM yyyy").toString()
                        }",
                        relatedMenu = banner.related_menu,
                        version = sharedPreference.loadVersion()
                    )
                )
                banner.related_menu?.map { relatedMenu ->
                    RelatedMenu(
                        id = relatedMenu.id_menu,
                        menuCode = relatedMenu.menu_code,
                        menuName = localizationMenuName(relatedMenu),
                        menuImage = relatedMenu.menu_image,
                        isPromo = relatedMenu.is_promo == "1",
                        price = relatedMenu.menu_price,
                        priceText = Converter.rupiah(relatedMenu.menu_price.toString()),
                    )
                }
                _datas.postValue(temp)
                _relatedMenus.postValue(banner.related_menu ?: mutableListOf())
//                _relatedMenus.postValue(banner.related_menu ?: mutableListOf())
            }, {
                handleError(it)
            })

        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun localizationMenuName(product: RelatedMenuRes): String =
        if (sharedPreference.loadLanguage()
                ?.contains("indonesia", true) == true
        ) product.menu_name.toString()
        else product.menu_name_en.toString()

    override fun onAddToCart(position: Int) {
        _datas.value.let {
            val banner = (it)?.get(position) as DetailSpecialOffer
            val pInfo = app.packageManager.getPackageInfo(app.packageName, 0)
            val version = pInfo.versionCode
//            if (version < sharedPreference.loadVersion() ?: 0) {
//                _update.value = SingleEvents("Update Aplikasi")
//            } else {
            if (banner.relatedMenu?.size ?: 0 <= 1) {
                banner.relatedMenu?.map { relatedMenu ->
                    _addToCart.value = SingleEvents("add-to-cart")
                    doInsertToCart(
                        CartProduct(
                            menuCode = relatedMenu.menu_code,
                            name = relatedMenu.menu_name,
                            imgURL = relatedMenu.menu_image,
                            price = relatedMenu.menu_price?.toDouble() ?: 0.0,
                            productType = "",
                            qty = 1,
                            priceOriginal = relatedMenu.menu_price?.toDouble() ?: 0.0,
                            detailVariant = mutableListOf(),
                            variantname = "",
                            isBasePrice = 0,
                            idType = 0,
                            menuDetail = ""
                        )
                    )
                    doInsertToCartPickUp(
                        CartProductPickUp(
                            menuCode = relatedMenu.menu_code,
                            name = relatedMenu.menu_name,
                            imgURL = relatedMenu.menu_image,
                            price = relatedMenu.menu_price?.toDouble() ?: 0.0,
                            productType = "",
                            qty = 1,
                            priceOriginal = relatedMenu.menu_price?.toDouble() ?: 0.0,
                            detailVariant = mutableListOf(),
                            variantname = "",
                            isBasePrice = 0,
                            idType = 0,
                            menuDetail = ""
                        )
                    )
                }
            } else {
                _showMenu.value = SingleEvents("show-menu")
            }
//            }


        }
    }

    override fun onClickMenu(position: Int) {

    }
}