package com.breadtalk.app.ui.voucher.claim_voucher

interface ClaimVoucherControllerListener {
    fun onClaimClick(position: Int)
}