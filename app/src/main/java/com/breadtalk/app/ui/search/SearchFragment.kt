package com.breadtalk.app.ui.search

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.SearchView
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentSearchBinding
import com.breadtalk.app.ui.base.BaseFragment
import com.breadtalk.app.utils.KeyboardUtil
import javax.inject.Inject


class SearchFragment @Inject constructor() :
    BaseFragment<FragmentSearchBinding, SearchViewModel>() {


    override fun getViewModelClass(): Class<SearchViewModel> = SearchViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_search

    override fun onViewReady(savedInstance: Bundle?) {

        binding.apply {
            topBar.searchView.apply {
                requestFocusFromTouch()
                isFocusable = true
                setOnQueryTextFocusChangeListener { view, hasFocus ->
                    if (hasFocus) {
                        showInputMethod(view.findFocus())
                    }
                    viewModel.doShowSearchLatest(hasFocus)
                }
            }
            topBar.cancel.setOnClickListener {
                onBackPress()
            }
            topBar.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    viewModel.loadSearch(query)
                    KeyboardUtil.hideKeyboard(activity)
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean = false
            })
        }
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        initRecyclerView()
    }

    private fun initRecyclerView() {
        val controller = SearchController(viewModel)
        binding.recyclerView.setController(controller)
        viewModel.datas.observe(this, {
            controller.data = it
        })

        viewModel.showSearchLatest.observe(viewLifecycleOwner, {
            controller.showSearchLatest = it
        })


        viewModel.menuSelected.observe(viewLifecycleOwner, {
            controller.menuSelected = it
        })

        viewModel.openProductDetail.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { data ->
                val url = getString(R.string.linkProductDetail).replace("{id}", data.menuCode)
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })
    }

    private fun showInputMethod(view: View) {
        val imm =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.showSoftInput(view, 0)
    }
}