package com.breadtalk.app.ui.detail_special_offer

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.breadtalk.app.R
import com.breadtalk.app.data.remote.model.res.RelatedMenuRes
import com.breadtalk.app.databinding.ItemBannerMenuBinding
import com.breadtalk.app.utils.Converter

class BannerMenuAdapter(private val data: (RelatedMenuRes) -> Unit) :
    RecyclerView.Adapter<BannerMenuAdapter.BannerMenuViewHolder>() {

    private var relatedMenu = emptyList<RelatedMenuRes>()

    private var selectedItemPosition: Int? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): BannerMenuAdapter.BannerMenuViewHolder {
        return BannerMenuViewHolder(ItemBannerMenuBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: BannerMenuAdapter.BannerMenuViewHolder, position: Int) {
        holder.bind(relatedMenu[position], position)
        holder.itemView.setOnClickListener {
            selectedItemPosition = position
            notifyDataSetChanged()
            Log.d("cekposition", selectedItemPosition.toString())
        }
    }

    override fun getItemCount(): Int {
        return relatedMenu.size
    }

    inner class BannerMenuViewHolder(private val binding: ItemBannerMenuBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(relatedMenu: RelatedMenuRes, position: Int) {
            binding.apply {
                data = relatedMenu
                price.text = Converter.rupiah(relatedMenu.menu_price.toString())


                if (position == selectedItemPosition) {
                    Log.d("cekposition", "true")
                    radioButton.setTextColor(ContextCompat.getColor(radioButton.context, R.color.orange))
                    price.setTextColor(ContextCompat.getColor(radioButton.context, R.color.orange))
                    this@BannerMenuAdapter.data(relatedMenu)
                } else {
                    radioButton.setTextColor(ContextCompat.getColor(radioButton.context, R.color.black))
                    price.setTextColor(ContextCompat.getColor(radioButton.context, R.color.black))
                }
                executePendingBindings()
            }
        }
    }

    internal fun setData(relatedMenu: List<RelatedMenuRes>) {
        this.relatedMenu = relatedMenu
        notifyDataSetChanged()
    }
}