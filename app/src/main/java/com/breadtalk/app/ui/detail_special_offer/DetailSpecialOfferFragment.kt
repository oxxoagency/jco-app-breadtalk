package com.breadtalk.app.ui.detail_special_offer

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.breadtalk.app.R
import com.breadtalk.app.data.local.CartProduct
import com.breadtalk.app.data.local.CartProductPickUp
import com.breadtalk.app.data.remote.model.res.RelatedMenuRes
import com.breadtalk.app.databinding.FragmentDetailSpecialOfferBinding
import com.breadtalk.app.ui.base.BaseFragment
import de.mateware.snacky.Snacky
import javax.inject.Inject


class DetailSpecialOfferFragment @Inject constructor() :
    BaseFragment<FragmentDetailSpecialOfferBinding, DetailSpecialOfferViewModel>() {

    override fun getViewModelClass(): Class<DetailSpecialOfferViewModel> =
        DetailSpecialOfferViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_detail_special_offer

    override fun onViewReady(savedInstance: Bundle?) {
        binding.topBar.btnBack.setOnClickListener { onBackPress() }
        if (!isFragmentFromPaused) {
            arguments?.let {
                it.getString("id")?.let { it1 -> viewModel.loadData(it1) }
            }
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = DetailSpecialOfferController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.addToCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                Snacky.builder()
                    .setActivity(activity)
                    .setText("Sukses ditambahkan ke keranjang")
                    .setDuration(Snacky.LENGTH_SHORT)
                    .setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.grey
                        )
                    )
                    .success()
                    .show()
            }
        })

        viewModel.showMenu.observe(viewLifecycleOwner, { showMenu ->
            viewModel.relaatedMenus.observe(this, { data ->
                showMenu.getContentIfNotHandled()?.let {
                    val dlg = DialogBannerDetail()
                    dlg.showDialog(
                        requireActivity().supportFragmentManager,
                        "BannerDetailFragment",
                        object : DialogBannerDetail.DialogBannerDetailListener {
                            override fun promoMenu(): List<RelatedMenuRes> {
                                return data
                            }

                            override fun getPromo(relatedMenu: RelatedMenuRes) {
                                viewModel.doInsertToCart(
                                    CartProduct(
                                        menuCode = relatedMenu.menu_code,
                                        name = relatedMenu.menu_name,
                                        imgURL = relatedMenu.menu_image,
                                        price = relatedMenu.menu_price?.toDouble() ?: 0.0,
                                        productType = "",
                                        qty = 1,
                                        priceOriginal = relatedMenu.menu_price?.toDouble() ?: 0.0,
                                        detailVariant = mutableListOf(),
                                        variantname = "",
                                        isBasePrice = 0,
                                        idType = 0,
                                        menuDetail = ""
                                    )
                                )

                                viewModel.doInsertToCartPickUp(
                                    CartProductPickUp(
                                        menuCode = relatedMenu.menu_code,
                                        name = relatedMenu.menu_name,
                                        imgURL = relatedMenu.menu_image,
                                        price = relatedMenu.menu_price?.toDouble() ?: 0.0,
                                        productType = "",
                                        qty = 1,
                                        priceOriginal = relatedMenu.menu_price?.toDouble() ?: 0.0,
                                        detailVariant = mutableListOf(),
                                        variantname = "",
                                        isBasePrice = 0,
                                        idType = 0,
                                        menuDetail = ""
                                    )
                                )
                                Snacky.builder()
                                    .setActivity(activity)
                                    .setText("Sukses ditambahkan ke keranjang")
                                    .setDuration(Snacky.LENGTH_SHORT)
                                    .setActionText(R.string.close)
                                    .setBackgroundColor(
                                        ContextCompat.getColor(
                                            requireContext(),
                                            R.color.grey
                                        )
                                    )
                                    .success()
                                    .show()
                            }

                            override fun close() {
                                dlg.dissmissDialog()
                            }
                        }
                    )
                }
            })
        })
        viewModel.update.observe(viewLifecycleOwner, {
            openAppOnPlayStore(requireContext(), null)
        })
    }

    private fun openAppOnPlayStore(ctx: Context, packageName: String?) {
        var packageName = packageName
        if (packageName == null) {
            packageName = ctx.packageName
        }
        val uri = Uri.parse("http://play.google.com/store/apps/details?id=$packageName")
        openURI(ctx, uri, "Play Store not found in your device")
    }

    private fun openURI(
        ctx: Context,
        uri: Uri?,
        errorMessage: String?,
    ) {
        val i = Intent(Intent.ACTION_VIEW, uri)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        if (ctx.packageManager.queryIntentActivities(i, 0).size > 0) {
            ctx.startActivity(i)
        } else if (errorMessage != null) {
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
        }
    }
}