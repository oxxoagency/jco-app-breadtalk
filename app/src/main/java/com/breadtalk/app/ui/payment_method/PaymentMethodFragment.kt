package com.breadtalk.app.ui.payment_method

import android.os.Bundle
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentPaymentMethodBinding
import com.breadtalk.app.ui.base.BaseFragment
import javax.inject.Inject


class PaymentMethodFragment @Inject constructor() :
    BaseFragment<FragmentPaymentMethodBinding, PaymentMethodViewModel>() {

    override fun getViewModelClass(): Class<PaymentMethodViewModel> =
        PaymentMethodViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_payment_method

    override fun onViewReady(savedInstance: Bundle?) {
        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }
        binding.topBar.btnBack.setOnClickListener { onBackPress() }
        initRecyclerView()
    }

    private fun initRecyclerView() {
        val controller = PaymentMethodController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })

        viewModel.openCart.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                parentFragmentManager.popBackStack()
            }
        })

    }
}