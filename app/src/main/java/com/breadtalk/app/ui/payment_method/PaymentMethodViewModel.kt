package com.breadtalk.app.ui.payment_method

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.local.BaseCell
import com.breadtalk.app.data.local.Payment
import com.breadtalk.app.data.repository.PaymentRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import javax.inject.Inject

class PaymentMethodViewModel @Inject constructor(
    private val sharedPreference: SharedPreference,
    private val repository: PaymentRepository,
    private val schedulers: SchedulerProvider
) : BaseViewModel(), PaymentMethodControllerListener {
    private val _datas = MutableLiveData<List<BaseCell>>()
    val datas: LiveData<List<BaseCell>>
        get() = _datas

    private val _openCart = MutableLiveData<SingleEvents<String>>()
    val openCart: LiveData<SingleEvents<String>>
        get() = _openCart

    fun loadData() {
        val temp = _datas.value?.toMutableList() ?: mutableListOf()
        lastDisposable = repository.getPaymentList()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                response.data.map { paymentList ->
                    temp.add(
                        Payment(
                            paymentMethod = paymentList.payment_method,
                            paymentIcon = paymentList.payment_icon,
                            paymentName = paymentList.payment_name
                        )
                    )
                }
                _datas.postValue(temp)
            }, {

            })

    }

    override fun onClick(position: Int) {
        _datas.value.let {
            val payment = it?.get(position) as Payment
            sharedPreference.savePaymentMethod(payment.paymentMethod.toString())
            sharedPreference.savePaymentName(payment.paymentName.toString())
            sharedPreference.savePaymentIcon(payment.paymentIcon.toString())
            sharedPreference.saveRefreshStateCartDetail(true)
            _openCart.value = SingleEvents("open-cart")
        }
    }
}