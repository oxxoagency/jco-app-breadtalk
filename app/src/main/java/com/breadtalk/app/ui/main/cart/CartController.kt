package com.breadtalk.app.ui.main.cart

import com.airbnb.epoxy.AsyncEpoxyController
import com.breadtalk.app.*
import com.breadtalk.app.data.local.*

class CartController(
    private val listener: CartControllerListener,
) : AsyncEpoxyController() {

    var data: List<BaseCell> = emptyList()
        set(value) {
            field = value
            requestModelBuild()
        }

    var note = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var date = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var paymentMethod = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var paymentIcon = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    var pickUp = ""
        set(value) {
            field = value
            requestModelBuild()
        }

    override fun buildModels() {
        data.forEachIndexed { index, cellData ->
            when (cellData) {
                is CartSwitch -> addCartSwitch(listener, cellData)
                is CartDeliveryAddress -> addCartDeliveryAddress(cellData, index, listener)
                is CartProduct -> addCartProduct(cellData, index, listener)
                is CartProductPickUp -> addCartProductPickUp(cellData, index, listener)
                is CartDetail -> addCartDetail(cellData, index, listener)
                is EmptyItem -> addEmptyCart(cellData)
                is CartPickupAddress -> addCartPickupAddress(cellData, index, listener)
            }
        }
    }

    private fun addCartSwitch(listener: CartControllerListener, cellData: CartSwitch) {
        cartSwitch {
            id("switch_cart")
            listener(listener)
            data(cellData)
        }
    }

    private fun addCartDeliveryAddress(
        cellData: CartDeliveryAddress,
        index: Int,
        listener: CartControllerListener,
    ) {
        cartDeliveryAddress {
            id("delivery_address")
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addCartProduct(
        cellData: CartProduct,
        index: Int,
        listener: CartControllerListener,
    ) {
        cartProduct {
            id(cellData.id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addCartProductPickUp(
        cellData: CartProductPickUp,
        index: Int,
        listener: CartControllerListener,
    ) {
        cartProductPickUp {
            id(cellData.id)
            data(cellData)
            index(index)
            listener(listener)
        }
    }

    private fun addCartDetail(cellData: CartDetail, index: Int, listener: CartControllerListener) {
        cartDetail {
            id("cart_detail")
            data(cellData)
            index(index)
            listener(listener)
            note(note)
            paymentMethod(paymentMethod)
            paymentIcon(paymentIcon)
            date(date)
        }
    }

    private fun addEmptyCart(cellData: EmptyItem) {
        emptyItem {
            id("empty-cart")
            data(cellData)
        }
    }

    private fun addCartPickupAddress(
        cellData: CartPickupAddress,
        index: Int,
        listener: CartControllerListener,
    ) {
        cartPickUpAddress {
            id("pickup_address")
            data(cellData)
            index(index)
            listener(listener)
            pickUp(pickUp)
        }
    }
}