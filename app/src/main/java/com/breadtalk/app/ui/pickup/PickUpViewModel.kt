package com.breadtalk.app.ui.pickup

import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.data.local.*
import com.breadtalk.app.data.local.room.BreadTalkDatabase
import com.breadtalk.app.data.remote.model.req.*
import com.breadtalk.app.data.remote.model.res.CityDataRes
import com.breadtalk.app.data.repository.HomeRepository
import com.breadtalk.app.data.repository.PickupRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import io.reactivex.Completable
import io.reactivex.Flowable
import java.math.RoundingMode
import javax.inject.Inject

class PickUpViewModel @Inject constructor(
    private val homeRepository: PickupRepository,
    private val productRepository: HomeRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val database: BreadTalkDatabase,
) : BaseViewModel(), PickupControllerListener {

    val _datas = MutableLiveData<MutableList<BaseCell>>()
    val datas: LiveData<MutableList<BaseCell>>
        get() = _datas

    private val _openCart = MutableLiveData<SingleEvents<String>>()
    val openCart: LiveData<SingleEvents<String>>
        get() = _openCart

    private val _cities = MutableLiveData<List<CityDataRes>>()
    val cities: LiveData<List<CityDataRes>>
        get() = _cities

    private val _showCity = MutableLiveData<SingleEvents<String>>()
    val showCity: LiveData<SingleEvents<String>>
        get() = _showCity

    private val _showLoading = MutableLiveData<SingleEvents<Boolean>>()
    val showLoading: LiveData<SingleEvents<Boolean>>
        get() = _showLoading

    fun cartProduct(): Flowable<List<CartProductPickUp>> =
        database.cartDao().getAllCartProductPickUp()

    private fun updatePrice(priceOriginal: Double, price: Double, id: Int): Completable =
        database.cartDao().updatePricePickUp(priceOriginal, price, id)

    fun loadData() {
        val body = OutletReq(3, sharedPreference.loadLatitude(), sharedPreference.loadLongitude(), 1)
        lastDisposable = homeRepository.getOutletLocation(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ data ->
                val temp = mutableListOf<BaseCell>()

                temp.add(LocationSearch("temp"))
                data.data.map { model ->
                    temp.add(
                        PickupItem(
                            _id = model.outlet_id.toString(),
                            address = model.outlet_address.toString(),
                            distance = "${
                                model.distance?.toDouble()?.toBigDecimal()
                                    ?.setScale(1, RoundingMode.UP)?.setScale(1, RoundingMode.UP)
                            }Km",
                            placeName = model.outlet_name,
                            postCode = model.outlet_postcode.toString(),
                            outletCode = model.outlet_code,
                            city = model.outlet_city.toString(),
                            latitude = model.outlet_latitude.toString(),
                            longiutde = model.outlet_longitude.toString()
                        )
                    )
                    Log.d("cekpickup", temp.toString())
                    _datas.postValue(temp)

                }

            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun doUpdateCart(priceOriginal: Double, price: Double, id: Int) {
        lastDisposable =
            updatePrice(priceOriginal, price, id)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe()
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun loadProductDetail(
        menuCode: String,
        city: String,
        qty: Int,
        id: Int,
        cartDetailVariant: MutableList<CartDetailVariant>,
    ) {
        val body = ProductDetailReq(
            "3",
            city,
            menuCode,
            sharedPreference.loadPhoneNumber().toString()
        )

        lastDisposable = productRepository.getProductDetail(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({ response ->
                val data = response.data
                var price: Int?
                if (response.status == 200) {
                    if (data.is_baseprice == 0) {
                        price = data.menu_price?.times(qty)
                        doUpdateCart(
                            data.menu_price?.toDouble() ?: 0.0,
                            price?.toDouble() ?: 0.0,
                            id
                        )
                    } else {
                        var priceBasePrice = 0
                        data.details?.map { detailVariant ->
                            cartDetailVariant.map { cartDetailVariant ->
                                if (detailVariant.package_code == cartDetailVariant.menuCode) {
                                    priceBasePrice += detailVariant.package_price.times(
                                        cartDetailVariant.qty)
                                    Log.d("cekprice", priceBasePrice.toString())
                                }
                            }

                        }
                        doUpdateCart(
                            data.menu_price?.toDouble() ?: 0.0,
                            priceBasePrice.toDouble(),
                            id
                        )

                    }
                }
            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    private fun getItemCarts(city: String) {
        lastDisposable = cartProduct().subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .take(1)
            .subscribe({
                it.forEach { cartProduct ->
                    if (cartProduct.isBasePrice == 0) {
                        loadProductDetail(
                            cartProduct.menuCode.toString(),
                            city,
                            cartProduct.qty,
                            cartProduct.id,
                            mutableListOf()
                        )
                    } else {
                        loadProductDetail(
                            cartProduct.menuCode.toString(),
                            city,
                            cartProduct.qty,
                            cartProduct.id,
                            cartProduct.detailVariant ?: return@subscribe
                        )

                    }
                }
            }, {

            })
        Handler(Looper.getMainLooper()).postDelayed({
            _showLoading.value = SingleEvents(false)
            _openCart.value = SingleEvents("open-cart")
        }, 1000)
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getCity() {
        val body = CityReq(3, "indonesia")
        lastDisposable = homeRepository.getCity(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe({
                _cities.postValue(it.data)
            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getSearchCity(city: String?) {
        val body = CitySearchReq(3, "indonesia", city)
        lastDisposable = homeRepository.getCitySearch(body)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribe({
                _cities.postValue(it.data)
            }, {

            })
        lastDisposable?.let { compositeDisposable.add(it) }
    }

    fun getOutletByCity(city: String?) {
        _datas.value?.let {
            val body = OutletCityReq(3, city)
            lastDisposable = homeRepository.getOutletByCity(body)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe({ res ->
                    it.removeAll { data -> data is PickupItem }
                    res.data.map { model ->
                        it.add(
                            PickupItem(
                                _id = model.outlet_id.toString(),
                                address = model.outlet_address.toString(),
                                distance = "${
                                    model.distance?.toDouble()?.toBigDecimal()
                                        ?.setScale(1, RoundingMode.UP)?.setScale(1, RoundingMode.UP)
                                }Km",
                                placeName = model.outlet_name,
                                postCode = model.outlet_postcode.toString(),
                                outletCode = model.outlet_code,
                                city = model.outlet_city.toString(),
                                latitude = model.outlet_latitude.toString(),
                                longiutde = model.outlet_longitude.toString()
                            )
                        )
                    }
                    _datas.value = it
                }, {

                })
        }
    }

    override fun onClick(index: Int) {
        _datas.value.let {
            _showLoading.value = SingleEvents(true)
            val pickUp = (it?.get(index) as PickupItem).copy()
            sharedPreference.savePickUp(pickUp.placeName)
            sharedPreference.savePostCode(pickUp.postCode)
            sharedPreference.saveOutletCode(pickUp.outletCode)
            sharedPreference.saveOutletId(pickUp._id)
            sharedPreference.savePickUpAddress(pickUp.address)
            sharedPreference.savePickUpCity(pickUp.city)
            sharedPreference.saveSelectLatitudePickUp(pickUp.latitude)
            sharedPreference.saveSelectLongitudePickup(pickUp.longiutde)
            sharedPreference.saveDetailAddressPickUp(pickUp.address)
            Log.d("cekpickupcity", pickUp.city)
            sharedPreference.saveStatePickup(true)
//            _openToCart.value = SingleEvents("open-cart")
            getItemCarts(sharedPreference.loadPickUpCity().toString())
        }
    }

    override fun onSearchClick() {
        _showCity.value = SingleEvents("show-city")
    }
}