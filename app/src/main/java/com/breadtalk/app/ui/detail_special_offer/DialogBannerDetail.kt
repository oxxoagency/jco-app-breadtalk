package com.breadtalk.app.ui.detail_special_offer

import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.breadtalk.app.R
import com.breadtalk.app.data.remote.model.res.RelatedMenuRes
import com.breadtalk.app.databinding.DlgBannerDetailBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import javax.inject.Inject


class DialogBannerDetail @Inject constructor() : BottomSheetDialogFragment() {

    private val TAG = DialogBannerDetail::class.java.simpleName

    private lateinit var binding: DlgBannerDetailBinding
    private lateinit var listener: DialogBannerDetailListener
    private var mBehavior: BottomSheetBehavior<*>? = null
    private val adapter by lazy { BannerMenuAdapter { addToCart(it) } }

    override fun getTheme(): Int {
        return R.style.DialogFullWidth
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = DlgBannerDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.setData(listener.promoMenu())

        binding.apply {
            recyclerViewBanner.apply {
                adapter = this@DialogBannerDetail.adapter
                layoutManager = LinearLayoutManager(context)
            }
            btnDlgClose.setOnClickListener {
                dissmissDialog()
                listener.close()
                Log.d("clicked","clicked")
            }
        }
        isCancelable = false
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        val view = View.inflate(context, R.layout.dlg_banner_detail, null)

        val linearLayout = view.findViewById<ConstraintLayout>(R.id.container)
        val params = linearLayout.layoutParams as? ConstraintLayout.LayoutParams
        params?.height = getScreenHeight()
        dialog.setContentView(view)
        mBehavior = BottomSheetBehavior.from(view.parent as View)
        return dialog
    }

    override fun onStart() {
        super.onStart()
        mBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun getScreenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    private fun addToCart(relatedMenuRes: RelatedMenuRes) {
        binding.btnSave.setOnClickListener {
            listener.getPromo(relatedMenuRes)
            dissmissDialog()
        }
    }

    fun showDialog(
        fragmentManager: FragmentManager,
        tag: String?,
        listener: DialogBannerDetailListener,
    ) {
        show(fragmentManager, tag)
        this.listener = listener

    }

    fun dissmissDialog() {
        dialog?.cancel()
    }

    interface DialogBannerDetailListener {
        fun promoMenu(): List<RelatedMenuRes>
        fun getPromo(relatedMenuRes: RelatedMenuRes)
        fun close()
    }
}