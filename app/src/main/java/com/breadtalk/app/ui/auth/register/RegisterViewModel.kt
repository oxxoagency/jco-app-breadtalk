package com.breadtalk.app.ui.auth.register

import android.app.Application
import android.webkit.WebView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breadtalk.app.R
import com.breadtalk.app.data.remote.model.req.RegisterReq
import com.breadtalk.app.data.repository.AuthRepository
import com.breadtalk.app.ui.base.BaseViewModel
import com.breadtalk.app.utils.NetworkUtils
import com.breadtalk.app.utils.SchedulerProvider
import com.breadtalk.app.utils.SharedPreference
import com.breadtalk.app.utils.SingleEvents
import javax.inject.Inject

class RegisterViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val schedulers: SchedulerProvider,
    private val sharedPreference: SharedPreference,
    private val app: Application,
) : BaseViewModel() {

    private val _errorFullName = MutableLiveData<SingleEvents<String>>()
    val errorFullName: LiveData<SingleEvents<String>>
        get() = _errorFullName

    private val _errorEmail = MutableLiveData<SingleEvents<String>>()
    val errorEmail: LiveData<SingleEvents<String>>
        get() = _errorEmail

    private val _errorPassword = MutableLiveData<SingleEvents<String>>()
    val errorPassword: LiveData<SingleEvents<String>>
        get() = _errorPassword

    private val _errorConfirmPassword = MutableLiveData<SingleEvents<String>>()
    val errorConfirmPassword: LiveData<SingleEvents<String>>
        get() = _errorConfirmPassword

    private val _showToast = MutableLiveData<SingleEvents<String>>()
    val showToast: LiveData<SingleEvents<String>>
        get() = _showToast

    private val _registerFailed = MutableLiveData<SingleEvents<String>>()
    val registerFailed: LiveData<SingleEvents<String>>
        get() = _registerFailed

    private val _closeRegisterPage = MutableLiveData<SingleEvents<String>>()
    val closeRegisterPage: LiveData<SingleEvents<String>>
        get() = _closeRegisterPage

    private val _showLoadingOTP = MutableLiveData<Boolean>()
    val showLoadingOTP: LiveData<Boolean>
        get() = _showLoadingOTP

    fun register(
        fullName: String,
        email: String,
        phone: String,
        password: String,
        confirmPassword: String
    ) {
        val phoneNumber = phone.replace("-", "")
        when {
            fullName.isEmpty() -> {
                _errorFullName.value = SingleEvents(app.getString(R.string.err_fullname_empty))
            }
            email.isEmpty() -> {
                _errorEmail.value = SingleEvents(app.getString(R.string.err_email_empty))
            }
            !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                _errorEmail.value = SingleEvents(app.getString(R.string.err_email_invalid))
            }
            password.isEmpty() -> {
                _errorPassword.value = SingleEvents(app.getString(R.string.err_password_empty))
            }
            password.length < 5 -> {
                _errorPassword.value = SingleEvents(app.getString(R.string.err_password_length))
            }
            confirmPassword.isEmpty() -> {
                _errorConfirmPassword.value =
                    SingleEvents(app.getString(R.string.err_password_empty))
            }
            confirmPassword.length < 5 -> {
                _errorConfirmPassword.value =
                    SingleEvents(app.getString(R.string.err_password_length))
            }
            confirmPassword != password -> {
                _errorConfirmPassword.value =
                    SingleEvents(app.getString(R.string.err_password_not_same))
            }
            else -> {
                _showLoadingOTP.postValue(true)
                val body = RegisterReq(
                    fullName,
                    email,
                    phoneNumber,
                    "",
                    password,
                    confirmPassword,
                    NetworkUtils.getIPAddress(true),
                    WebView(app).settings.userAgentString
                )
                lastDisposable = authRepository.register(body)
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
                    .subscribe({ model ->
                        _showLoadingOTP.postValue(false)
                        sharedPreference.save(
                            SharedPreference.ACCESS_TOKEN,
                            model.token?.accessToken.toString()
                        )
                        sharedPreference.save(
                            SharedPreference.REFRESH_TOKEN,
                            model.token?.refreshToken.toString()
                        )
                        sharedPreference.save(SharedPreference.FROM_LOGIN, true)

                        if (model.status == 200) {
                            _showToast.value = SingleEvents(app.getString(R.string.account_created))
                            _closeRegisterPage.value = SingleEvents("close")
                        } else {
                            _showToast.value = SingleEvents(model.message.toString())
                        }
                    }, { e ->
                        _showLoadingOTP.postValue(false)
                        _registerFailed.value =
                            SingleEvents("Sedang terjadi gangguan, mohon coba lagi")
                    })

                lastDisposable?.let { compositeDisposable.add(it) }
            }
        }
    }
}