package com.breadtalk.app.ui.detail_special_offer

interface DetailSpecialOfferControllerListener {
    fun onAddToCart(position: Int)
    fun onClickMenu(position: Int)
}