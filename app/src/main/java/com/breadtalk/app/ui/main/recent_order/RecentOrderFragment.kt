package com.breadtalk.app.ui.main.recent_order

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.breadtalk.app.R
import com.breadtalk.app.databinding.FragmentRecentOrderBinding
import com.breadtalk.app.ui.base.BaseFragment
import javax.inject.Inject

class RecentOrderFragment @Inject constructor() :
    BaseFragment<FragmentRecentOrderBinding, RecentOrderViewModel>() {

    override fun getViewModelClass(): Class<RecentOrderViewModel> = RecentOrderViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_recent_order

    override fun onViewReady(savedInstance: Bundle?) {
        binding.topBar.btnBack.visibility = View.GONE

        if (!isFragmentFromPaused) {
            viewModel.loadData()
        }

        initRecyclerView()
        initObserver()
    }

    private fun initRecyclerView() {
        val controller = RecentOrderController(viewModel)
        binding.recyclerView.setController(controller)

        viewModel.datas.observe(this, {
            controller.data = it
        })
    }

    private fun initObserver() {
        viewModel.showDetail.observe(this, {
            it.getContentIfNotHandled()?.let { data ->
                val url = getString(R.string.linkTransactionDetail).replace(
                    "{id}",
                    data.orderId.toString()
                )
                val uri = Uri.parse(url)
                navigateTo(uri)
            }
        })
    }
}