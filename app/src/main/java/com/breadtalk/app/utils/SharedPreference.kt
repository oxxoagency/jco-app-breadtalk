package com.breadtalk.app.utils

import android.content.Context
import android.content.SharedPreferences
import com.breadtalk.app.R

class SharedPreference(val context: Context) {

    companion object {
        val ACCESS_TOKEN = "ACCESS_TOKEN"
        val REFRESH_TOKEN = "REFRESH_TOKEN"
        val DATA_HOME = "DATA_HOME"
        val PHONE_NUMBER = "PHONE_NUMBER"
        val POINTS = "POINTS"
        val EMAIL = "EMAIL"
        val NAME = "NAME"
        val LANGUANGE = "LANGUANGE"
        val FROM_LOGIN = "FROM_LOGIN"
        val ADDRESS = "ADDRESS"
        val CITY = "CITY"
        val PICKUP = "PICKUP"
        val SELECT_CITY = "SELECT_CITY"
        val SELECT_LONGITUDE_ADDRESS = "SELECT_LONGITUDE_ADDRESS"
        val SELECT_LATITUDE_ADDRESS = "SELECT_LATITUDE_ADDRESS"
        val SELECT_PHONE_NUMBER = "SELECT_PHONE_NUMBER"
        val CODE_VOUCHER = "CODE_VOUCHER"
        val PAYMENT_METHOD = "PAYMENT_METHOD"
        val VOUCHER = "VOUCHER"
        val PAYMENT_NAME = "PAYMENT_NAME"
        val PAYMENT_ICON = "PAYMENT_ICON"
        val NOTES = "NOTES"
        val LONGITUDE = "LONGITUDE"
        val LATITUDE = "LATITUDE"
        val DETAIL_ADDRESS = "DETAIL_ADDRESS"
        val DETAIL_ADDRESS_PICKUP = "DETAIL_ADDRESS_PICKUP"
        val REFRESH_STATE_CART_DETAIL = "REFRESH_STATE_CART_DETAIL"
        val LATITUDE_ADDRESS = "LATITUDE_ADDRESS"
        val LONGITUDE_ADDRESS = "LONGITUDE_ADDRESS"
        val ORDER_POST_CODE_FOR_DELIVERY = "ORDER_POST_CODE_FOR_DELIVERY"
        val OUTLET_ID_FOR_DELIVERY = "OUTLET_ID_FOR_DELIVERY"
        val OUTLET_CODE_FOR_DELIVERY = "OUTLET_CODE_FOR_DELIVERY"
        val PICKUP_ADDRESS = "PICKUP_ADDRESS"
        val POSTCODE = "POSTCODE"
        val OUTLET_ID = "OUTLET_ID"
        val OUTLET_CODE = "OUTLET_CODE"
        val VERSION = "VERSION"
        val FCM_TOKEN = "FCM_TOKEN"
        val PICKUP_CITY = "PICKUP_CITY"
        val RECIPIENT_NAME = "RECIPIENT_NAME"
        val STATE_PICKUP = "STATE_PICKUP"
        val SELECT_LATITUDE_PICKUP = "SELECT_LATITUDE_PICKUP"
        val SELECT_LONGITUDE_PICKUP = "SELECT_LONGITUDE_PICKUP"
    }

    private val PREFS_NAME = "breadtalkApp"

    val sharedPref: SharedPreferences =
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun save(KEY_NAME: String, text: String) {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putString(KEY_NAME, text)

        editor!!.commit()
    }

    fun save(KEY_NAME: String, value: Int) {
        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putInt(KEY_NAME, value)

        editor.commit()
    }

    fun save(KEY_NAME: String, status: Boolean) {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putBoolean(KEY_NAME, status!!)

        editor.commit()
    }

    fun getValueString(KEY_NAME: String): String? {

        return sharedPref.getString(KEY_NAME, null)


    }

    fun getValueInt(KEY_NAME: String): Int {

        return sharedPref.getInt(KEY_NAME, 0)
    }

    fun getValueBoolean(KEY_NAME: String, defaultValue: Boolean): Boolean {

        return sharedPref.getBoolean(KEY_NAME, defaultValue)

    }

    fun clearSharedPreference() {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        //sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        editor.clear()
        editor.commit()
    }

    fun removeValue(KEY_NAME: String) {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.remove(KEY_NAME)
        editor.commit()
    }

    fun savePhoneNumber(phone: String?) {
        sharedPref.edit()
            .putString(PHONE_NUMBER, phone)
            .apply()
    }

    fun loadPhoneNumber(): String? = sharedPref.getString(PHONE_NUMBER, "0")

    fun savePoints(points: String?) {
        sharedPref.edit()
            .putString(POINTS, points)
            .apply()
    }

    fun loadPoints(): String? = sharedPref.getString(POINTS, "")


    fun saveEmail(email: String?) {
        sharedPref.edit()
            .putString(EMAIL, email)
            .apply()
    }

    fun loadEmail(): String? = sharedPref.getString(EMAIL, "")

    fun saveName(name: String?) {
        sharedPref.edit()
            .putString(NAME, name)
            .apply()
    }

    fun loadName(): String? = sharedPref.getString(NAME, "")

    fun saveLanguage(language: String) {
        sharedPref.edit()
            .putString(LANGUANGE, language)
            .apply()
    }

    fun loadLanguage(): String? =
        sharedPref.getString(LANGUANGE, context.getString(R.string.indonesia))

    fun removeAddress() {
        sharedPref.edit()
            .remove(ADDRESS)
            .apply()
    }

    fun removeCity() {
        sharedPref.edit()
            .remove(CITY)
            .apply()
    }

    fun removePickUp() {
        sharedPref.edit()
            .remove(PICKUP)
            .apply()
    }
    fun removePickUpAddress() {
        sharedPref.edit()
            .remove(PICKUP_ADDRESS)
            .apply()
    }

    fun removeSelectCity() {
        sharedPref.edit()
            .remove(SELECT_CITY)
            .apply()
    }

    fun removeSelectLongitudeAddress() {
        sharedPref.edit()
            .remove(SELECT_LONGITUDE_ADDRESS)
            .apply()
    }

    fun removeSelectLatitudeAddress() {
        sharedPref.edit()
            .remove(SELECT_LATITUDE_ADDRESS)
            .apply()
    }

    fun removeSelectPhoneNumber() {
        sharedPref.edit()
            .remove(SELECT_PHONE_NUMBER)
            .apply()
    }

    fun saveSelectCity(address: String) {
        sharedPref.edit()
            .putString(SELECT_CITY, address)
            .apply()
    }

    fun loadSelectCity(): String? =
        sharedPref.getString(SELECT_CITY, "Jakarta Barat")

    fun saveCodeVoucher(voucher: String) {
        sharedPref.edit()
            .putString(CODE_VOUCHER, voucher)
            .apply()
    }

    fun loadCodeVoucher(): String? = sharedPref.getString(CODE_VOUCHER, "")

    fun savePaymentMethod(payment: String) {
        sharedPref.edit()
            .putString(PAYMENT_METHOD, payment)
            .apply()
    }

    fun loadPaymentMethod(): String? = sharedPref.getString(PAYMENT_METHOD, "")

    fun removeVoucher() {
        sharedPref.edit()
            .remove(VOUCHER)
            .apply()
    }

    fun removeCodeVoucher() {
        sharedPref.edit()
            .remove(CODE_VOUCHER)
            .apply()
    }

    fun savePaymentName(payment: String) {
        sharedPref.edit()
            .putString(PAYMENT_NAME, payment)
            .apply()
    }

    fun loadPaymentName(): String? = sharedPref.getString(PAYMENT_NAME, "")

    fun savePaymentIcon(payment: String) {
        sharedPref.edit()
            .putString(PAYMENT_ICON, payment)
            .apply()
    }

    fun loadPaymentIcon(): String? = sharedPref.getString(PAYMENT_ICON, "")

    fun saveNotes(notes: String) {
        sharedPref.edit()
            .putString(NOTES, notes)
            .apply()
    }

    fun loadNotes(): String? = sharedPref.getString(NOTES, "")

    fun saveVoucher(voucher: String) {
        sharedPref.edit()
            .putString(VOUCHER, voucher)
            .apply()
    }

    fun loadVoucher(): String? =
        sharedPref.getString(VOUCHER, "")


    fun saveLongitude(longitude: String?) {
        sharedPref.edit()
            .putString(LONGITUDE, longitude)
            .apply()
    }

    fun loadLongitude(): String? = sharedPref.getString(LONGITUDE, "")

    fun saveLatitude(latitude: String?) {
        sharedPref.edit()
            .putString(LATITUDE, latitude)
            .apply()
    }

    fun loadLatitude(): String? = sharedPref.getString(LATITUDE, "")

    fun saveAddress(address: String) {
        sharedPref.edit()
            .putString(ADDRESS, address)
            .apply()
    }

    fun loadAddress(): String? = sharedPref.getString(ADDRESS, "")

    fun saveDetailAddress(address: String) {
        sharedPref.edit()
            .putString(DETAIL_ADDRESS, address)
            .apply()
    }

    fun loadDetailAddress(): String? = sharedPref.getString(DETAIL_ADDRESS, "")

    fun saveDetailAddressPickUp(address: String) {
        sharedPref.edit()
            .putString(DETAIL_ADDRESS_PICKUP, address)
            .apply()
    }

    fun loadDetailAddressPickUp(): String? = sharedPref.getString(DETAIL_ADDRESS_PICKUP, "")

    fun saveSelectLatitudeAddress(latitude: String?) {
        sharedPref.edit()
            .putString(SELECT_LATITUDE_ADDRESS, latitude)
            .apply()
    }


    fun loadSelectLatitudeAddress(): String? = sharedPref.getString(SELECT_LATITUDE_ADDRESS, "")

    fun saveSelectLongitudeAddress(longitude: String?) {
        sharedPref.edit()
            .putString(SELECT_LONGITUDE_ADDRESS, longitude)
            .apply()
    }

    fun loadSelectLongitudeAddress(): String? = sharedPref.getString(SELECT_LONGITUDE_ADDRESS, "")

    fun saveSelectPhoneNumber(address: String) {
        sharedPref.edit()
            .putString(SELECT_PHONE_NUMBER, address)
            .apply()
    }

    fun loadSelectPhoneNumber(): String? =
        sharedPref.getString(SELECT_PHONE_NUMBER, "")

    fun saveRecipientName(name: String) {
        sharedPref.edit()
            .putString(RECIPIENT_NAME, name)
            .apply()
    }

    fun loadRecipientName(): String? =
        sharedPref.getString(RECIPIENT_NAME, "")

    fun savePickUp(pickUp: String) {
        sharedPref.edit()
            .putString(PICKUP, pickUp)
            .apply()
    }

    fun loadPickUp(): String? = sharedPref.getString(PICKUP, "")

    fun saveRefreshStateCartDetail(state: Boolean) {
        sharedPref.edit()
            .putBoolean(REFRESH_STATE_CART_DETAIL, state)
            .apply()
    }

    fun loadRefreshStateCartDetail(): Boolean =
        sharedPref.getBoolean(REFRESH_STATE_CART_DETAIL, false)

    fun saveLatitudeAddress(latitude: String?) {
        sharedPref.edit()
            .putString(LATITUDE_ADDRESS, latitude)
            .apply()
    }

    fun loadLatitudeAddress(): String? = sharedPref.getString(LATITUDE_ADDRESS, "")

    fun saveLongitudeAddress(longitude: String?) {
        sharedPref.edit()
            .putString(LONGITUDE_ADDRESS, longitude)
            .apply()
    }

    fun loadLongitudeAddress(): String? = sharedPref.getString(LONGITUDE_ADDRESS, "")


    fun saveCity(address: String?) {
        sharedPref.edit()
            .putString(CITY, address)
            .apply()
    }

    fun loadCity(): String? =
        sharedPref.getString(CITY, "")

    fun saveOrderPostCodeForDelivery(address: String?) {
        sharedPref.edit()
            .putString(ORDER_POST_CODE_FOR_DELIVERY, address)
            .apply()
    }

    fun loadOrderPosttCodeForDelivery(): String? =
        sharedPref.getString(ORDER_POST_CODE_FOR_DELIVERY, "")

    fun saveOutletIdForDelivery(address: String?) {
        sharedPref.edit()
            .putString(OUTLET_ID_FOR_DELIVERY, address)
            .apply()
    }

    fun loadOutletIdForDelivery(): String? = sharedPref.getString(OUTLET_ID_FOR_DELIVERY, "")

    fun saveOutletCodeForDelivery(address: String?) {
        sharedPref.edit()
            .putString(OUTLET_CODE_FOR_DELIVERY, address)
            .apply()
    }

    fun loadOutletCodeForDelivery(): String? = sharedPref.getString(OUTLET_CODE_FOR_DELIVERY, "")

    fun savePickUpAddress(address: String) {
        sharedPref.edit()
            .putString(PICKUP_ADDRESS, address)
            .apply()
    }

    fun loadPickUpAddress(): String? = sharedPref.getString(PICKUP_ADDRESS, "")

    fun savePostCode(postCode: String) {
        sharedPref.edit()
            .putString(POSTCODE, postCode)
            .apply()
    }

    fun loadPostCode(): String? = sharedPref.getString(POSTCODE, "")

    fun saveOutletId(outletId: String) {
        sharedPref.edit()
            .putString(OUTLET_ID, outletId)
            .apply()
    }

    fun loadOutletId(): String? = sharedPref.getString(OUTLET_ID, "")

    fun saveOutletCode(outletId: String) {
        sharedPref.edit()
            .putString(OUTLET_CODE, outletId)
            .apply()
    }

    fun loadOutletCode(): String? = sharedPref.getString(OUTLET_CODE, "")

    fun saveVersion(version: Int) {
        sharedPref.edit()
            .putInt(VERSION, version)
            .apply()
    }

    fun loadVersion(): Int? =
        sharedPref.getInt(VERSION, 0)

    fun saveFcmToken(token: String) {
        sharedPref.edit()
            .putString(FCM_TOKEN, token)
            .apply()
    }

    fun loadFcmToken(): String? =
        sharedPref.getString(FCM_TOKEN, "")

    fun savePickUpCity(address: String) {
        sharedPref.edit()
            .putString(PICKUP_CITY, address)
            .apply()
    }

    fun loadPickUpCity(): String? = sharedPref.getString(PICKUP_CITY, "Jakarta Barat")

    fun saveStatePickup(state: Boolean) {
        sharedPref.edit()
            .putBoolean(STATE_PICKUP, state)
            .apply()
    }

    fun loadStatePickup(): Boolean =
        sharedPref.getBoolean(STATE_PICKUP, false)

    fun saveSelectLatitudePickUp(latitude: String?) {
        sharedPref.edit()
            .putString(SELECT_LATITUDE_PICKUP, latitude)
            .apply()
    }

    fun loadSelectLatitudePickUp(): String? = sharedPref.getString(SELECT_LATITUDE_PICKUP, "")

    fun saveSelectLongitudePickup(longitude: String?) {
        sharedPref.edit()
            .putString(SELECT_LONGITUDE_PICKUP, longitude)
            .apply()
    }

    fun loadSelectLongitudePickup(): String? = sharedPref.getString(SELECT_LONGITUDE_PICKUP, "")
}