package com.breadtalk.app.utils

import android.app.Activity
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Paint
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import co.lujun.androidtagview.TagContainerLayout
import co.lujun.androidtagview.TagView
import com.breadtalk.app.R
import com.breadtalk.app.data.local.*
import com.breadtalk.app.ui.main.cart.CartControllerListener
import com.breadtalk.app.ui.main.home.BannerSliderAdapter
import com.breadtalk.app.ui.main.home.HomeControllerListener
import com.breadtalk.app.utils.KeyboardUtil.hideKeyboard
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.android.material.button.MaterialButton
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import org.sufficientlysecure.htmltextview.HtmlTextView
import java.text.SimpleDateFormat
import java.util.*


@BindingAdapter(value = ["sliderPromo", "listenerPromo", "selectedPromo"])
fun SliderView.bindSliderViewPromo(
    data: HomePromos?,
    listener: HomeControllerListener,
    selected: Boolean,
) {
    val adapter = BannerSliderAdapter(listener, selected)
    adapter.setSlider(data?.promos as MutableList<PromoBanner>)
    this.setSliderAdapter(adapter)
    this.setIndicatorAnimation(IndicatorAnimationType.WORM)
    this.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
    Log.d("cekdatapromo", data.toString())

}

@BindingAdapter(value = ["imgUrl", "roundCorner", "imgPlaceholder"], requireAll = false)
fun AppCompatImageView.setImgUrl(imgUrl: String?, roundCorner: Int = 0, imgPlaceholder: Int = 0) {

    var placeholder = R.color.grey
    if (imgPlaceholder != 0) {
        placeholder = imgPlaceholder
    }

    if (imgUrl != null) {
        if (roundCorner > 0) {
            Glide.with(context)
                .load(imgUrl)
                .placeholder(placeholder)
                .error(placeholder)
                .transform(CenterInside(), RoundedCorners(dpToPx(roundCorner)))
                .into(this)
        } else {
            Glide.with(context)
                .load(imgUrl)
                .placeholder(placeholder)
                .error(placeholder)
                .into(this)
        }
    } else {
        setImageResource(R.color.grey)
    }
}

fun dpToPx(dp: Int): Int {
    return (dp * Resources.getSystem().displayMetrics.density).toInt()
}

@BindingAdapter("isMenuCategories")
fun TextView.bindMenuCategories(state: Boolean) {
    if (state) {
        this.setTextColor(ContextCompat.getColor(context, R.color.white))
//        this.compoundDrawables[0].setTint(ContextCompat.getColor(context, R.color.white))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.orange))
//        this.compoundDrawables[0].setTint(ContextCompat.getColor(context, R.color.orange))
    }
}

@BindingAdapter("selected")
fun LinearLayout.bindSelected(selected: Boolean) {
    this.isSelected = selected
}


@BindingAdapter("titleActionbar")
fun TextView.bindTitle(title: Int) {
    this.setText(title)
}

@BindingAdapter("textColorStatusOrder")
fun TextView.bindTextColorStatusOrder(data: RecentOrder) {
    if (data.orderStatus?.contains("8", true) == true) {
        this.setTextColor(ContextCompat.getColor(context, R.color.red))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.light_green))
    }
}

@BindingAdapter("textColorStatusOrder")
fun TextView.bindTextColorStatusOrder(data: TransactionDetailHeader) {
    if (data.orderStatus?.contains("8", true) == true) {
        this.setTextColor(ContextCompat.getColor(context, R.color.red))
    } else {
        this.setTextColor(ContextCompat.getColor(context, R.color.light_green))
    }
}

@BindingAdapter(value = ["onCartChangeListener", "cartSwitch"], requireAll = false)
fun SwitchCompatEx.onCartChangeListener(listener: CartControllerListener, cartSwitch: CartSwitch) {
    this.setOnCheckedChangeListener(null)
    this.isChecked = cartSwitch.pickup
    this.setOnCheckedChangeListener { _, isChecked ->
        listener.onSwitchChange(isChecked)
    }
}

@BindingAdapter("convertRupiah")
fun bindConvertRupiah(textView: TextView, price: Double) {
    textView.text = Converter.rupiah(price)
}

@BindingAdapter("notes")
fun bindNotes(textView: TextView, notes: String?) {
    if (notes.isNullOrEmpty()) {
        textView.text = "Add Notes"
    } else {
        textView.text = notes
    }
}

@BindingAdapter("dateCart")
fun TextView.bindDateCart(date: String) {
    this.text = date
}

@BindingAdapter("voucher")
fun TextView.bindVoucher(voucher: String?) {
    if (voucher.isNullOrEmpty()) {
        this.text = context.getString(R.string.use_voucher_promo_code)
    } else {
        this.text = voucher
    }
}

@BindingAdapter("ecobagText")
fun TextView.bindEcobagText(data: CartDetail) {
    if (data.ecobag.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
        this.text = "${data.ecobagQty}X ${data.ecobag}"
    }
}

@BindingAdapter("ecobagPrice")
fun TextView.bindEcobagPrice(data: CartDetail) {
    if (data.ecobag.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
        this.text = data.ecobagSubTotal
    }
}

@BindingAdapter("hideFreeDelivery")
fun LinearLayout.bindHideFreeDelivery(data: CartDetail) {
    if (data.isDelivery) {
        if (data.freeDelivery == 0 || data.deliveryService ?: 0 < 0) {
            this.visibility = View.GONE
        } else {
            this.visibility = View.VISIBLE
        }
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("dateDeliveryLabel")
fun TextView.bindDateDeliveryLabel(isDelivery: Boolean) {
    if (isDelivery) {
        this.text = this.context.getString(R.string.delivery_date)
    } else {
        this.text = this.context.getString(R.string.pickup_date)
    }
}

@BindingAdapter("scratchText")
fun TextView.bindScratchText(data: CartDetail) {
    this.text = data.deliveryServiceText
    this.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
}

@BindingAdapter("hideDeliveryFee")
fun LinearLayout.bindHideDeliveryFee(data: CartDetail) {
    if (data.isDelivery) {
        if (data.freeDelivery == 0 || data.deliveryService ?: 0 < 0) {
            this.visibility = View.VISIBLE
        } else {
            this.visibility = View.GONE
        }
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("hidePromoCart")
fun LinearLayout.bindHidePromoCart(data: CartDetail) {
    if (data.promo == 0) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("convertRupiahPromo")
fun bindConvertRupiahPromo(textView: TextView, price: Int) {
    textView.text = "-" + Converter.rupiah(price.toDouble())
}

@BindingAdapter("paymentMethod")
fun bindPaymentMethod(textView: TextView, paymentMethod: String?) {
    if (paymentMethod.isNullOrEmpty()) {
        textView.text = textView.context.getString(R.string.choose_payment)
    } else {
        textView.text = paymentMethod
    }
}

@BindingAdapter("imgDrawable")
fun AppCompatImageView.bindImgDrawable(drawable: Int) {
    this.setImageResource(drawable)
}

@BindingAdapter("isMenuCategories")
fun TagContainerLayout.bindMenuCategories(state: Boolean) {
    if (state) {
        this.tagTextColor = ContextCompat.getColor(context, R.color.orange)
    } else {
        this.tagTextColor = ContextCompat.getColor(context, R.color.orange)
    }
}

@BindingAdapter("selected")
fun TagContainerLayout.bindSelected(selected: Boolean) {
    this.isSelected = selected
}

@BindingAdapter(value = ["tagClickListener", "tagData"], requireAll = false)
fun TagContainerLayout.bindClick(
    listener: HomeControllerListener,
    menuSearchTagName: SearchCategory,
) {
    this.setOnTagClickListener(object : TagView.OnTagClickListener {
        override fun onTagClick(position: Int, text: String?) {
            listener.onSearchMenuCategoryClick(menuSearchTagName, position)
        }

        override fun onTagLongClick(position: Int, text: String?) {
            TODO("Not yet implemented")
        }

        override fun onSelectedTagDrag(position: Int, text: String?) {
            TODO("Not yet implemented")
        }

        override fun onTagCrossClick(position: Int) {
            TODO("Not yet implemented")
        }
    })
}

@BindingAdapter("listTags")
fun TagContainerLayout.bindListTags(datas: List<String>) {
    this.tags = datas
}

@BindingAdapter("showSearchLatest")
fun ConstraintLayout.bindShowSearchLatest(hasFocus: Boolean) {
    if (hasFocus) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("showSearchLatest")
fun TextView.bindShowSearchLatest(hasFocus: Boolean) {
    if (hasFocus) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("showSearchLatest")
fun ImageView.bindShowSearchLatest(hasFocus: Boolean) {
    if (hasFocus) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("textButtonBanner")
fun MaterialButton.bindTextButtonBanner(data: DetailSpecialOffer) {
    if (data.relatedMenu?.size ?: 0 > 1) {
        this.text = context.getString(R.string.choose_promo_menu)
    } else {
        this.text = context.getString(R.string.order_now)
    }
}

@BindingAdapter("htmlText")
fun bindHtmlText(textView: TextView, text: String?) {
    textView.text = text?.replace("<br>", "\n", true)
}

@BindingAdapter("typeDelivery")
fun TextView.bindTypeDelivery(orderDetail: TransactionDetailShippingInformation) {
    if (orderDetail.deliveryType?.contains("0", true) == true) {
        this.text = context.getString(R.string.delivered_to)
    } else {
        this.text = context.getString(R.string.pickup_at)
    }
}

@BindingAdapter("addressName")
fun TextView.bindAddressName(orderDetail: TransactionDetailShippingInformation) {
    if (orderDetail.deliveryType?.contains("0", true) == true) {
        this.text = orderDetail.orderAddress
    } else {
        this.text = orderDetail.outletName
    }
}

@BindingAdapter("addressDetail")
fun TextView.bindAddressDetail(orderDetail: TransactionDetailShippingInformation) {
    if (orderDetail.deliveryType?.contains("0", true) == true) {
        this.text = orderDetail.orderInfo
    } else {
        this.text = orderDetail.outletAddress
    }
}

@BindingAdapter("phoneTransaction")
fun TextView.bindPhoneTransaction(orderDetail: TransactionDetailShippingInformation) {
    if (orderDetail.deliveryType?.contains("0", true) == true) {
        this.text = orderDetail.memberPhone
    } else {
        this.text = orderDetail.outletPhone
    }
}

@BindingAdapter("isHideSendFrom")
fun LinearLayout.bindIsHideSendFrom(orderDetail: TransactionDetailShippingInformation) {
    if (orderDetail.deliveryType?.contains("0", true) == true) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("hideVaNumber")
fun TextView.bindHideVaNumber(data: TransactionDetailOrderSummary) {
    if (data.vaNumber.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideInstruction")
fun TextView.bindHideInstruction(data: TransactionDetailOrderSummary) {
    if (data.getInstruction().isNullOrEmpty() || data.orderStatus?.contains("1") == false) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("htmlTextView")
fun HtmlTextView.bindHtmlTexView(text: String?) {
    this.setHtml(text.toString().replace("\n", "<br>", true))
}

@BindingAdapter("hidePromo")
fun TextView.bindHidePromo(data: TransactionDetailTotal) {
    if (data.promo == 0) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideText")
fun TextView.bindHideText(text: String?) {
    if (text.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("hideText")
fun TextView.bindHideText(state: Boolean) {
    if (state) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("hideText")
fun AppCompatImageView.bindHideText(text: String?) {
    if (text.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("textProductDetailHeader")
fun TextView.bindTextProductDetailHeader(state: Boolean) {
    if (state) {
        this.text = this.context.getString(R.string.select_variant)
    } else {
        this.text = this.context.getString(R.string.photo)
    }
}

@BindingAdapter("hidePlusMinus")
fun ImageView.bindHideText(data: ProductDetailContent) {
    if (data.variant.isNullOrEmpty()) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("hidePlusMinus")
fun EditText.bindHideText(data: ProductDetailContent) {
    if (data.variant.isNullOrEmpty()) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("hideQtyDetail")
fun TextView.bindHideText(data: ProductDetailContent) {
    if (data.variant.isNullOrEmpty()) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
    }
}

@BindingAdapter("isShow")
fun EditText.bindIsShow(state: Boolean) {
    if (state) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("isShow")
fun ImageView.bindIsShow(state: Boolean) {
    if (state) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("isShow")
fun TextView.bindIsShow(state: Boolean) {
    if (state) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

@BindingAdapter("setMarginTopProduct")
fun ImageView.bindSetMarginTopProduct(state: Boolean) {
    val param = this.layoutParams as ViewGroup.MarginLayoutParams
    if (state) {
        param.setMargins(0, 240, 0, 8)
    } else {
        param.setMargins(0, 160, 0, 8)
    }
}

@BindingAdapter("changeToDelete")
fun ImageView.bindChangeToDelete(data: CartProduct) {
    if (data.qty == 1) {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_delete))
    } else {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_minus))
    }
}

@BindingAdapter("changeToDelete")
fun ImageView.bindChangeToDelete(data: CartProductPickUp) {
    if (data.qty == 1) {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_delete))
    } else {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_minus))
    }
}

@BindingAdapter(value = ["search", "activity"])
fun SearchView.bindSearch(listener: HomeControllerListener, activity: Activity) {
    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            listener.onSearchItem(query.toString())
            hideKeyboard(activity)
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean = false

    })
}

@BindingAdapter("isMenuItemFavorite")
fun ImageView.bindFavorite(isFavorite: Boolean) {
    if (isFavorite) {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_red))
    } else {
        this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite))
    }

}

@BindingAdapter("textVersion")
fun TextView.bindTextVersion(data: ProfileFooter) {
    try {
        val pInfo = this.context.packageManager.getPackageInfo(this.context.packageName, 0)
        val version = pInfo.versionName
        this.text = "V$version(${pInfo.versionCode})"
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }
}

@BindingAdapter("isUsedVoucherBackground")
fun AppCompatButton.bindIsUsedVoucherBackground(data: Voucher) {
    if (data.isUsed == 1) {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_grey)
    } else {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_orange_button)
    }
}

@BindingAdapter("isUsedVoucherText")
fun TextView.bindIsUsedVoucherText(data: Voucher) {
    if (data.isUsed == 1) {
        this.text = context.getString(R.string.already_used)
    } else {
        this.text = context.getString(R.string.use_the_voucher)
    }
}

@BindingAdapter("isClaimBackground")
fun AppCompatButton.bindIsClaimBackground(data: Voucher) {
    if (data.isClaim == 0) {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_orange_button)
    } else {
        this.background = ContextCompat.getDrawable(context, R.drawable.rounded_grey)
    }
}

@BindingAdapter("isClaimText")
fun TextView.bindIsClaimText(data: Voucher) {
    if (data.isClaim == 0) {
        this.text = context.getString(R.string.claim_now)
    } else {
        this.text = context.getString(R.string.claimed)
    }
}

@BindingAdapter(value = ["showPcsBasePrice", "textBasePrice"], requireAll = false)
fun TextView.bindShowBasePrice(isBasePrice: Int, text: String) {
    if (isBasePrice == 1) {
        this.text = "${text}pcs"
    } else {
        this.text = "${text}X"
    }
}

@BindingAdapter("distancePickup")
fun TextView.bindDistancePickup(data: PickupItem) {
    if (data.distance.contains("null")) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
        this.text = data.distance
    }
}

fun String.parseDateAndTimeWithFormat(format: String): String? {
    val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    try {
        val result = df.parse(this) ?: return ""
        val sdf = SimpleDateFormat(format, Locale.getDefault())
        return sdf.format(result)
    } catch (e: java.lang.Exception) {
        e.printStackTrace()
    }
    return this
}


fun String.parseDateWithFormat(format: String): String? {
    val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    try {
        val result = df.parse(this) ?: return ""
        val sdf = SimpleDateFormat(format, Locale.getDefault())
        return sdf.format(result)
    } catch (e: java.lang.Exception) {
        e.printStackTrace()
    }
    return this
}

fun Date.parseToTime(): String {
    val format = SimpleDateFormat("HH:mm")
    return format.format(this)
}