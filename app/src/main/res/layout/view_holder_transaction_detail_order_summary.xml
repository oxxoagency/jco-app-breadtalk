<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="data"
            type="com.breadtalk.app.data.local.TransactionDetailOrderSummary" />

        <variable
            name="listener"
            type="com.breadtalk.app.ui.transaction_detail.TransactionDetailControllerListener" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:paddingHorizontal="16dp"
        android:paddingTop="16dp">


        <TextView
            android:id="@+id/label_order"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:fontFamily="@font/poppins_medium"
            android:text="@string/order_summary"
            android:textColor="@color/black"
            android:textSize="18sp"
            android:textStyle="bold"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <TextView
            android:id="@+id/label_no"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:fontFamily="@font/poppins"
            android:text="@string/no_order"
            android:textColor="@color/black_1818"
            android:textSize="14sp"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/label_order" />

        <TextView
            android:id="@+id/order_id"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="8dp"
            android:fontFamily="@font/poppins_medium"
            android:text="@{data.orderId}"
            android:textColor="@color/black"
            android:textSize="16sp"
            android:textStyle="bold"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/label_no"
            tools:text="99999999" />

        <TextView
            android:id="@+id/label_va"
            hideVaNumber="@{data}"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="24dp"
            android:fontFamily="@font/poppins"
            android:text="@string/virtual_account_number"
            android:textColor="@color/black_1818"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/order_id" />

        <TextView
            android:id="@+id/va"
            hideVaNumber="@{data}"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="8dp"
            android:fontFamily="@font/poppins_medium"
            android:text="@{data.vaNumber}"
            android:textColor="@color/black"
            android:textSize="16sp"
            android:textStyle="bold"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/label_va"
            tools:text="99999999" />

        <org.sufficientlysecure.htmltextview.HtmlTextView
            android:id="@+id/instruction"
            hideInstruction="@{data}"
            htmlTextView="@{data.instruction}"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="24dp"
            android:fontFamily="@font/poppins"
            android:textColor="@color/black_1818"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="@id/va"
            app:layout_constraintTop_toBottomOf="@id/copy_va"
            tools:maxLines="5"
            tools:text="@tools:sample/lorem/random" />

        <TextView
            android:id="@+id/copy"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:background="@drawable/bg_copy"
            android:drawablePadding="6dp"
            android:fontFamily="@font/poppins"
            android:onClick="@{() -> listener.onCopyClick(data)}"
            android:padding="8dp"
            android:text="@string/copy"
            android:textColor="@color/orange"
            android:textSize="14sp"
            app:drawableLeftCompat="@drawable/ic_copy"
            app:layout_constraintBottom_toBottomOf="@+id/order_id"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="@id/order_id" />

        <TextView
            android:id="@+id/copy_va"
            hideVaNumber="@{data}"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:background="@drawable/bg_copy"
            android:drawablePadding="6dp"
            android:fontFamily="@font/poppins"
            android:onClick="@{() -> listener.onCopyVAClick(data)}"
            android:padding="8dp"
            android:text="@string/copy"
            android:textColor="@color/orange"
            android:textSize="14sp"
            app:drawableLeftCompat="@drawable/ic_copy"
            app:layout_constraintBottom_toBottomOf="@+id/va"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="@id/va" />

        <TextView
            android:id="@+id/label_date"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="24dp"
            android:fontFamily="@font/poppins"
            android:text="@string/purchase_date"
            android:textColor="@color/black_1818"
            android:textSize="14sp"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/instruction" />

        <TextView
            android:id="@+id/date"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="8dp"
            android:fontFamily="@font/poppins_medium"
            android:text="@{data.purchaseDate}"
            android:textColor="@color/black"
            android:textSize="16sp"
            android:textStyle="bold"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/label_date"
            tools:layout_editor_absoluteX="16dp"
            tools:text="14 October 2020" />

        <TextView
            android:id="@+id/label_send_date"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="24dp"
            android:fontFamily="@font/poppins"
            dateDeliveryLabel="@{data.isDelivery}"
            android:textColor="@color/black_1818"
            android:textSize="14sp"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/date" />

        <TextView
            android:id="@+id/send_date"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="8dp"
            android:fontFamily="@font/poppins_medium"
            android:text="@{data.sendDate}"
            android:textColor="@color/black"
            android:textSize="16sp"
            android:textStyle="bold"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/label_send_date"
            tools:layout_editor_absoluteX="16dp"
            tools:text="14 October 2020" />

        <TextView
            android:id="@+id/label_note"
            hideText="@{data.note}"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="24dp"
            android:fontFamily="@font/poppins"
            android:text="@string/notes"
            android:textColor="@color/black_1818"
            android:textSize="14sp"
            app:layout_constraintStart_toStartOf="@id/send_date"
            app:layout_constraintTop_toBottomOf="@id/send_date" />

        <TextView
            android:id="@+id/note"
            hideText="@{data.note}"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="8dp"
            android:fontFamily="@font/poppins"
            android:text="@{data.note}"
            android:textColor="@color/black_1818"
            android:textSize="14sp"
            app:layout_constraintStart_toStartOf="@id/label_note"
            app:layout_constraintTop_toBottomOf="@id/label_note"
            tools:text="ini catatan" />

        <TextView
            android:id="@+id/label_product"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="24dp"
            android:fontFamily="@font/poppins"
            android:text="@string/product"
            android:textColor="@color/black_1818"
            android:textSize="14sp"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/note" />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>